import { IBizSearchViewController } from '@ibizsys/app/IBizSearchViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 数据图表视图控制器
 * 
 * @export
 * @class IBizChartViewController
 * @extends {IBizSearchViewController}
 */
export class IBizChartViewController extends IBizSearchViewController {

    /**
     * Creates an instance of IBizChartViewController.
     * 创建 IBizChartViewController 对象
     * 
     * @param {*} [opts={}] 
     * @memberof IBizChartViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图部件初始化
     * 
     * @memberof IBizChartViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const chart = this.getChart();
        if (chart) {
            chart.on(IBizEvent.IBizChart_LOADED, (data) => {
                this.onStoreLoad(data);
            });
            chart.on(IBizEvent.IBizChart_BEFORELOAD, (data) => {
                this.onStoreBeforeLoad(data);
            });
            chart.on(IBizEvent.IBizChart_DBLCLICK, (data) => {
                this.onDataActivated();
            });
        }
    }

    /**
     * 视图加载
     * 
     * @memberof IBizChartViewController
     */
    public onLoad(): void {
        super.onLoad();
        if (!this.getSearchForm() && this.getChart() && this.isLoadDefault()) {
            this.getChart().load();
        }
    }

    /**
     * 获取图表部件
     * 
     * @returns {*} 
     * @memberof IBizChartViewController
     */
    public getChart(): any {
        return this.getControl('chart');
    }

    /**
     * 搜索表单触发加载
     * 
     * @param {boolean} isload 是否加载
     * @memberof IBizChartViewController
     */
    public onSearchFormSearched(isload: boolean): void {
        if (this.getChart() && isload) {
            this.getChart().load();
        }
    }

    /**
     * 表单重置完成
     * 
     * @memberof IBizChartViewController
     */
    public onSearchFormReseted(): void {
        if (this.getChart()) {
            this.getChart().load();
        }
    }

    /**
     * 视图部件刷新
     * 
     * @memberof IBizChartViewController
     */
    public onRefresh(): void {
        super.onRefresh();
        if (this.getChart()) {
            this.getChart().load();
        }
    }
}