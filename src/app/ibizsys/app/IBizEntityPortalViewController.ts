import { IBizPortalViewController } from '@ibizsys/app/IBizPortalViewController';

/**
 * Portal视图控制器
 * 
 * @export
 * @class IBizPortalViewController
 * @extends {IBizPortalViewController}
 */
export class IBizEntityPortalViewController extends IBizPortalViewController {

    /**
     * Creates an instance of IBizEntityPortalViewController. 
     * 创建 IBizEntityPortalViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizEntityPortalViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}


