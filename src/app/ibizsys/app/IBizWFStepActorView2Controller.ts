import { ParamMap } from '@angular/router';
import { IBizWFStepActorViewController } from '@ibizsys/app/IBizWFStepActorViewController';

/**
 * 工作流流程图页面
 * 
 * @export
 * @class IBizWFStepActorView2Controller
 * @extends {IBizWFStepActorViewController}
 */
export class IBizWFStepActorView2Controller extends IBizWFStepActorViewController {

    /**
     * 实体名称
     * 
     * @type {string}
     * @memberof IBizWFStepActorView2Controller
     */
    public srfdeid: string;

    /**
     * Creates an instance of IBizWFStepActorView2Controller.
     * 创建 IBizWFStepActorView2Controller 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizWFStepActorView2Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图初始化
     * 
     * @memberof IBizWFStepActorView2Controller
     */
    public onInit() {
        this.$routeActive.queryParamMap.subscribe((paramMap: ParamMap) => {
            if (paramMap.get('srfdeid')) {
                this.srfdeid = paramMap.get('srfdeid');
                this.addViewParam({ srfdeid: this.srfdeid });
            }
        });
        super.onInit();
    }
}