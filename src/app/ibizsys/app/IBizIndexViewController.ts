import { NavigationEnd } from '@angular/router';
import { IBizUICounterService } from '@ibizsys/util/IBizUICounterService';
import { IBizIndexViewControllerBase } from '@ibizsys/app/IBizIndexViewControllerBase';
import { IBizEvent } from '@ibizsys/IBizEvent';
import { filter } from 'rxjs/operators';

/**
 * 首页应用视图
 * 
 * @export
 * @class IBizIndexViewController
 * @extends {IBizMainViewController}
 */
export class IBizIndexViewController extends IBizIndexViewControllerBase {

    /**
     * 视图类型
     *
     * @type {string}
     * @memberof IBizIndexViewController
     */
    public viewtype: string = 'index';

    /**
     * 默认加载视图
     *
     * @type {string}
     * @memberof IBizIndexViewController
     */
    public defaultView: string = '';

    /**
     * 应用菜单是否加载完成
     *
     * @private
     * @type {boolean}
     * @memberof IBizIndexViewController
     */
    private appMenuIsLoad: boolean = false;

    /**
     * 路由时间
     *
     * @private
     * @type {*}
     * @memberof IBizIndexViewController
     */
    private routeEvent: any = null;

    /**
     * Creates an instance of IBizIndexViewController.
     * 创建 IBizIndexViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizIndexViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 应用菜单部件初始化
     * 
     * @memberof IBizIndexViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const appMenu = this.getAppMenu();
        if (appMenu) {
            appMenu.on(IBizEvent.IBizAppMenu_LOADED, (items: any) => {
                this.appMenuIsLoad = true;
                this.appMenuLoaded(items);
            });
            appMenu.on(IBizEvent.IBizAppMenu_UPDATE, (item: any) => {
                this.menuItemUpdate(item);
            });
        }
        const defaultUICounter: IBizUICounterService = this.getUICounter(this.getDefaultUICounterName());
        if (defaultUICounter) {
            defaultUICounter.on(IBizEvent.COUNTERCHANGE, (data: any) => {
                if (data) {
                    this.$ibizAppService.setAppUICounter(data);
                }
            });
        }

        this.routeEvent = this.$route.events
            .pipe(filter(evt => evt instanceof NavigationEnd))
            .subscribe((evt: NavigationEnd) => {
                if (this.appMenuIsLoad) {
                    this.openDefaultView();
                }
            });
    }

    public onDestroy(): void {
        super.onDestroy();
        if (this.routeEvent) {
            this.routeEvent.unsubscribe();
        }
    }

    /**
     * 部件加载
     * 
     * @memberof IBizIndexViewController
     */
    public onLoad(): void {
        super.onLoad();
        const appMenu = this.getAppMenu();
        if (appMenu) {
            appMenu.load();
        }
        this.setMianMenuState();
    }

    /**
     * 应用菜单部件加载完成,调用基类处理
     * 
     * @private
     * @param {any[]} items 
     * @memberof IBizIndexViewController
     */
    public appMenuLoaded(items: any[]): void {
        super.appMenuLoaded(items);

        this.openDefaultView();
    }

    /**
     * 打开默认视图
     *
     * @private
     * @memberof IBizIndexViewController
     */
    private openDefaultView(): void {
        if (this.$routeActive.children && this.$routeActive.children.length > 0) {
            return;
        }
        const appMenu: any = this.getAppMenu();
        let item: any = {};
        if (!Object.is(this.defaultView, '')) {
            let subItem = appMenu.getItem(appMenu.getItems(), null, this.defaultView, null);
            if (Object.keys(subItem).length) {
                Object.assign(item, { link: this.defaultView });
                Object.assign(item, subItem);
            }
        }
        if (Object.keys(item).length === 0) {
            Object.assign(item, appMenu.getFirstItem(appMenu.getItems()));
        }
        if (Object.keys(item).length > 0) {
            this.openView(item.link, {});
        }
    }

    /**
     * 获取表单项
     * 
     * @returns {*} 
     * @memberof IBizIndexViewController
     */
    public getAppMenu(): any {
        return this.getControl('appmenu');
    }

    /**
     * 导航数据跳转处理
     * 
     * @param {*} [data={}] 
     * @memberof IBizIndexViewController
     */
    public navigationLink(data: any = {}) {
        Object.assign(data, { breadcrumbs: true });
        this.$ibizAppService.updateActivatedRouteDatas(data);
        if (data.routerurl) {
            this.$route.navigateByUrl(data.routerurl);
        }
    }

    /**
     * 设置主菜单状态
     *
     * @param {string} [align]
     * @memberof IBizIndexViewController
     */
    public setMianMenuState(align?: string): void {
        if (Object.is(align, 'NONE')) {
            this.$ibizAppService.setMainMenuState(false);
        } else {
            this.$ibizAppService.setMainMenuState(true);
        }
    }

    /**
     * 获取默认计数器名称
     *
     * @returns {string}
     * @memberof IBizIndexViewController
     */
    public getDefaultUICounterName(): string {
        return undefined;
    }

}
