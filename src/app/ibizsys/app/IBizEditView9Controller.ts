import { Input } from '@angular/core';
import { IBizEditViewController } from '@ibizsys/app/IBizEditViewController';

/**
 * 嵌入编辑视图控制器
 * 
 * @export
 * @class IBizEditView9Controller
 * @extends {IBizEditViewController}
 */
export class IBizEditView9Controller extends IBizEditViewController {

    /**
     * 
     *
     * @type {*}
     * @memberof IBizEditView9Controller
     */
    @Input()
    parentmode: any

    /**
     * 
     *
     * @type {string}
     * @memberof IBizEditView9Controller
     */
    @Input()
    srfparentkey: string;

    /**
     * 
     *
     * @type {*}
     * @memberof IBizEditView9Controller
     */
    @Input()
    activeData: any;

    /**
     * 
     *
     * @memberof IBizEditView9Controller
     */
    @Input()
    set refreshGrid(count) {
        if (count > 0) {
            if (this.parentmode && this.parentmode.srfparenttype && Object.is(this.parentmode.srfparenttype, 'CUSTOM')) {
                // this.$viewParam = {};
                this.addViewParam({ srfreferdata: JSON.stringify(this.activeData) });
                this.addViewParam({ srfparentkey: this.srfparentkey });
                this.onRefresh();
                return;
            }
            if (!this.srfparentkey || Object.is(this.srfparentkey, '')) {
                return;
            }
            if (!this.activeData || Object.is(this.activeData, '') || Object.keys(this.activeData).length === 0) {
                return;
            }

            // this.$viewParam = {};
            this.addViewParam({ srfreferdata: JSON.stringify(this.activeData) });
            this.addViewParam({ srfparentkey: this.srfparentkey });
            this.onRefresh();
        }
    }

    /**
     * Creates an instance of IBizEditView9Controller.
     * 创建 IBizEditView9Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizEditView9Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 
     *
     * @memberof IBizEditView9Controller
     */
    public onInited(): void {
        super.onInited();
        if (this.parentmode && !Object.is(this.parentmode, '') && typeof this.parentmode === 'object') {
            this.addViewParam(this.parentmode);
        }

        if (this.$viewParam && this.$viewParam.srfkey) {
            delete this.$viewParam.srfkey;
        }
    }

    /**
     * 
     *
     * @memberof IBizEditView9Controller
     */
    public onLoad(): void {
    }

    /**
     * 
     *
     * @memberof IBizEditView9Controller
     */
    public updateViewInfo(): void {

    }

    /**
     * 视图刷新
     * 
     * @memberof IBizEditView9Controller
     */
    public onRefresh(): void {
        const editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    }

    /**
     * 视图参数变化，嵌入表单，手动刷新数据
     * 
     * @param {*} change 
     * @memberof IBizEditView9Controller
     */
    public viewParamChange(change: any) {
        if (change.srfparentkey && !Object.is(change.srfparentkey, '')) {
            this.addViewParam({ srfkey: change.srfparentkey });
            this.onRefresh();
        }
    }
}
