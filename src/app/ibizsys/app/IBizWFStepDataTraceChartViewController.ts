import { ParamMap } from '@angular/router';
import { IBizMDViewController } from '@ibizsys/app/IBizMDViewController';

/**
 * 工作流流程图页面
 * 
 * @export
 * @class IBizWFStepActorViewController
 * @extends {IBizMDViewController}
 */
export class IBizWFStepDataTraceChartViewController extends IBizMDViewController {

    /**
     * 实体名称
     * 
     * @type {string}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    public srfdeid: string;

    /**
     * Creates an instance of IBizWFStepDataTraceChartViewController.
     * 创建 IBizWFStepDataTraceChartViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizWFStepDataTraceChartViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图初始化
     * 
     * @memberof IBizWFStepDataTraceChartViewController
     */
    public onInit() {
        this.$routeActive.queryParamMap.subscribe((paramMap: ParamMap) => {
            if (paramMap.get('srfdeid')) {
                this.srfdeid = paramMap.get('srfdeid');
                this.addViewParam({ srfdeid: this.srfdeid });
            }
        });
        super.onInit();
    }
}