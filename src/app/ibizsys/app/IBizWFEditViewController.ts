import { IBizEditViewController } from '@ibizsys/app/IBizEditViewController';

/**
 * 工作流编辑视图控制器
 * 
 * @export
 * @class IBizWFEditViewController
 * @extends {IBizEditViewController}
 */
export class IBizWFEditViewController extends IBizEditViewController {

    /**
     * Creates an instance of IBizWFEditViewController.
     * 创建 IBizWFEditViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizWFEditViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

