import { IBizMDViewController } from '@ibizsys/app/IBizMDViewController';

/**
 * 多数据编辑视图控制器对象
 *
 * @export
 * @class IBizMEditView9Controller
 * @extends {IBizMDViewController}
 */
export class IBizMEditView9Controller extends IBizMDViewController {

    /**
     * Creates an instance of IBizMEditView9Controller.
     * 创建 IBizMEditView9Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizMEditView9Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 获取多数据部件对象
     *
     * @returns {*}
     * @memberof IBizMEditView9Controller
     */
    public getMDCtrl(): any {
        return this.getControl('meditviewpanel');
    }

    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizMEditView9Controller
     */
    public isEnableQuickSearch(): boolean {
        return false;
    }
}