import { IBizMainViewController } from '@ibizsys/app/IBizMainViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * Portal视图控制器
 * 
 * @export
 * @class IBizPortalViewController
 * @extends {IBizMainViewController}
 */
export class IBizPortalViewController extends IBizMainViewController {

    /**
     * 门户部件
     *
     * @private
     * @type {Object}
     * @memberof IBizPortalViewController
     */
    private $portalCtrls: Object = {};

    /**
     * Creates an instance of IBizPortalViewController.
     * 创建 IBizPortalViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizPortalViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化视图
     * 
     * @memberof IBizPortalViewController
     */
    public onInit(): void {
        this.regPortalCtrls();
        super.onInit();
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizPortalViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();

        for (const key in this.$portalCtrls) {
            if (this.$portalCtrls.hasOwnProperty(key)) {
                const portalCtrl: any = Object.getOwnPropertyDescriptor(this.$portalCtrls, key).value;
                // portalCtrl.on(IBizStaticVariables.LOADED, (data) => {
                //     this.portalCtrlLoaded(data);
                // });
                portalCtrl.on(IBizEvent.IBizAppMenu_MENUSELECTION, (data) => {
                    this.onMenuSelection(data);
                });
            }
        }
    }

    /**
     * 视图加载
     *
     * @memberof IBizPortalViewController
     */
    public onLoad(): void {
        super.onLoad();
        for (const ctrlName in this.$portalCtrls) {
            if (this.$portalCtrls.hasOwnProperty(ctrlName)) {
                const portalCtrl: any = Object.getOwnPropertyDescriptor(this.$portalCtrls, ctrlName).value;
                if (typeof portalCtrl.load !== 'undefined' && portalCtrl.load instanceof Function) {
                    portalCtrl.load(this.$viewParam);
                }
            }
        }
    }

    /**
     * 部件加载完成
     * 
     * @param {Array<any>} data 
     * @memberof IBizPortalViewController
     */
    public portalCtrlLoaded(data: Array<any>): void {

    }

    /**
     * 注册所有Portal部件
     *
     * @memberof IBizPortalViewController
     */
    public regPortalCtrls(): void {

    }

    /**
     * 注册所有Portal部件
     * 
     * @param {string} name 
     * @param {*} ctrl 
     * @memberof IBizPortalViewController
     */
    public regPortalCtrl(name: string, ctrl: any): void {
        if (name && ctrl) {
            this.$portalCtrls[name] = ctrl;
        }
    }

    /**
     * 菜单选中跳转路由
     * 
     * @param {*} data 
     * @memberof IBizPortalViewController
     */
    public onMenuSelection(data: any) {
        if (data && data.link) {
            if (this.$routeActive['_routerState']) {
                let url = this.$routeActive['_routerState'].snapshot.url;
                this.$route.navigate([url + '/' + data.link, {}]);
            }
        }
    }

}


