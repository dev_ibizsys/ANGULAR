import { IBizSearchViewController } from '@ibizsys/app/IBizSearchViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 报表视图控制器
 * 
 * @export
 * @class IBizReportViewController
 * @extends {IBizSearchViewController}
 */
export class IBizReportViewController extends IBizSearchViewController {

    /**
     * Creates an instance of IBizReportViewController.
     * 创建 IBizReportViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizReportViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizReportViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const reportPanel = this.getReportPanel();
        if (reportPanel) {
            reportPanel.on(IBizEvent.IBizReportPanel_BEFORELOAD, (data) => {
                this.onStoreBeforeLoad(data);
            });
        }
    }

    /**
     * 视图加载
     * 
     * @memberof IBizReportViewController
     */
    public onLoad(): void {
        super.onLoad();
        if (!this.getSearchForm() && this.getReportPanel() && this.isLoadDefault()) {
            this.getReportPanel().load();
        }
    }

    /**
     * 获取报表部件
     * 
     * @returns {*} 
     * @memberof IBizReportViewController
     */
    public getReportPanel(): any {
        return this.getControl('reportpanel');
    }

    /**
     * 搜索搜索表单搜索
     * 
     * @param {boolean} isload 是否加载
     * @memberof IBizReportViewController
     */
    public onSearchFormSearched(isload: boolean): void {
        if (this.getReportPanel() && isload) {
            this.getReportPanel().load();
        }
    }

    /**
     * 搜搜表单重置
     * 
     * @memberof IBizReportViewController
     */
    public onSearchFormReseted(): void {
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    }

    /**
     * 视图刷新
     * 
     * @memberof IBizReportViewController
     */
    public onRefresh(): void {
        super.onRefresh();
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    }
}