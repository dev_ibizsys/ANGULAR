import { IBizListViewController } from '@ibizsys/app/IBizListViewController';

/**
 * 选择列表视图控制器
 * 
 * @export
 * @class IBizPickupListViewController
 * @extends {IBizListViewController}
 */
export class IBizPickupListViewController extends IBizListViewController {

    /**
     * Creates an instance of IBizPickupListViewController.
     * 创建 IBizPickupListViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizPickupListViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

