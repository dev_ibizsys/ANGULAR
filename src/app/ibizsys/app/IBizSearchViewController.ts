import { IBizMainViewController } from '@ibizsys/app/IBizMainViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 搜索视图控制器
 * 
 * @export
 * @class IBizSearchViewController
 * @extends {IBizMainViewController}
 */
export class IBizSearchViewController extends IBizMainViewController {

    /**
     * 父数据改变
     * 
     * @memberof IBizSearchViewController
     */
    public $parentDataChanged = true;

    /**
     * 快速搜索值变化
     * 
     * @type {*}
     * @memberof IBizSearchViewController
     */
    public $searchValue: any;

    /**
     * Creates an instance of IBizSearchViewController.
     * 创建 IBizSearchViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizSearchViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizSearchViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();

        const searchform = this.getSearchForm();
        if (searchform) {
            // 表单加载完成
            searchform.on(IBizEvent.IBizForm_FORMLOADED, (data) => {
                this.onSearchFormSearched(this.isLoadDefault());
            });
            // 表单搜索，手动触发
            searchform.on(IBizEvent.IBizSearchForm_FORMSEARCHED, (data) => {
                this.onSearchFormSearched(true);
            });
            // 表单重置
            searchform.on(IBizEvent.IBizSearchForm_FORMRESETED, (data) => {
                this.onSearchFormReseted();
            });
            // 表单值变化
            searchform.on(IBizEvent.IBizForm_FORMFIELDCHANGED, (data) => {
                if (!data) {
                    return;
                }
                const fieldname = data.name;
                this.onSearchFormFieldChanged(fieldname, data.value, data);
            });
            // 设置表单是否开启
            searchform.setOpen(!this.isEnableQuickSearch());
        }
    }

    /**
     * 视图加载
     * 
     * @memberof IBizSearchViewController
     */
    public onLoad(): void {
        super.onLoad();
        const searchform = this.getSearchForm();
        if (searchform) {
            searchform.autoLoad({});
        }
    }

    /**
     * 获取搜索表单对象
     * 
     * @returns {*} 
     * @memberof IBizSearchViewController
     */
    public getSearchForm(): any {
        return this.getControl('searchform');
    }

    /**
     * 搜索表单属性值发生变化
     * 
     * @param {string} fieldname 
     * @param {string} field 
     * @param {string} value 
     * @memberof IBizSearchViewController
     */
    public onSearchFormFieldChanged(fieldname: string, field: string, value: string): void {

    }

    /**
     * 搜索表单重置
     * 
     * @memberof IBizSearchViewController
     */
    public onSearchFormReseted(): void {

    }

    /**
     * 视图是否默认加载
     * 
     * @returns {boolean} 
     * @memberof IBizSearchViewController
     */
    public isLoadDefault(): boolean {
        return true;
    }

    /**
     * 是否支持快速搜索
     * 
     * @returns {boolean} 
     * @memberof IBizSearchViewController
     */
    public isEnableQuickSearch(): boolean {
        return true;
    }

    /**
     * 获取搜索表单值
     * 
     * @returns {*} 
     * @memberof IBizSearchViewController
     */
    public getSearchCond(): any {
        if (this.getSearchForm()) {
            return this.getSearchForm().getValues();
        }
        return {};
    }

    /**
     * 搜索表单搜索执行
     * 
     * @param {boolean} isload 是否加载
     * @memberof IBizSearchViewController
     */
    public onSearchFormSearched(isload: boolean): void {

    }

    /**
     * 数据加载之前
     * 
     * @param {*} [args={}] 
     * @memberof IBizSearchViewController
     */
    public onStoreBeforeLoad(args: any = {}): void {
        let fetchParam: any = {};
        if (this.getViewParam()) {
            Object.assign(fetchParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(fetchParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(fetchParam, this.getParentData());
        }
        if ((this.getSearchCond() && this.getSearchForm().isOpen()) || !this.isEnableQuickSearch()) {
            Object.assign(fetchParam, this.getSearchCond());
        }
        // 获取快速搜索里的搜索参数
        if (this.isEnableQuickSearch() && this.$searchValue !== undefined) {
            args.search = this.$searchValue;
        }
        Object.assign(args, fetchParam);
    }

    /**
     * 数据加载完成
     * 
     * @param {*} data 
     * @memberof IBizSearchViewController
     */
    public onStoreLoad(data: any): void {
        this.$parentDataChanged = false;
        this.reloadUICounters();
    }

    /**
     *设置父数据
     * 
     * @memberof IBizSearchViewController
     */
    public onSetParentData(): void {
        super.onSetParentData();
        this.$parentDataChanged = true;
    }

    /**
     * 数据被激活<最典型的情况就是行双击>
     * 
     * @memberof IBizSearchViewController
     */
    public onDataActivated(): void {

    }

    /**
     * 搜索表单打开
     *
     * @returns {void}
     * @memberof IBizSearchViewController
     */
    public openSearchForm(): void {
        if (!this.isEnableQuickSearch()) {
            return;
        }
        const searchForm = this.getSearchForm();
        if (searchForm) {
            searchForm.setOpen(!searchForm.$opened);
        }
    }

    /**
     * 搜索按钮执行搜索
     *
     * @memberof IBizSearchViewController
     */
    public btnSearch(): void {
        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched(true);
        }
    }

    /**
     * 清空快速搜索值
     *
     * @memberof IBizSearchViewController
     */
    public clearQuickSearchValue(): void {
        this.$searchValue = undefined;
        this.onRefresh();
    }

    /**
     * 执行快速搜索
     *
     * @param {*} $event
     * @returns {void}
     * @memberof IBizSearchViewController
     */
    public onQuickSearch($event: any): void {
        if (!$event || $event.keyCode !== 13) {
            return;
        }

        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched(true);
        }
    }
}
