import { Input } from '@angular/core';
import { IBizTreeViewController } from '@ibizsys/app/IBizTreeViewController';

/**
 * 树视图控制器（嵌入）
 *
 * @export
 * @class IBizTreeView9Controller
 * @extends {IBizTreeViewController}
 */
export class IBizTreeView9Controller extends IBizTreeViewController {

    /**
    * 父数据模型
    * 
    * @type {*}
    * @memberof IBizGridView9Controller
    */
    @Input()
    parentmode: any

    /**
     * 父数据主键
     * 
     * @type {string}
     * @memberof IBizGridView9Controller
     */
    @Input()
    srfparentkey: string;

    /**
     * 表单活动数据
     * 
     * @type {*}
     * @memberof IBizGridView9Controller
     */
    @Input()
    activeData: any;

    /**
     * 刷新表格设置
     * 
     * @memberof IBizGridView9Controller
     */
    @Input()
    set refreshGrid(count) {
        if (count > 0) {
            if (this.parentmode && this.parentmode.srfparenttype && Object.is(this.parentmode.srfparenttype, 'CUSTOM')) {
                this.addViewParam({ srfreferdata: JSON.stringify(this.activeData) });
                this.addViewParam({ srfparentkey: this.srfparentkey });
                this.onRefresh();
                return;
            }
            if (!this.srfparentkey || Object.is(this.srfparentkey, '')) {
                return;
            }
            if (!this.activeData || Object.is(this.activeData, '') || Object.keys(this.activeData).length === 0) {
                return;
            }

            this.addViewParam({ srfreferdata: JSON.stringify(this.activeData) });
            this.addViewParam({ srfparentkey: this.srfparentkey });
            this.onRefresh();
        }
    }

    /**
     * Creates an instance of IBizTreeView9Controller.
     * 创建 IBizTreeView9Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizTreeView9Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图初始化，处理视图父数据，清除其他参数
     *
     * @memberof IBizTreeView9Controller
     */
    public onInited(): void {
        super.onInited();
        if (this.parentmode && !Object.is(this.parentmode, '') && typeof this.parentmode === 'object') {
            this.addViewParam(this.parentmode);
        }

        if (this.$viewParam && this.$viewParam.srfkey) {
            delete this.$viewParam.srfkey;
        }
    }

    /**
     * 视图部件加载，不提供预加载方法
     *
     * @param {*} [opt={}]
     * @memberof IBizTreeView9Controller
     */
    public load(opt: any = {}): void {

    }

    /**
     * 获取树部件
     *
     * @returns {*}
     * @memberof IBizTreeView9Controller
     */
    public getTree(): any {
        return this.getControl('tree');
    }

    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizTreeView9Controller
     */
    public isEnableQuickSearch(): boolean {
        return false;
    }

    /**
     *  树部件数据选中
     *
     * @param {Array<any>} datas
     * @memberof IBizTreeView9Controller
     */
    public onSelectionChange(datas: Array<any>): void {
        super.onSelectionChange(datas);
    }

    /**
     * 树部件数据激活
     *
     * @param {Array<any>} datas
     * @memberof IBizTreeView9Controller
     */
    public onDataActivated(datas: Array<any>): void {
        super.onDataActivated(datas);
    }

    /**
     * 树部件数据加载完成
     *
     * @param {Array<any>} datas
     * @memberof IBizTreeView9Controller
     */
    public onTreeLoad(datas: Array<any>): void {
        super.onTreeLoad(datas);
    }
}