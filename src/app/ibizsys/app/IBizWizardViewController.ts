﻿import { IBizMainViewController } from '@ibizsys/app/IBizMainViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 向导编辑视图控制器
 * 
 * @export
 * @class IBizWizardViewController
 * @extends {IBizMainViewController}
 */
export class IBizWizardViewController extends IBizMainViewController {

    /**
     * Creates an instance of IBizWizardViewController.
     * 创建 IBizWizardViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizWizardViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 上一页
     * 
     * @memberof IBizWizardViewController
     */
    public goPrev(): void {
        if (this.getWizardPanel()) {
            this.getWizardPanel().goPrev();
        }
    }

    /**
     * 下一页
     * 
     * @memberof IBizWizardViewController
     */
    public goNext(): void {
        if (this.getWizardPanel()) {
            this.getWizardPanel().goNext();
        }
    }

    /**
     * 完成
     * 
     * @memberof IBizWizardViewController
     */
    public goFinal(): void {
        if (this.getWizardPanel()) {
            this.getWizardPanel().goFinal();
        }
    }

    /**
     * 获取向导面板对象
     * 
     * @returns {*} 
     * @memberof IBizWizardViewController
     */
    public getWizardPanel(): any {
        return this.getControl('wizardpanel');
    }

    /**
     * 注册向导面板事件
     * 
     * @returns {void} 
     * @memberof IBizWizardViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const wizardpanel = this.getWizardPanel();
        if (wizardpanel) {
            wizardpanel.on(IBizEvent.IBizWizardPanel_WIZARDINITED, (params) => {
                this.onWizardInited(params);
            });
            wizardpanel.on(IBizEvent.IBizWizardPanel_WIZARDFINISH, (params) => {
                this.onWizardFinish(params);
            });
            wizardpanel.on(IBizEvent.IBizWizardPanel_WIZARDCHANGEFORM, (params) => {
                this.onWizardFormChange();
            });
            wizardpanel.on(IBizEvent.IBizForm_FORMFIELDCHANGED, (params) => {
                if (params) {
                    this.onFieldValueChanged(params.name, params.value, params.field);
                } else {
                    this.onFieldValueChanged('', '', null);
                }
            });
        }
    }

    /**
     * 加载面板数据
     * 
     * @memberof IBizWizardViewController
     */
    public onLoad(): void {
        if (this.getWizardPanel()) {
            this.getWizardPanel().autoLoad(this.getViewParam());
        }
    }

    /**
     * 向导面板初始化完成
     * 
     * @param {*} param 
     * @memberof IBizWizardViewController
     */
    public onWizardInited(param: any): void {

    }

    /**
     * 向导步骤完成以后
     * 
     * @param {*} param 
     * @memberof IBizWizardViewController
     */
    public onWizardFinish(param: any): void {
        if (this.isModal()) {
            this.nzModalSubject.next({ ret: 'OK', data: {}, 'refreshView': true });
            this.nzModalSubject.next('DATACHANGE');
        }
        this.closeWindow();
    }

    /**
     * 改变当前操作表单时
     * 
     * @memberof IBizWizardViewController
     */
    public onWizardFormChange(): void {

    }

    /**
     * 表单项值变化事件
     * 
     * @param {string} name 
     * @param {*} value 
     * @returns {*} 
     * @memberof IBizEditViewController
     */
    public onFieldValueChanged(fieldname: string, value: any, field: any): void {

    }
}

