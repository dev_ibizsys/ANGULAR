import { IBizGridViewController } from '@ibizsys/app/IBizGridViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 表格视图控制器
 * 嵌入edit9视图
 * 
 * @export
 * @class IBizGridView2Controller
 * @extends {IBizGridViewController}
 */
export class IBizGridView2Controller extends IBizGridViewController {
    /**
     * 选中行数据
     * 
     * @type {*}
     * @memberof IBizGridView2Controller
     */
    public $selectItem: any = {};

    /**
     * 排序字段名称
     * 
     * @memberof IBizGridView2Controller
     */
    public $sortFieldName: string = '关键字';

    /**
     * 排序字段属性文本值
     * 
     * @memberof IBizGridView2Controller
     */
    public $sortField: string = '';

    /**
     * 属性字段排序方向
     * 
     * @type {string}
     * @memberof IBizGridView2Controller
     */
    public $sortDirection: string = 'desc';

    /**
     * Creates an instance of IBizGridView2Controller.
     * 创建 IBizGridView2Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizGridView2Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图显示数据类型
     * 
     * @memberof IBizGridView2Controller
     */
    public $gridType = 'listgrid';

    /**
     * 部件初始化
     * 
     * @memberof IBizGridView2Controller
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const listgrid: any = this.getControl('listgrid');
        if (listgrid) {

            listgrid.on(IBizEvent.IBizMDControl_SELECTIONCHANGE, (args) => {
                this.onSelectionChange(args);
            });
            listgrid.on(IBizEvent.IBizMDControl_BEFORELOAD, (args) => {
                this.onStoreBeforeLoad(args);
            });
            listgrid.on(IBizEvent.IBizMDControl_LOADED, (args) => {
                this.onStoreLoad(args);
            });
            listgrid.on(IBizEvent.IBizMDControl_CHANGEEDITSTATE, (args) => {
                this.onGridRowEditChange(undefined, args, undefined);
            });

            listgrid.on(IBizEvent.IBizDataGrid_ROWDBLCLICK, (args) => {
                this.onDataActivated(args);
            });
        }
    }

    /**
     * 视图部件加载
     * 
     * @param {*} [opt={}] 
     * @memberof IBizGridView2Controller
     */
    public load(opt: any = {}): void {
        super.load(opt);
        const listgrid: any = this.getControl('listgrid');
        if (listgrid) {
            listgrid.load(opt);
        }
    }

    /**
     * 部件加载完成
     * 
     * @param {Array<any>} args 
     * @memberof IBizGridView2Controller
     */
    public onStoreLoad(args: Array<any>): void {
        super.onStoreLoad(args);

        if (args.length > 0) {
            setTimeout(() => {
                this.$selectItem = {};
                Object.assign(this.$selectItem, { srfparentkey: args[args.length - 1].srfkey });
            }, 1);
        }
    }


    /**
     * 选中值变化
     * 
     * @param {any} selection 
     * @memberof IBizMDViewController
     */
    public onSelectionChange(args: Array<any>): void {

        if (args.length > 0) {
            this.$selectItem = {};
            Object.assign(this.$selectItem, { srfparentkey: args[args.length - 1].srfkey });
        }
        super.onSelectionChange(args);
    }

    /**
     * 数据列表排序
     * 
     * @param {string} field 
     * @param {string} name 
     * @memberof IBizGridView2Controller
     */
    public dataListSort(field: string, name: string): void {
        if (Object.is(this.$sortField, field)) {
            return;
        }

        this.$sortField = field;
        this.$sortFieldName = name;

        if (Object.is(this.$sortField, '')) {
            return;
        }
        let sort: Array<any> = [];
        sort.push({ property: this.$sortField, direction: this.$sortDirection });
        const listgrid: any = this.getControl('listgrid');
        listgrid.refresh({ sort: JSON.stringify(sort) });
    }

    /**
     * 视图类型切换
     * 
     * @param {string} type 
     * @memberof IBizGridView2Controller
     */
    public viewTypeChange(type: string): void {
        this.$gridType = type;
    }

    /**
     * 数据列表字段排序
     * 
     * @memberof IBizGridView2Controller
     */
    public dataListFiledSort(): void {
        if (Object.is(this.$sortField, '')) {
            return;
        }
        if (Object.is(this.$sortDirection, 'desc')) {
            this.$sortDirection = 'asc';
        } else {
            this.$sortDirection = 'desc';
        }

        let sort: Array<any> = [];
        sort.push({ property: this.$sortField, direction: this.$sortDirection });
        const listgrid: any = this.getControl('listgrid');
        listgrid.refresh({ sort: JSON.stringify(sort) });
    }
}
