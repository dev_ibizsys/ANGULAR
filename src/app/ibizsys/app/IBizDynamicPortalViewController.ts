import { IBizPortalViewController } from '@ibizsys/app/IBizPortalViewController';
import { ViewContainerRef, ViewChild } from '@angular/core';

/**
 * 动态portal视图控制器
 *
 * @export
 * @class IBizDynamicPortalViewController
 * @extends {IBizPortalViewController}
 */
export class IBizDynamicPortalViewController extends IBizPortalViewController {

    @ViewChild('dynamicDashboard', { read: ViewContainerRef })
    $container: ViewContainerRef;

    /**
     * Creates an instance of IBizDynamicPortalViewController.
     * 创建 IBizDynamicPortalViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizDynamicPortalViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }


    

    
}


