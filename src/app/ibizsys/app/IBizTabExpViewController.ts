import { IBizMainViewController } from '@ibizsys/app/IBizMainViewController';

/**
 * 分页导航视图控制器
 * 
 * @export
 * @class IBizTabExpViewController
 * @extends {IBizMainViewController}
 */
export class IBizTabExpViewController extends IBizMainViewController {

    /**
     * Creates an instance of IBizTabExpViewController.
     * 创建 IBizTabExpViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizTabExpViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

