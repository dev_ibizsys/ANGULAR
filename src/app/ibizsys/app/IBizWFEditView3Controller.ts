import { IBizEditView3Controller } from '@ibizsys/app/IBizEditView3Controller';

/**
 * 工作流分页编辑视图
 * 
 * @export
 * @class IBizWFEditView3Controller
 * @extends {IBizEditView3Controller}
 */
export class IBizWFEditView3Controller extends IBizEditView3Controller {

    /**
     * Creates an instance of IBizWFEditView3Controller.
     * 创建 IBizWFEditView3Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizWFEditView3Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
