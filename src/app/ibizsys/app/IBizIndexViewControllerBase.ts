import { MenuService } from '@delon/theme';
import { IBizMainViewController } from '@ibizsys/app/IBizMainViewController';

/**
 * 首页应用视图基类，处理主题菜单服务对象
 * 
 * @export
 * @class IBizIndexViewControllerBase
 * @extends {IBizMainViewController}
 */
export class IBizIndexViewControllerBase extends IBizMainViewController {

    /**
     * 菜单服务对象
     *
     * @private
     * @type {MenuService}
     * @memberof IBizIndexViewControllerBase
     */
    private $menuService: MenuService;

    /**
     * Creates an instance of IBizIndexViewControllerBase.
     * 创建 IBizIndexViewControllerBase 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizIndexViewControllerBase
     */
    constructor(opts: any = {}) {
        super(opts);
        this.$menuService = opts.menuService;
    }

    /**
     * 应用菜单部件加载完成
     * 
     * @param {any[]} items 
     * @memberof IBizIndexViewControllerBase
     */
    public appMenuLoaded(items: any[]): void {
        if (this.$menuService) {
            this.$menuService.clear();
            this.$menuService.add(items);
        }
    }

    /**
     * 更新菜单项
     *
     * @param {*} item
     * @memberof IBizIndexViewControllerBase
     */
    public menuItemUpdate(item: any): void {
        if (this.$menuService) {
            this.$menuService.resume();
        }
    }
}
