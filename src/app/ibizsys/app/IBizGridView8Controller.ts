﻿import { IBizGridViewController } from '@ibizsys/app/IBizGridViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';
import { Input } from '@angular/core';

/**
 * 表格视图控制
 * 
 * @export
 * @class IBizGridViewController
 * @extends {IBizGridView8Controller}
 */
export class IBizGridView8Controller extends IBizGridViewController {

    /**
     * 父数据模型
     * 
     * @type {*}
     * @memberof IBizGridView8Controller
     */
    @Input()
    parentmode: any

    /**
     * 父数据主键
     * 
     * @type {string}
     * @memberof IBizGridView8Controller
     */
    @Input()
    srfparentkey: string;

    /**
     * 表单活动数据
     * 
     * @type {*}
     * @memberof IBizGridView8Controller
     */
    @Input()
    activeData: any;

    /**
     * Creates an instance of IBizGridViewController.
     * 创建 IBizGridViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizGridView8Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图初始化, 处理视图父数据，清除其他参数
     * 
     * @memberof IBizGridView8Controller
     */
    public onInited(): void {
        super.onInited();
        if (this.parentmode && !Object.is(this.parentmode, '') && typeof this.parentmode === 'object') {
            this.addViewParam(this.parentmode);
        }

        if (this.$viewParam && this.$viewParam.srfkey) {
            delete this.$viewParam.srfkey;
        }
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizGridView8Controller
     */
    public onInitComponents(): void {
        super.onInitComponents();

        const grid = this.getMDCtrl();
        if (grid) {
            // 数据删除成功
            grid.on(IBizEvent.IBizDataGrid_REMOVED, (args) => {
                this.onDataRemoved(args);
            });
            // 数据添加成功
            grid.on(IBizEvent.IBizDataGrid_ADDBATCHED, (args) => {
                this.onDataAddBatched(args);
            });
        }

        const totalgrid = this.getTotalGrid();
        if (totalgrid) {
            // 双击行数据
            totalgrid.on(IBizEvent.IBizDataGrid_ROWDBLCLICK, (args) => {
                this.onSelectionChange(args);
                if (this.getGridRowActiveMode() === 0) {
                    return;
                }
                this.onDataActivated(args[0]);
            });
            // 单击行数据
            totalgrid.on(IBizEvent.IBizDataGrid_ROWCLICK, (args) => {
                this.onSelectionChange(args);
                if (this.getGridRowActiveMode() === 1) {
                    this.onDataActivated(args[0]);
                }
            });
            // 多数据部件选中
            totalgrid.on(IBizEvent.IBizMDControl_SELECTIONCHANGE, (args) => {
                this.onSelectionChange(args);
            });
            // 多数据部件加载之前
            totalgrid.on(IBizEvent.IBizMDControl_BEFORELOAD, (args) => {
                this.onStoreBeforeLoad(args);
            });
            // 多数据部件加载完成
            totalgrid.on(IBizEvent.IBizMDControl_LOADED, (args) => {
                this.onStoreLoad(args);
            });
            // 多数据界面行为
            totalgrid.on(IBizEvent.IBizMDControl_UIACTION, (agrs) => {
                if (agrs.tag) {
                    this.doUIAction(agrs.tag, agrs.data);
                }
            });
        }
    }

    /**
     * 多数据视图加载，加载部件
     * 
     * @memberof IBizGridView8Controller
     */
    public onLoad(): void {

    }

    /**
     * 视图刷新
     * 
     * @memberof IBizGridView8Controller
     */
    public onRefresh(): void {
        super.onRefresh();
        if (this.getTotalGrid()) {
            this.getTotalGrid().load();
        }
    }

    /**
     * 获取表格部件对象
     * 
     * @returns {*} 
     * @memberof IBizGridView8Controller
     */
    public getTotalGrid(): any {
        // return this.datagrid;
        return this.getControl('totalgrid');
    }

    /**
     * 获取搜索表单对象
     * 
     * @returns {*} 
     * @memberof IBizGridView8Controller
     */
    public getTotalSearchForm(): any {
        return this.getControl('totalsearchform');
    }

    /**
     * 搜索表单搜索执行
     * 
     * @param {boolean} isload 是否加载数据
     * @memberof IBizGridView8Controller
     */
    public onTotalSearchFormSearched(isload: boolean): void {
        if (this.getTotalGrid() && isload) {
            this.getTotalGrid().setCurPage(1);
            this.getTotalGrid().load();
        }
    }

    /**
     * 搜索表单重置完成
     * 
     * @memberof IBizGridView8Controller
     */
    public onTotalSearchFormReseted(): void {
        if (this.getTotalGrid()) {
            this.getTotalGrid().load();
        }
    }

    /**
     * 搜索表单属性值发生变化
     * 
     * @param {string} fieldname 
     * @param {*} field 
     * @param {*} value 
     * @memberof IBizGridView8Controller
     */
    public onTotalSearchFormFieldChanged(fieldname: string, field: any, value: any): void {

    }

    /**
     * 数据删除成功
     *
     * @param {*} [args={}]
     * @memberof IBizGridView8Controller
     */
    public onDataRemoved(args: any = {}) {
        if (this.getTotalGrid()) {
            this.getTotalGrid().load();
        }
    }

    /**
     * 数据删除成功
     *
     * @param {*} [args={}]
     * @memberof IBizGridView8Controller
     */
    public onDataAddBatched(args: any = {}) {
        if (this.getTotalGrid()) {
            this.getTotalGrid().load();
        }
    }

    /**
     * 隐藏关系列
     * 
     * @param {any} parentMode 
     * @memberof IBizGridView8Controller
     */
    public doHideParentColumns(parentMode: any): void {
    }

    /**
     * 隐藏列
     * 
     * @param {any} columnname 
     * @memberof IBizGridView8Controller
     */
    public hideTotalGridColumn(columnname: any): void {

    }

    /**
     * 刷新表格设置
     * 
     * @memberof IBizGridView8Controller
     */
    @Input()
    set refreshGrid(count) {
        if (count > 0) {
            if (this.parentmode && this.parentmode.srfparenttype && Object.is(this.parentmode.srfparenttype, 'CUSTOM')) {
                this.activeData.srfparentkey = this.srfparentkey;
                this.addViewParam({ srfreferdata: JSON.stringify(this.activeData) });
                this.addViewParam({ srfparentkey: this.srfparentkey });
                this.onRefresh();
                return;
            }
            if (!this.srfparentkey || Object.is(this.srfparentkey, '')) {
                return;
            }
            if (!this.activeData || Object.is(this.activeData, '') || Object.keys(this.activeData).length === 0) {
                return;
            }
            this.activeData.srfparentkey = this.srfparentkey;
            this.addViewParam({ srfreferdata: JSON.stringify(this.activeData) });
            this.addViewParam({ srfparentkey: this.srfparentkey });
            this.onRefresh();
        }
    }

    /**
     * 删除所有数据
     *
     * @returns
     * @memberof IBizGridView8Controller
     */
    public removeAllData() {
        let arg: any = {};
        const grid = this.getGrid();
        if (grid) {
            if (this.getParentMode()) {
                Object.assign(arg, this.getParentMode());
            }

            if (this.getParentData()) {
                Object.assign(arg, this.getParentData());
            }

            const allDatas: Array<any> = grid.getItems();
            if (!allDatas || allDatas == null || allDatas.length === 0) {
                return;
            }
            let keys = '';
            allDatas.forEach((record) => {
                let key = record.srfkey;
                if (!Object.is(keys, '')) {
                    keys += ';';
                }
                keys += key;
            });
            arg.srfkeys = keys;

            grid.remove(arg);
        }
    }
}