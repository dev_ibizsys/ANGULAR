import { IBizEditViewController } from '@ibizsys/app/IBizEditViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 实体索引视图控制器
 * 
 * @export
 * @class IBizEntityIndexViewController
 * @extends {IBizEditViewController}
 */
export class IBizEntityIndexViewController extends IBizEditViewController {
    /**
     * 默认选中项
     * 
     * @memberof IBizEditView2Controller
     */
    public $selectIndex = 0;

    /**
     * Creates an instance of IBizEntityIndexViewController.
     * 创建 IBizEntityIndexViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizEntityIndexViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化部件
     * 
     * @memberof IBizEditView2Controller
     */
    public onInitComponents(): void {
        super.onInitComponents();

        const drBar: any = this.getDRBar();
        if (drBar) {
            drBar.on(IBizEvent.IBizDRBar_DRBARLOADED, (item) => {
                this.DRBarLoaded(item);
            });
            drBar.on(IBizEvent.IBizDRBar_DRBARSELECTCHANGE, (item) => {
                this.DRBarItemChangeSelect(item);
            });
        }
    }

    /**
     * 数据项加载完成
     * 
     * @private
     * @param {any} item 
     * @memberof IBizEditView2Controller
     */
    private DRBarLoaded(item: Array<any>): void {
        // 默认加载第一项
        this.doDRData(item);
    }

    /**
     * 处理关系数据栏，保证加载第一项
     * 
     * @private
     * @param {Array<any>} items 
     * @returns {boolean} 
     * @memberof IBizEntityIndexViewController
     */
    private doDRData(items: Array<any>): boolean {
        let hasItem = false;
        items.forEach((data) => {
            if (hasItem) {
                return;
            }
            let routeName = data.id;
            if (this.hasRoute(routeName)) {
                this.DRBarItemChangeSelect(data);
                hasItem = true;
            }
            if (data.items) {
                let subItem = this.doDRData(data.items);
                if (subItem) {
                    hasItem = true;
                }
            }
        });
        return hasItem;
    }


    /**
     * 数据项选中变化
     * 
     * @private
     * @param {*} [item={}] 
     * @memberof IBizEditView2Controller
     */
    private DRBarItemChangeSelect(item: any = {}): void {
        if (this.$route) {
            // tslint:disable-next-line:prefer-const
            let viewParam: any = item.dritem.viewparam;

            if (!Object.is(item.id, 'form')) {
                this.$selectIndex = 10;
                const form = this.getForm();
                if (!form) {
                    this.$notification.error('错误', '视图异常，未配置视图默认表单');
                    return;
                }
                if (form) {
                    const srfkey = form.$data.srfkey;
                    // viewParam.srfparentid = srfkey;
                    if (!srfkey || Object.is(srfkey, '')) {
                        this.$notification.error('错误', '视图主键不存在');
                        return;
                    }
                    viewParam.srfparentkey = srfkey;
                }
            } else {
                this.$selectIndex = 0;
            }

            this.$route.navigate([item.id, viewParam], { relativeTo: this.$routeActive });
        }
    }

    /**
     * 视图加载，触发部件加载
     * 
     * @memberof IBizEditView2Controller
     */
    public onLoad(): void {
        if (this.$viewParam) {
            if (!this.$viewParam.srfkey || Object.is(this.$viewParam.srfkey, '')) {
                this.$notification.error('错误', '视图主键不存在');
                return;
            }
        }
        const drBar: any = this.getDRBar();
        if (drBar) {
            drBar.load();
        }
        super.onLoad();
    }




    /**
     * 获取数据关系部件
     * 
     * @returns {IBizDRBarService} 
     * @memberof IBizEditView2Controller
     */
    public getDRBar(): any {
        return this.getControl('drbar');
    }


    /**
    * 节点路由是否存在
    * 
    * @param {string} routeLink 
    * @returns {boolean} 
    * @memberof IBizExpViewController
    */
    public hasRoute(routeLink: string): boolean {
        let hasRoute = false;
        if (this.$routeActive && this.$routeActive.routeConfig && this.$routeActive.routeConfig.children != null) {
            const router: Array<any> = this.$routeActive.routeConfig.children.filter(item => Object.is(item.path, routeLink));

            if (router.length > 0) {
                hasRoute = true;
            }
        }
        return hasRoute;
    }

    /**
     * 解析路由状态，保证路由自持
     * 
     * @memberof IBizEditView2Controller
     */
    public goBack(): void {
        if (!this.$location) {
            return;
        }
        let routeSeat = 0;
        let findRouteState = false;
        // tslint:disable-next-line:prefer-const
        let routeString: string = this.$location.path(true);
        const routeArr: Array<any> = routeString.split('/');
        routeArr.forEach((item, index) => {
            if (findRouteState) {
                return;
            }
            const routeData: string = item;
            if (this.$routeActive.routeConfig.children == null) {
                return;
            }
            this.$routeActive.routeConfig.children.forEach(subRoute => {
                if (findRouteState) {
                    return;
                }
                if (routeData.includes(subRoute.path)) {
                    routeSeat = index;
                    findRouteState = true;
                }
            });
        });

        let subArr: Array<any> = [];

        if (routeSeat > 0) {
            subArr = routeArr.filter((item, index) => index < routeSeat - 1);
        } else {
            subArr = routeArr.filter((item, index) => index < routeArr.length - 1);
        }
        this.$route.navigate([subArr.join('/')]);
    }
}

