import { IBizMDViewController } from '@ibizsys/app/IBizMDViewController';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 工作流流程图页面
 * 
 * @export
 * @class IBizWFStepActorViewController
 * @extends {IBizMDViewController}
 */
export class IBizWFStepActorViewController extends IBizMDViewController {

    /**
     * Creates an instance of IBizWFStepActorViewController.
     * 创建 IBizWFStepActorViewController 实例对象
     * 
     * @param {*} [opts={}] 
     * @memberof IBizWFStepActorViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }


    /**
     * 部件初始化
     * 
     * @memberof IBizWFStepActorViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        let WFStepActor: any = this.getWFStepActor();
        if (WFStepActor) {
            WFStepActor = this.getMDCtrl();
            if (WFStepActor) {
                WFStepActor.on(IBizEvent.IBizMDControl_SELECTIONCHANGE, (args) => {
                    this.onSelectionChange(args);
                });
                WFStepActor.on(IBizEvent.IBizMDControl_BEFORELOAD, (args) => {
                    this.onStoreBeforeLoad(args);
                });
                WFStepActor.on(IBizEvent.IBizMDControl_LOADED, (args) => {
                    this.onStoreLoad(args);
                });
                WFStepActor.on(IBizEvent.IBizMDControl_CHANGEEDITSTATE, (args) => {
                    this.onGridRowEditChange(undefined, args, undefined);
                });
            }
        }
    }


    /**
     * 部件加载
     * 
     * @memberof IBizWFStepActorViewController
     */
    public onLoad(): void {
        super.onLoad();
        const WFStepActor: any = this.getWFStepActor();
        if (WFStepActor) {
            WFStepActor.load(this.getViewParam());
        }
    }


    /**
     * 获取数据部件
     * 
     * @returns {*} 
     * @memberof IBizWFStepActorViewController
     */
    public getWFStepActor(): any {
        return this.getControl('grid2');
    }


    /**
     * 获取表格部件
     * 
     * @returns {*} 
     * @memberof IBizWFStepActorViewController
     */
    public getMDCtrl(): any {
        return this.getControl('grid');
    }

    /**
     * 获取搜索表单对象
     * 
     * @returns {*} 
     * @memberof IBizWFStepActorViewController
     */
    public getSearchForm(): any {
        return this.getControl('searchform');
    }
}