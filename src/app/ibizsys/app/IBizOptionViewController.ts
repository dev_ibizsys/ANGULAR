import { IBizEditViewController } from '@ibizsys/app/IBizEditViewController';

/**
 * 实体选项视图控制器
 * 
 * @export
 * @class IBizOptionViewController
 * @extends {IBizEditViewController}
 */
export class IBizOptionViewController extends IBizEditViewController {

    /**
     * Creates an instance of IBizOptionViewController.
     * 创建 IBizOptionViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizOptionViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 确定
     * 
     * @memberof IBizOptionViewController
     */
    public onClickOkButton(): void {
        this.doSaveAndExit();
    }

    /**
     * 关闭串口
     * 
     * @memberof IBizOptionViewController
     */
    public onClickCancelButton(): void {
        // var me = this;
        // var window = me.window;
        // if(window){
        // 	window.dialogResult = 'cancel';
        // }
        // me.closeWindow();
        // this.closeView();
        this.closeWindow();
    }

    /**
     * 视图加载
     *
     * @memberof IBizOptionViewController
     */
    public onLoad(): void {
        this.loadModel();
        const editForm: any = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    }

    /**
     * 表单保存完成
     * 
     * @returns {void} 
     * @memberof IBizOptionViewController
     */
    public onFormSaved(): void {
        // var me = this;
        // var window = me.window;
        // if (window) {
        // 	window.dialogResult = 'ok';
        // 	window.activeData = me.getForm().getValues();
        // 	window.selectedData = window.activeData
        // }
        // me.closeWindow();
        this.refreshReferView();
        // this.closeView();
        this.closeWindow();
        return;
    }
}