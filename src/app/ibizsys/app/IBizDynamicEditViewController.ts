import { IBizEditViewController } from './IBizEditViewController';

export class IBizDynamicEditViewController extends IBizEditViewController {

    constructor(opt: any) {
        super(opt);
    }

    /**
     * 获取工具栏控制器
     *
     * @returns {*}
     * @memberof IBizDynamicGridViewController
     */
    public getToolBar(): any {
        return this.getControl('dynamicToolbar');
    }

    /**
     * 加载数据
     *
     * @memberof IBizDynamicEditViewController
     */
    public onLoad(): void {
        this.setDynamicParams(this.getViewParam());
        super.onLoad();
    }

    /**
     * 模型加载完毕
     *
     * @protected
     * @param {*} model
     * @memberof IBizDynamicEditView3Controller
     */
    protected afterLoadMode(model: any): void {
        this.$dynamicViewModalService.setAllModalService(this.getAllModalService());
        this.destroyDynamicComponents();
        if (model) {
            this.$viewModel = model;
            if (model.ctrls) {
                const ctrls: any[] = model.ctrls;
                ctrls.forEach((item: any) => {
                    if (Object.is(item.type, 'TOOLBAR')) {
                        this.initToolbarModel([...item.items]);
                    }
                    // else if (item.hasOwnProperty('pages')) {
                    //     if (item.hasOwnProperty('hiddens')) {
                    //         this.regHiddenFields(item.hiddens);
                    //     }
                    //     this.initFormModel(item.pages);
                    // }
                });
            }
            // 注册界面行为
            if (model.uiactions) {
                this.initUiactions(model.uiactions);
            }
        }
    }

    /**
     * 
     *
     * @param {string} name
     * @returns
     * @memberof IBizDynamicEditViewController
     */
    public getModalService(name: string) {
        return this.$modalServiceAll[name];
    }

    /**
     * 是否是动态视图
     *
     * @returns {boolean}
     * @memberof IBizDynamicEditViewController
     */
    public isDynamicView(): boolean {
        return true;
    }

}