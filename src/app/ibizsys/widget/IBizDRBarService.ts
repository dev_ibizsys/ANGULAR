﻿import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 数据关系区(IBizEditView2)
 * 
 * @export
 * @class IBizDRBarService
 * @extends {IBizService}
 */
export class IBizDRBarService extends IBizService {

    /**
     * 关系部件是否收缩，默认展开
     * 
     * @type {boolean}
     * @memberof IBizDRBarService
     */
    public $isCollapsed: boolean = true;

    /**
     * 关系数据项
     * 
     * @type {Array<any>}
     * @memberof IBizDRBarService
     */
    public $items: Array<any> = [];

    /**
     * 选中项
     * 
     * @type {*}
     * @memberof IBizDRBarService
     */
    public $selectItem: any = {};

    /**
     * 默认选中项
     * 
     * @type {*}
     * @memberof IBizDRBarService
     */
    public $defaultItem: any = {};

    /**
     * Creates an instance of IBizDRBarService.
     * 创建IBizDRBarService实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizDRBarService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 加载关系数据
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizDRBarService
     */
    public load(arg: any = {}): void {
        if (!this.$IBizHttp) {
            return;
        }
        let param: any = {};
        param.srfaction = 'fetch';
        param.srfctrlid = this.getName();
        Object.assign(param, arg);

        this.$IBizHttp.post(this.getBackendUrl(), param).subscribe(result => {
            if (result.ret === 0) {
                this.$items = result.items;
                this.doSelectItem(this.$items);
                this.fire(IBizEvent.IBizDRBar_DRBARLOADED, this.$items);
            }
        }, error => {
            console.log(error);
        });
    }


    /**
     * 获取默认选中项
     * 
     * @private
     * @param {Array<any>} _items 
     * @returns {*} 
     * @memberof IBizDRBarService
     */
    private doSelectItem(_items: Array<any>): any {
        let checkState = false;
        _items.forEach(item => {
            if (item.checked) {
                item.bchecked = true;
                checkState = true;
                Object.assign(this.$defaultItem, item);
            } else {
                item.bchecked = false;
            }

            if (item.items) {
                const hasItemCheck = this.doSelectItem(item.items);
                if (hasItemCheck) {
                    item.expanded = true;
                }
                item.hassubmenu = true;
            } else {
                item.hassubmenu = false;
            }
            item.expanded = true;

        });
    }

    /**
     * 菜单节点选中处理
     * 
     * @param {*} [item={}] 
     * @returns {void} 
     * @memberof IBizDRBarService
     */
    public expandedAndSelectSubMenu(item: any = {}): void {
        if (!item.expanded) {
            this.closeSubItems(item);
        }

        const viewController: any = this.getViewController();
        let routeString: string = item.id;

        if (Object.is(routeString.toLowerCase(), 'form')) {
            this.fire(IBizEvent.IBizDRBar_DRBARSELECTCHANGE, item);
        } else {
            if (viewController) {
                const hasRoute: boolean = viewController.hasRoute(routeString.toLowerCase());
                if (hasRoute) {
                    this.fire(IBizEvent.IBizDRBar_DRBARSELECTCHANGE, item);
                }
            }
        }
    }

    /**
     * 收缩子菜单
     * 
     * @param {*} [item={}] 
     * @memberof IBizDRBarService
     */
    public closeSubItems(item: any = {}): void {
        item.expanded = true;
        item.items.forEach(subItem => {
            if (subItem.items && subItem.items.length > 0) {
                this.closeSubItems(subItem);
            }
        });
    }


    /**
     * 菜单项选中事件
     * 
     * @param {*} item 
     * @returns {void} 
     * @memberof IBizTreeExpBarService
     */
    public selection(item: any): void {

        if (item.items && item.items.length > 0) {
            return;
        }

        const viewController: any = this.getViewController();
        let routeString: string = item.id;

        if (routeString) {
            const hasRoute = viewController.hasRoute(routeString.toLowerCase());
            if (!hasRoute) {
                return;
            }
            this.fire(IBizEvent.IBizDRBar_DRBARSELECTCHANGE, item);
        }
    }

    /**
     * 重新加载关系数据
     * 
     * @memberof IBizDRBarService
     */
    public reLoad(arg: any = {}): void {
        this.load(arg);
    }

    /**
     * 获取所有关系数据项
     * 
     * @returns {Array<any>} 
     * @memberof IBizDRBarService
     */
    public getItems(): Array<any> {
        return this.$items;
    }

    /**
     * 获取某一数据项
     * 
     * @param {string} id 
     * @param {Array<any>} _arr 
     * @returns {*} 
     * @memberof IBizDRBarService
     */
    public getItem(id: string, _arr: Array<any>): any {
        let _item: any = {};
        _arr.some((item: any) => {
            if (Object.keys(_item).length !== 0) {
                return false;
            }
            if (Object.is(id.toLocaleLowerCase(), item.id.toLocaleLowerCase())) {
                Object.assign(_item, item);
                return true;
            }
            if (!id && !Object.is(item.id.toLocaleLowerCase(), 'form')) {
                Object.assign(_item, item);
                return true;
            }

            if (item.items) {
                const _subItem: any = this.getItem(id, item.items);
                if (Object.keys(_subItem).length === 0) {
                    return false;
                }
                Object.assign(_item, _subItem);
                return true;
            }
        });
        return _item;
    }

    /**
     * 设置关系数据默认选中项
     * 
     * @param {*} [_item={}] 
     * @memberof IBizDRBarService
     */
    public setDefaultSelectItem(_item: any = {}): void {
        if (!Object.is(this.$selectItem.id, _item.id)) {
            this.$selectItem = {};
            Object.assign(this.$selectItem, _item);
        }
    }

}