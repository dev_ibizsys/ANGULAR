import { IBizService } from '@ibizsys/widget/IBizService';

/**
 * 多编辑视图面板部件服务对象
 *
 * @export
 * @class IBizMultiEditViewPanelService
 * @extends {IBizService}
 */
export class IBizMultiEditViewPanelService extends IBizService {

    /**
     * Creates an instance of IBizMultiEditViewPanelService.
     * 创建 IBizMultiEditViewPanelService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizMultiEditViewPanelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据记载
     *
     * @param {*} [params={}]
     * @memberof IBizMultiEditViewPanelService
     */
    public load(params: any = {}): void {
        let opt: any = {};
        if (params) {
            Object.assign(opt, params);
        }
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch', start: 0, limit: 500 });
        this.post(opt).subscribe(success => {
            console.log(success);
        }, error => {
            console.log(error);
        });
    }
}