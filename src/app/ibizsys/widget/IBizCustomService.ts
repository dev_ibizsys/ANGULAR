import { IBizService } from '@ibizsys/widget/IBizService';

/**
 * 自定义部件服务对象
 * 
 * @export
 * @class IBizCustomService
 * @extends {IBizService}
 */
export class IBizCustomService extends IBizService {

    /**
     * Creates an instance of IBizCustomService.
     * 创建 IBizCustomService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizCustomService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

