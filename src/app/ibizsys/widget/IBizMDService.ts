﻿import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';
import { IBizUtil } from '@ibizsys/util/IBizUtil';
import { IBizImportdataViewComponent } from '@ibizsys/components/ibiz-importdata-view/ibiz-importdata-view.component';

/**
 * 多项数据部件服务对象
 * 
 * @export
 * @class IBizMDService
 * @extends {IBizService}
 */
export class IBizMDService extends IBizService {

    /**
     * 多数据列头
     *
     * @type {*}
     * @memberof IBizMDService
     */
    public $columns: any = {};

    /**
     * 所有数据项
     * 
     * @type {Array<any>}
     * @memberof IBizMDService
     */
    public $items: Array<any> = [];

    /**
     * 选中数据项
     * 
     * @type {Array<any>}
     * @memberof IBizMDService
     */
    public $selection: Array<any> = [];

    /**
     * 加载状态
     * 
     * @memberof IBizMDService
     */
    public $loading = false;

    /**
     * Creates an instance of IBizMDService.
     * 创建 IBizMDService 实例 
     * 
     * @param {*} [opts={}] 
     * @memberof IBizMDService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.regColumns();
    }

    /**
     * 加载数据
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizMDService
     */
    public load(arg: any = {}): void {

    }

    /**
     * 刷新数据
     * @param arg 
     */
    public refresh(arg: any = {}): void {

    }

    /**
     * 设置选中项
     * 
     * @param {Array<any>} selection 
     * @memberof IBizMDService
     */
    public setSelection(selection: Array<any>): void {
        this.$selection = selection;
        this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);
    }

    /**
     * 选中对象
     * 
     * @param {*} [item={}] 
     * @returns {void} 
     * @memberof IBizMDService
     */
    public clickItem(item: any = {}): void {
        if (this.$loading) {
            return;
        }
        this.setSelection([item]);
    }

    /**
     * 
     * 
     * @param {any} item 
     * @memberof IBizMDService
     */
    public activeItem(item): void {

    }

    /**
     * 
     * 
     * @returns {boolean} 
     * @memberof IBizMDService
     */
    public isloading(): boolean {
        return this.$loading;
    }

    /**
     * 获取列表中某条数据
     * 
     * @param {string} name 字段
     * @param {string} value 名称
     * @returns {*} 
     * @memberof IBizMDService
     */
    public findItem(name: string, value: string): any {
        let item: any;
        this.$items.forEach((element: any) => {
            if (Object.is(element[name], value)) {
                item = element;
                return;
            }
        });
        return item;
    }

    /**
     * 删除数据
     * 
     * @param {*} [arg={}] 
     * @memberof IBizMDService
     */
    public remove(arg: any = {}): void {

    }

    /**
     * 单选
     * 
     * @memberof IBizMDService
     */
    public onItemSelect(value: boolean, item: any): void {

    }

    /**
     * 全选
     * 
     * @param {boolean} value 
     * @memberof IBizMDService
     */
    public selectAll(value: boolean): void {

    }

    /**
     * 获取选中行
     * 
     * @returns {Array<any>} 
     * @memberof IBizMDService
     */
    public getSelection(): Array<any> {
        return this.$selection;
    }

    /**
     * 工作流提交
     * 
     * @param {*} [params={}] 
     * @memberof IBizMDService
     */
    public wfsubmit(params: any = {}): void {
        if (!params) {
            params = {};
        }
        Object.assign(params, { srfaction: 'wfsubmit', srfctrlid: this.getName() });
        this.post(params, this.getBackendUrl()).subscribe((data) => {
            if (data.ret === 0) {
                this.refresh();
            } else {
                this.showToast(this.$showErrorToast, '', '执行工作流操作失败,' + data.info);
            }
        }, (error) => {
            this.showToast(this.$showErrorToast, '', '执行工作流操作失败,' + error.info);
        });
    }

    /**
     * 实体界面行为
     * 
     * @param {*} [params={}] 
     * @memberof IBizMDService
     */
    public doUIAction(arg: any = {}): void {
        let params: any = {};
        if (arg) {
            Object.assign(params, arg);
        }
        Object.assign(params, { srfaction: 'uiaction', srfctrlid: this.getName() });
        this.post(params, this.getBackendUrl()).subscribe((data) => {
            if (data.ret === 0) {
                if (data.reloadData) {
                    this.refresh();
                }
                if (data.info && !Object.is(data.info, '')) {
                    this.showToast(this.$showInfoToast, '', data.info);
                }
                IBizUtil.processResult(data);
            } else {
                this.showToast(this.$showErrorToast, '操作失败', '操作失败,执行操作发生错误,' + data.info);
            }
        }, (error) => {
            this.showToast(this.$showErrorToast, '操作失败', '操作失败,执行操作发生错误,' + error.info);
        });
    }

    /**
     * 批量添加
     * 
     * @param {*} [arg={}] 
     * @memberof IBizMDService
     */
    public addBatch(arg: any = {}): void {
        let params: any = {};
        if (arg) {
            Object.assign(params, arg);
        }

        Object.assign(params, { srfaction: 'addbatch', srfctrlid: this.getName() });
        this.$IBizHttp.post(this.getBackendUrl(), params).subscribe((data) => {
            if (data.ret === 0) {
                this.refresh();
                this.fire(IBizEvent.IBizDataGrid_ADDBATCHED, data);
            } else {
                this.showToast(this.$showErrorToast, '添加失败', '执行批量添加失败,' + data.info);
            }
        }, (error) => {
            this.showToast(this.$showErrorToast, '添加失败', '执行批量添加失败,' + error.info);
        });
    }

    /**
     * 获取所有数据项
     * 
     * @returns {Array<any>} 
     * @memberof IBizMDService
     */
    public getItems(): Array<any> {
        return this.$items;
    }

    /**
     * 注册多数据列头
     * 
     * @memberof IBizMDService
     */
    public regColumns(): void {

    }

    /**
     * 获取多数据列头
     *
     * @returns {*}
     * @memberof IBizMDService
     */
    public getColumns(): any {
        return this.$columns;
    }

    /**
     * 设置多数据列头
     * 
     * @param {*} [column={}] 
     * @returns {void} 
     * @memberof IBizMDService
     */
    public regColumn(column: any = {}): void {
        if (Object.keys(column).length === 0) {
            return;
        }
        if (!this.$columns) {
            this.$columns = {};
        }
        this.$columns[column.name] = column;
    }

    /**
     * 多数据项界面_数据导入栏
     * 
     * @memberof IBizMDService
     */
    public doImportData(name: string): void {
        if (Object.is(name, '')) {
            return;
        }
        this.nzModalService.open({
            content: IBizImportdataViewComponent,
            wrapClassName: 'ibiz_wrap_modal',
            componentParams: { dename: name },
            footer: false,
            maskClosable: false,
            width: 500,
        }).subscribe((result) => {
            if (result && result.ret) {
                this.refresh();
            }
        });
    }

    /**
     * 界面行为
     *
     * @param {string} tag
     * @param {*} [data={}]
     * @memberof IBizMDService
     */
    public uiAction(tag: string, data: any = {}) {

    }

    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizMDService
     */
    public rendererDatas(items: Array<any>): Array<any> {
        return items;
    }
}

