import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 向导面板部件
 * 
 * @export
 * @class IBizWizardPanelService
 * @extends {IBizService}
 */
export class IBizWizardPanelService extends IBizService {

    /**
     * 所有向导表单
     *
     * @type {*}
     * @memberof IBizWizardPanelService
     */
    public $wizardForms: any = {};

    /**
     * 所有表单ID
     *
     * @type {string[]}
     * @memberof IBizWizardPanelService
     */
    public $formids: string[] = [];

    /**
     * 当前表单
     *
     * @type {*}
     * @memberof IBizWizardPanelService
     */
    public $curform: any;

    /**
     * 当前表单ID
     *
     * @memberof IBizWizardPanelService
     */
    public $curformId = '';

    /**
     * 表达界面行为
     *
     * @type {*}
     * @memberof IBizWizardPanelService
     */
    public $formactions: any = {};

    /**
     * 参数
     *
     * @type {*}
     * @memberof IBizWizardPanelService
     */
    public $param: any = {};

    /**
     * 当前步骤
     *
     * @type {string}
     * @memberof IBizWizardPanelService
     */
    public $curstep: string;

    /**
     * Creates an instance of IBizWizardPanelService. 
     * 创建 IBizWizardPanelService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizWizardPanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.init();
    }

    /**
     * 初始化
     *
     * @memberof IBizWizardPanelService
     */
    public init(): void {
        this.onInit();
        this.onInitFormComponents();
    }

    /**
     * 初始化
     *
     * @memberof IBizWizardPanelService
     */
    public onInit(): void {
        this.regForms();
        this.regActions();
    }

    /**
     * 注册多部件
     * 
     * @memberof IBizWizardPanelService
     */
    public regForms(): void {

    }

    /**
     * 注册部件行为
     * 
     * @memberof IBizWizardPanelService
     */
    public regActions(): void {

    }

    /**
     * 注册部件事件
     * 
     * @returns {void} 
     * @memberof IBizWizardPanelService
     */
    public onInitFormComponents(): void {
        if (!this.$wizardForms) {
            return;
        }
        for (const key in this.$wizardForms) {
            const form = this.getForm(key);
            form.on(IBizEvent.IBizForm_FORMSAVED, (params) => {
                this.onFormSaved(params);
            });
            form.on(IBizEvent.IBizForm_FORMLOADED, (params) => {
                this.onEditFormLoaded(params);
            });
            form.on(IBizEvent.IBizForm_FORMFIELDCHANGED, (params) => {
                this.onFieldValueChanged(params);
            });
            // form.on(IBizEvent. IBizStaticVariables.SAVED, (params) => {
            //     this.onEditFormSaved(params);
            // });
        }
    }

    /**
     * 表单保存成功
     * 
     * @param {any} params 
     * @memberof IBizWizardPanelService
     */
    public onFormSaved(params): void {
        this.fire(IBizEvent.IBizForm_FORMSAVED, params);
        if (Object.is(this.$curstep, 'NEXT')) {
            const index = this.$formids.indexOf(this.$curformId);
            if (this.$formids[index + 1]) {
                this.changeForm(this.$formids[index + 1]);
            } else {
                this.doFinish();
            }
        } else if (Object.is(this.$curstep, 'FINAL')) {
            this.doFinish();
        }
    }

    /**
     * 表单加载成功
     * 
     * @param {any} params 
     * @memberof IBizWizardPanelService
     */
    public onEditFormLoaded(params): void {
        this.fire(IBizEvent.IBizForm_FORMLOADED, params);
    }

    /**
     * 表单项值发生改成
     * 
     * @param {any} params 
     * @memberof IBizWizardPanelService
     */
    public onFieldValueChanged(params): void {
        this.fire(IBizEvent.IBizForm_FORMFIELDCHANGED, params);
    }

    /**
     * 
     *
     * @param {*} params
     * @memberof IBizWizardPanelService
     */
    public onEditFormSaved(params): void {
        this.fire(IBizEvent.IBizForm_FORMSAVED, params);
    }

    /**
     * 注册部件
     *
     * @param {string} name
     * @param {*} form
     * @memberof IBizWizardPanelService
     */
    public registerForm(name: string, form: any): void {
        if (name) {
            this.$wizardForms[name] = form;
            this.$formids.push(name);
        }
    }

    /**
     * 注册表单允许进行的操作行为
     *
     * @param {string} formId
     * @param {string} tag
     * @memberof IBizWizardPanelService
     */
    public regFormActions(formId: string, tag: string): void {
        if (!this.$formactions[formId]) {
            this.$formactions[formId] = [];
        }
        this.$formactions[formId].push(tag);
    }

    /**
     * 加载数据
     * 
     * @param {*} param 
     * @memberof IBizWizardPanelService
     */
    public autoLoad(arg: any): void {

        // tslint:disable-next-line:prefer-const
        let param: any = { srfaction: 'init', srfctrlid: this.getName() };
        Object.assign(param, arg);
        this.$IBizHttp.post(this.getBackendUrl(), param).subscribe(
            response => {
                if (response.ret === 0) {
                    this.fire(IBizEvent.IBizWizardPanel_WIZARDINITED, response);
                    this.$param = response.data;
                    if (this.$formids.length > 0) {
                        this.changeForm(this.$formids[0]);
                    }
                }
            }, error => {
                console.log(error);
            }
        );
    }

    /**
     * 执行完成操作
     * 
     * @memberof IBizWizardPanelService
     */
    public doFinish() {
        this.$param.srfaction = 'finish';
        this.$param.srfctrlid = this.getName();
        this.$IBizHttp.post(this.getBackendUrl(), this.$param).subscribe(
            response => {
                if (response.ret === 0) {
                    this.fire(IBizEvent.IBizWizardPanel_WIZARDFINISH, response);
                }
            }, error => {
                console.log(error);
            }
        );
    }

    /**
     * 显示当前操作表单能进行的操作按钮
     *
     * @param {string} type
     * @returns {boolean}
     * @memberof IBizWizardPanelService
     */
    public onChangeActionBtn(type: string): boolean {
        if (this.$formactions[this.$curformId]) {
            if (this.$formactions[this.$curformId].indexOf(type) >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取当前步骤表单
     *
     * @returns {*}
     * @memberof IBizWizardPanelService
     */
    public getCurForm(): any {
        return this.$curform;
    }

    /**
     * 获取表单对象
     *
     * @param {string} formId
     * @returns {*}
     * @memberof IBizWizardPanelService
     */
    public getForm(formId: string): any {
        if (this.$wizardForms && this.$wizardForms[formId]) {
            return this.$wizardForms[formId];
        }
        return undefined;
    }

    /**
     * 跳转到指定表单
     *
     * @param {string} formId
     * @memberof IBizWizardPanelService
     */
    public changeForm(formId: string): void {
        if (this.getForm(formId)) {
            this.fire(IBizEvent.IBizWizardPanel_WIZARDCHANGEFORM, {});
            this.$curformId = formId;
            this.$curform = this.getForm(formId);
            this.$curform.autoLoad(this.$param);
        }
    }

    /**
     * 上一步
     *
     * @memberof IBizWizardPanelService
     */
    public goPrev(): void {
        const index = this.$formids.indexOf(this.$curformId);
        if (this.$formids[index - 1]) {
            this.changeForm(this.$formids[index - 1]);
        }
    }

    /**
     * 下一步
     *
     * @memberof IBizWizardPanelService
     */
    public goNext(): void {
        this.$curstep = 'NEXT';
        this.$curform.save2(this.$param);
    }

    /**
     * 完成
     *
     * @memberof IBizWizardPanelService
     */
    public goFinal(): void {
        this.$curstep = 'FINAL';
        this.$curform.save2(this.$param);
    }
}

