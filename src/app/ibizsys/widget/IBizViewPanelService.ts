import { IBizTabService } from '@ibizsys/widget/IBizTabService';

/**
 * 多视图面板服务对象
 * 
 * @export
 * @class IBizViewPanelService
 * @extends {IBizTabService}
 */
export class IBizViewPanelService extends IBizTabService {

    /**
     * Creates an instance of IBizViewPanelService.
     * 创建 IBizViewPanelService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizViewPanelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}