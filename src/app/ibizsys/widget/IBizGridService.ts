﻿import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Rx';
import { IBizEnvironment } from '@env/IBizEnvironment';
import { IBizMDService } from '@ibizsys/widget/IBizMDService';
import { IBizEvent } from '@ibizsys/IBizEvent';
import { IBizUtil } from '@ibizsys/util/IBizUtil';

/**
 * 表格部件服务对象
 * 
 * @export
 * @class IBizGridService
 * @extends {IBizMDService}
 */
export class IBizGridService extends IBizMDService {

    /**
     * 查询开始条数
     * 
     * @memberof IBizGridService
     */
    public $start = 0;

    /**
     * 每次加载条数
     * 
     * @memberof IBizGridService
     */
    public $limit = 20;

    /**
     * 总条数
     * 
     * @memberof IBizGridService
     */
    public $totalrow = 0;

    /**
     * 当前显示页码
     * 
     * @memberof IBizGridService
     */
    public $curPage = 1;

    /**
     * 是否全选
     * 
     * @memberof IBizGridService
     */
    public $allChecked = false;

    /**
     * 表格行选中动画
     * 
     * @memberof IBizGridService
     */
    public $indeterminate = false;

    /**
     * 是否分页设置
     * 
     * @type {boolean}
     * @memberof IBizGridService
     */
    public $pageChangeFlag: boolean;

    /**
     * 表格全部排序字段
     * 
     * @type {Array<any>}
     * @memberof IBizGridService
     */
    public $gridSortField: Array<any> = [];

    /**
     * 行多项选中设置，用于阻塞多次触发选中效果
     *
     * @private
     * @type {boolean}
     * @memberof IBizGridService
     */
    private $rowsSelection: boolean = false;

    /**
     * 是否支持多项
     * 
     * @type {boolean}
     * @memberof IBizGridService
     */
    public $multiSelect: boolean = true;

    /**
     * 是否启用行编辑
     *
     * @type {boolean}
     * @memberof IBizGridService
     */
    public $isEnableRowEdit: boolean = false;

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof IBizGridService
     */
    public $isEnablePagingBar: boolean = true;

    /**
     * 打开行编辑
     *
     * @type {boolean}
     * @memberof IBizGridService
     */
    public $openRowEdit: boolean = false;

    /**
     * 表格编辑项集合
     *
     * @type {*}
     * @memberof IBizGridService
     */
    public $editItems: any = {};

    /**
     * 编辑行数据处理
     *
     * @type {*}
     * @memberof IBizGridService
     */
    public $state: any = {};

    /**
     * 备份数据
     *
     * @type {Array<any>}
     * @memberof IBizGridService
     */
    public $backupDatas: Array<any> = [];

    /**
     * 最大导出行数
     *
     * @type {number}
     * @memberof IBizGridService
     */
    public $maxExportRow: number = 1000;

    /**
     * Creates an instance of IBizGridService.
     * 创建 IBizGridService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizGridService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.regEditItems();
    }

    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGridService
     */
    public load(arg: any = {}): void {
        let opt: any = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        if (this.$isEnablePagingBar) {
            if (!opt.start) {
                Object.assign(opt, { start: (this.$curPage - 1) * this.$limit });
            }
            if (!opt.limit) {
                Object.assign(opt, { limit: this.$limit });
            }
        }

        Object.assign(opt, { sort: JSON.stringify(this.$gridSortField) });

        // 发送加载数据前事件
        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, opt);

        this.$allChecked = false;
        this.$indeterminate = false;
        this.$selection = [];
        this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);

        this.post(opt).subscribe((response: any) => {
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    this.showToast(this.$showErrorToast, '', response.errorMessage);
                }
                return;
            }
            this.$items = this.rendererDatas(response.items);
            this.$totalrow = response.totalrow;
            this.fire(IBizEvent.IBizMDControl_LOADED, response.items);
        }, (error: any) => {
            this.showToast(this.$showErrorToast, '', error.errorMessage);
        });
    }

    /**
     * 刷新数据
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizGridService
     */
    public refresh(arg: any = {}): void {
        let opt: any = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        if (this.$isEnablePagingBar) {
            if (!opt.start) {
                Object.assign(opt, { start: (this.$curPage - 1) * this.$limit });
            }
            if (!opt.limit) {
                Object.assign(opt, { limit: this.$limit });
            }
        }

        Object.assign(opt, { sort: JSON.stringify(this.$gridSortField) });

        // 发送加载数据前事件
        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, opt);

        this.$allChecked = false;
        this.$indeterminate = false;
        this.$selection = [];
        this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);

        this.post(opt).subscribe((response: any) => {
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    this.showToast(this.$showErrorToast, '', response.errorMessage);
                }
                return;
            }

            this.fire(IBizEvent.IBizMDControl_LOADED, response.items);
            this.$items = this.rendererDatas(response.items);
            this.$totalrow = response.totalrow;
        }, (error: any) => {
            this.showToast(this.$showErrorToast, '', error.errorMessage);
        });
    }

    /**
     * 删除数据
     * 
     * @param {*} [arg={}] 
     * @memberof IBizGridService
     */
    public remove(arg: any = {}): void {
        const params: any = {};
        Object.assign(params, arg);
        Object.assign(params, { srfaction: 'remove', srfctrlid: this.getName() });
        this.post(params).subscribe((response: any) => {
            if (response.ret === 0) {
                if (this.$allChecked) {
                    const rows = this.$curPage * this.$limit;
                    if (this.$totalrow <= rows) {
                        this.$curPage = this.$curPage - 1;
                        if (this.$curPage === 0) {
                            this.$curPage = 1;
                        }
                    }
                }
                this.load({});
                this.fire(IBizEvent.IBizDataGrid_REMOVED, {});
                if (response.info && response.info !== '') {
                    this.showToast(this.$showSuccessToast, '', '删除成功!');
                }
                this.$selection = [];
                IBizUtil.processResult(response);
            } else {
                this.showToast(this.$showErrorToast, '', `删除数据失败,${response.info}`);
            }
        }, (error: any) => {
            this.showToast(this.$showErrorToast, '', `删除数据失败,${error.errorMessage}`);
        });
    }

    /**
     * 行数据复选框单选
     * 
     * @param {boolean} value 
     * @param {*} [item={}] 
     * @returns {void} 
     * @memberof IBizGridService
     */
    public onItemSelect(value: boolean, item: any = {}): void {
        if (item.disabled) {
            return;
        }
        if (this.$isEnableRowEdit && this.$openRowEdit) {
            return;
        }

        const index: number = this.$selection.findIndex(data => Object.is(data.srfkey, item.srfkey));
        if (index === -1) {
            this.$selection.push(item);
        } else {
            this.$selection.splice(index, 1);
        }

        if (!this.$multiSelect) {
            this.$selection.forEach(data => {
                data.checked = false;
            });
            this.$selection = [];
            if (index === -1) {
                this.$selection.push(item);
            }
        }
        this.$rowsSelection = true;
        this.$allChecked = this.$selection.length === this.$items.length ? true : false;
        this.$indeterminate = (!this.$allChecked) && (this.$selection.length > 0);
        item.checked = value;
        this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);
    }

    /**
     * 行数据复选框全选
     * 
     * @param {boolean} value 
     * @memberof IBizMDService
     */
    public selectAll(value: boolean): void {
        if (this.$isEnableRowEdit && this.$openRowEdit) {
            return;
        }

        if (!this.$multiSelect) {
            setTimeout(() => {
                this.$allChecked = false;
            });
            return;
        }
        this.$items.forEach(item => {
            if (!item.disabled) {
                item.checked = value;
            }
        });
        this.$selection = [];
        if (value) {
            this.$selection = [...this.$items];
        }
        this.$indeterminate = (!value) && (this.$selection.length > 0);
        this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);
    }

    /**
     * 导出数据
     * 
     * @param {any} params 
     * @memberof IBizGridService
     */
    public exportData(arg: any = {}): void {
        let params: any = {};
        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, params);
        if (params.search) {
            Object.assign(params, { query: params.search });
        }
        Object.assign(params, { srfaction: 'exportdata', srfctrlid: this.getName() });

        if (Object.is(arg.itemTag, 'all')) {
            Object.assign(params, { start: 0, limit: this.$maxExportRow });
        } else if (Object.is(arg.itemTag, 'custom')) {
            const nStart = arg.exportPageStart;
            const nEnd = arg.exportPageEnd;
            if (nStart < 1 || nEnd < 1 || nStart > nEnd) {
                this.showToast('INFO', '警告', '请输入有效的起始页');
                return;
            }
            Object.assign(params, { start: (nStart - 1) * this.$limit, limit: (nEnd - nStart + 1) * this.$limit });
        } else {
            Object.assign(params, { start: (this.$curPage * this.$limit) - this.$limit, limit: this.$curPage * this.$limit });
        }

        if (this.$gridSortField.length > 0) {
            Object.assign(params, { sort: this.$gridSortField[0].property, sortdir: this.$gridSortField[0].direction });
        }

        this.post(params).subscribe((res: any) => {
            if (res.ret === 0) {
                if (res.downloadurl) {
                    let downloadurl = `${IBizEnvironment.BaseUrl.toLowerCase()}${res.downloadurl} `;
                    downloadurl = (IBizEnvironment.LocalDeve ? '' : '../') + downloadurl;
                    IBizUtil.download(downloadurl);
                }
            } else {
                this.showToast('ERROR', '警告', res.info);
            }
        }, (error: any) => {
            this.showToast(this.$showErrorToast, '', error.errorMessage);
        });
    }

    /**
     * 导出模型
     *
     * @param {*} [arg={}]
     * @memberof IBizGridService
     */
    public exportModel(arg: any = {}): void {
        let params: any = {};

        Object.assign(params, arg);
        Object.assign(params, { srfaction: 'exportmodel', srfctrlid: this.getName() });

        this.post(params).subscribe((res: any) => {
            if (res.ret === 0) {
                if (res.downloadurl) {
                    let downloadurl = `${IBizEnvironment.BaseUrl.toLowerCase()}${res.downloadurl} `;
                    downloadurl = (IBizEnvironment.LocalDeve ? '' : '../') + downloadurl;
                    IBizUtil.download(downloadurl);
                }
            } else {
                this.showToast('ERROR', '警告', res.info);
            }
        }, (error: any) => {
            this.showToast(this.$showErrorToast, '', error.errorMessage);
        });
    }

    /**
     * 重置分页
     *
     * @memberof IBizGridService
     */
    public resetStart(): void {
        this.$start = 0;
    }

    /**
     * 第几页跳转
     * 
     * @memberof IBizGridService
     */
    public clickPageIndex($event) {
        if ($event && $event === this.$curPage) {
            return;
        }
        this.$curPage = $event;
        this.$pageChangeFlag = true;
        this.refresh();
    }

    /**
     * 分页页数改变
     * 
     * @memberof IBizGridService
     */
    public changePageIndex() {
        this.refresh();
    }

    /**
     * 每页显示条数
     * 
     * @memberof IBizGridService
     */
    public changePageSize(): void {
        this.$curPage = 1;
        this.refresh();
    }

    /**
     * 获取当前页数
     *
     * @type {number}
     * @memberof IBizGridService
     */
    get nzPageIndex(): number {
        return this.$curPage;
    }

    /**
     * 设置当前页数
     *
     * @memberof IBizGridService
     */
    set nzPageIndex(index: number) {
        if (index === this.$curPage) {
            return;
        }
        this.$curPage = index;
        this.refresh();
    }

    /**
     * 获取分页大小
     *
     * @type {number}
     * @memberof IBizGridService
     */
    get nzPageSize(): number {
        return this.$limit;
    }

    /**
     * 设置分页大小
     *
     * @memberof IBizGridService
     */
    set nzPageSize(size: number) {
        if (size === this.$limit) {
            return;
        }
        this.$limit = size;
        this.refresh();
    }

    /**
     * 单击行选中
     *
     * @param {*} [data={}]
     * @memberof IBizGridService
     */
    public clickRowSelect(data: any = {}): void {
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizEvent.IBizDataGrid_ROWCLICK, this.$selection);
    }

    /**
     * 双击行选中
     *
     * @param {*} [data={}]
     * @memberof IBizGridService
     */
    public dblClickRowSelection(data: any = {}): void {
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizEvent.IBizDataGrid_ROWDBLCLICK, this.$selection);
    }

    /**
     * 表格排序
     * 
     * @param {string} name 字段明显
     * @param {string} type 排序类型
     * @returns {void} 
     * @memberof IBizGridService
     */
    public sort(name: string, type: string): void {
        let item: any = this.$gridSortField.find((_item: any) => Object.is(_item.property, name));
        let _name: string = name;
        Object.values(this.$columns).some((_column: any) => {
            if (_column.dataItemName && Object.is(_column.dataItemName, name)) {
                _name = _column.name;
                return true;
            }
        });
        if (item === undefined) {
            if (Object.is('ascend', type)) {
                this.$gridSortField.push({ property: _name, direction: 'asc' });
            } else if (Object.is('descend', type)) {
                this.$gridSortField.push({ property: _name, direction: 'desc' });
            } else {
                return;
            }
        }

        const index = this.$gridSortField.findIndex((_item: any) => {
            return Object.is(_item.property, name);
        });

        if (Object.is('ascend', type)) {
            this.$gridSortField[index].direction = 'asc';
        } else if (Object.is('descend', type)) {
            this.$gridSortField[index].direction = 'desc';
        } else {
            this.$gridSortField.splice(index, 1);
        }

        this.refresh({});
    }

    /**
     * 设置表格数据当前页
     * 
     * @param {number} page 分页数量
     * @memberof IBizGridService
     */
    public setCurPage(page: number): void {
        this.$curPage = page;
    }

    /**
     * 设置是否支持多选
     * 
     * @param {boolean} state 是否支持多选
     * @memberof IBizGridService
     */
    public setMultiSelect(state: boolean): void {
        this.$multiSelect = state;
    }

    /**
     * 界面行为
     *
     * @param {string} tag
     * @param {*} [data={}]
     * @memberof IBizGridService
     */
    public uiAction(tag: string, data: any = {}) {
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizEvent.IBizMDControl_UIACTION, { tag: tag, data: data });
    }

    /**
     * 处理非复选框行数据选中,并处理是否激活数据
     *
     * @private
     * @param {*} [data={}]
     * @returns {boolean} 是否激活
     * @memberof IBizGridService
     */
    private doRowDataSelect(data: any = {}): boolean {
        if (this.$isEnableRowEdit && this.$openRowEdit) {
            return;
        }
        if (this.$rowsSelection) {
            this.$rowsSelection = false;
            return true;
        }
        this.$selection.forEach((_data) => {
            _data.checked = false;
        });
        this.$selection = [];
        data.checked = true;
        this.$selection.push(data);
        this.$indeterminate = (!this.$allChecked) && (this.$selection.length > 0);
        return false;
    }

    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizGridService
     */
    public rendererDatas(items: Array<any>): Array<any> {
        super.rendererDatas(items);
        items.forEach(item => {
            const names: Array<any> = Object.keys(item);
            names.forEach(name => { item[name] = item[name] ? item[name] : ''; });
        });
        if (this.$isEnableRowEdit) {
            items.forEach((item: any) => { item.openeditrow = (this.$isEnableRowEdit) ? true : false; });
        }

        if (this.$isEnableRowEdit) {
            items.forEach((item: any) => { item.openeditrow = (this.$openRowEdit) ? false : true; });

            if (this.$openRowEdit) {
                this.$selection = [];
                this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);
                this.$backupDatas = [...JSON.parse(JSON.stringify(items))];
                items.forEach((item: any, index: number) => {
                    this.setEditItemState(item.srfkey);
                    this.editRow(item, index, false);
                });
            } else {
                this.$backupDatas = [];
                this.$state = {};
            }
        }
        return items;
    }

    /**
     * 注册表格所有编辑项
     *
     * @memberof IBizGridService
     */
    public regEditItems(): void {
    }

    /**
     * 注册表格编辑项
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizGridService
     */
    public regEditItem(item: any = {}): void {
        if (Object.keys(item).length === 0) {
            return;
        }
        this.$editItems[item.name] = item;
    }

    /**
     * 设置编辑项状态
     *
     * @param {string} srfkey
     * @returns {void}
     * @memberof IBizGridService
     */
    public setEditItemState(srfkey: string): void {
        if (!this.$state) {
            return;
        }
        if (!srfkey) {
            this.$notification.warning('警告', '数据异常');
        }
        let editItems: any = {};
        const itemsName: Array<any> = Object.keys(this.$editItems);
        itemsName.forEach(name => {
            let item: any = {};
            let _editor = JSON.stringify(this.$editItems[name]);
            Object.assign(item, JSON.parse(_editor));
            editItems[name] = item;
        });
        this.$state[srfkey] = editItems;
    }

    /**
     * 删除信息编辑项状态
     *
     * @param {string} srfkey
     * @memberof IBizGridService
     */
    public deleteEditItemState(srfkey: string): void {
        if (srfkey && this.$state.hasOwnProperty(srfkey)) {
            delete this.$state.srfkey;
        }
    }

    /**
     * 设置编辑项是否启用
     *
     * @param {string} srfkey
     * @param {number} type
     * @memberof IBizGridService
     */
    public setEditItemDisabled(srfkey: string, type: number): void {
        if (this.$state && this.$state.hasOwnProperty(srfkey)) {
            let item = this.$state[srfkey];
            const itemsName = Object.keys(item);
            itemsName.forEach(name => {
                let disabled: boolean = (item[name].enabledcond === 3 || item[name].enabledcond === type);
                item[name].disabled = !disabled;
            });
            Object.assign(this.$state[srfkey], item);
        }
    }

    /**
     * 获取行编辑状态
     *
     * @returns {boolean}
     * @memberof IBizGridService
     */
    public getOpenEdit(): boolean {
        return this.$openRowEdit;
    }

    /**
     * 保存表格所有编辑行 <在插件模板中提供重写>
     *
     * @memberof IBizGridService
     */
    public saveAllEditRow(): void {
        let result: Array<any> = [];
        Observable.from(this.$items).filter((_item: any) => {
            let data = this.$backupDatas.find((_data: any) => Object.is(_data.srfkey, _item.srfkey));
            let state = Object.keys(data).some((name: string) => {
                let fieldIsSave: boolean = !Object.is(name, 'openeditrow') && (typeof _item[name] === 'string' && typeof data[name] === 'string') && !Object.is(_item[name], data[name]);
                return fieldIsSave;
            });
            return state;
        }).flatMap((_item: any, index: number) => {
            return this.editRowSave(_item, index + 1);
        }).subscribe((responses: any) => {
            result.push(responses);
        }, (error: any) => {
        }, () => {
            if (result.length === 0) {
                return;
            }
            this.showGridSaveInfo(result);
        });
    }

    /**
     * 显示表格保存信息
     *
     * @private
     * @param {Array<any>} infos
     * @memberof IBizGridService
     */
    private showGridSaveInfo(infos: Array<any>): void {
        infos.sort((a: any, b: any) => {
            return a.index - b.index;
        });

        let successInfo = '';
        let errorInfo = '';
        let info = '';

        infos.forEach((_result: any) => {
            if (Object.is(_result.state, 'success')) {
                successInfo = !Object.is(successInfo, '') ? `${successInfo}、${_result.index}` : `${successInfo}${_result.index}`;
            }
            if (Object.is(_result.state, 'error')) {
                errorInfo = `${errorInfo}\n第 ${_result.index} 条保存失败，错误信息如下：`;
                let _info = '';
                if (_result._result && Array.isArray(_result._result)) {
                    const items: Array<any> = _result._result;
                    items.forEach((item, _index) => {
                        _info = _index > 0 ? `${_info}、${item.info}` : `${_info}${item.info}`;
                    });
                } else {
                    _info = `${_info}${_result.info}`;
                }
                errorInfo = `${errorInfo}${_info}`;
            }
        });
        if (!Object.is(successInfo, '')) {
            info = `${info}第 ${successInfo} 条保存成功！\n`;
        }
        if (!Object.is(errorInfo, '')) {
            info = `${info}${errorInfo}`;
        }
        this.showToast(this.$showInfoToast, '', info);
    }

    /**
     * 是否启用行编辑
     *
     * @param {string} tag
     * @memberof IBizGridService
     */
    public isOpenEdit(tag: string): void {

        if (!this.$isEnableRowEdit) {
            this.$notification.info('提示', '未启用行编辑');
            return;
        }
        this.$openRowEdit = !this.$openRowEdit;
        if (this.$openRowEdit) {
            this.$items.forEach((item: any) => { item.openeditrow = true; });

            this.$selection.forEach((data) => {
                data.checked = false;
            });
            this.$selection = [];
            this.$indeterminate = false;
            this.fire(IBizEvent.IBizMDControl_SELECTIONCHANGE, this.$selection);
            this.$backupDatas = [...JSON.parse(JSON.stringify(this.$items))];
            this.$items.forEach((item: any, index: number) => {
                this.setEditItemState(item.srfkey);
                this.editRow(item, index, false);
            });
        } else {
            this.$backupDatas.forEach((_data: any) => { _data.openeditrow = true; });
            this.$items = [...JSON.parse(JSON.stringify(this.$backupDatas))];
            this.$backupDatas = [];
            this.$state = {};
        }
    }

    /**
     * 编辑行数据
     *
     * @param {*} [data={}]
     * @param {number} rowindex
     * @memberof IBizGridService
     */
    public editRow(data: any = {}, rowindex: number, state: boolean): void {
        data.openeditrow = state;
        this.setEditItemState(data.srfkey);
        if (data.openeditrow) {
            const index: number = this.$backupDatas.findIndex(item => Object.is(item.srfkey, data.srfkey));
            if (index !== -1) {
                Object.assign(data, this.$backupDatas[index]);
            }
            if (Object.is(data.srfkey, '')) {
                this.$items.splice(rowindex, 1);
            }
        } else {
            this.setEditItemDisabled(data.srfkey, 2);
        }
    }

    /**
     * 保存编辑行数据
     *
     * @param {*} [data={}]
     * @param {number} rowindex
     * @memberof IBizGridService
     */
    public editRowSave(data: any = {}, rowindex: number): Subject<any> {
        let subject: Subject<any> = new Subject();

        const srfaction: string = data.rowdatamodal && Object.is(data.rowdatamodal, 'gridloaddraft') ? 'create' : 'update';

        let params: any = { srfaction: srfaction, srfctrlid: 'grid' };
        const _names: Array<any> = Object.keys(data);
        _names.forEach(name => {
            data[name] = data[name] ? data[name] : '';
        });
        Object.assign(params, data);
        this.$IBizHttp.post(this.getBackendUrl(), params).subscribe((responce: any) => {
            let result = [];
            let state = responce.ret === 0 ? 'success' : 'error';
            if (responce.ret === 0) {
                const index: number = this.$backupDatas.findIndex(item => Object.is(data.srfkey, item.srfkey));
                if (index !== -1) {
                    Object.assign(this.$backupDatas[index], responce.data);
                    this.deleteEditItemState(data.srfkey);
                    this.setEditItemState(responce.data.srfkey);
                }
                Object.assign(data, responce.data);
            } else {
                if (responce.error && (responce.error.items && Array.isArray(responce.error.items))) {
                    responce.error.items.forEach((item: any) => {
                        Object.assign(this.$state[data.srfkey][item.id].styleCss, { 'border': '1px solid #f04134', 'border-radius': '4px' });
                    });
                    result = [...responce.error.items];
                }
            }
            subject.next({ state: state, index: rowindex, result: result, info: responce.info });
            subject.complete();
        }, (error: any) => {
            let items: Array<any> = [];
            if (error.error && (error.error.items && Array.isArray(error.error.items))) {
                error.error.items.forEach((item: any) => {
                    Object.assign(this.$state[data.srfkey][item.id].styleCss, { 'border': '1px solid #f04134', 'border-radius': '4px' });
                });
                items = [...error.error.items];
            }
            subject.next({ state: 'error', index: rowindex, result: items, info: error.info });
            subject.complete();
        });

        return subject;
    }

    /**
     * 行编辑文本框光标移出事件
     *
     * @param {*} $event
     * @param {string} name
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizGridService
     */
    public onBlur($event: any, name: string, data: any = {}): void {
        if ((!$event) || Object.keys(data).length === 0) {
            return;
        }
        if (Object.is($event.target.value, data[name])) {
            return;
        }
        this.colValueChange(name, $event.target.value, data);
    }

    /**
     * 行编辑文本框键盘事件
     *
     * @param {*} $event
     * @param {string} name
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizGridService
     */
    public onKeydown($event: any, name: string, data: any = {}): void {
        if ((!$event) || Object.keys(data).length === 0) {
            return;
        }
        if ($event.keyCode !== 13) {
            return;
        }
        if (Object.is($event.target.value, data[name])) {
            return;
        }
        this.colValueChange(name, $event.target.value, data);
    }

    /**
     * 行编辑单元格值变化
     *
     * @param {string} name
     * @param {*} data
     * @memberof IBizGridService
     */
    public colValueChange(name: string, value: string, data: any): void {
        const srfkey = data.srfkey;
        const _data = this.$backupDatas.find(back => Object.is(back.srfkey, srfkey));
        if (this.$state[srfkey][name]) {
            if (_data && !Object.is(_data[name], value)) {
                Object.assign(this.$state[srfkey][name].styleCss, { 'border': '1px solid #49a9ee', 'border-radius': '4px' });
            } else {
                Object.assign(this.$state[srfkey][name].styleCss, { 'border': '0px', 'border-radius': '0px' });
            }
        }
        data[name] = value;
        this.fire(IBizEvent.IBizDataGrid_UPDATEGRIDITEMCHANGE, { name: name, data: data });
    }

    /**
     * 更新表格编辑列值
     *
     * @param {string} srfufimode
     * @param {*} [data={}]
     * @memberof IBizGridService
     */
    public updateGridEditItems(srfufimode: string, data: any = {}): void {
        let opt = { srfaction: 'updategridedititem', srfufimode: srfufimode, srfctrlid: 'grid' };
        const viewController = this.getViewController();
        if (viewController && viewController.getViewParam() && Object.keys(viewController.getViewParam()).length > 0) {
            Object.assign(opt, viewController.getViewParam());
        }
        const _names: Array<any> = Object.keys(data);
        _names.forEach(name => {
            data[name] = data[name] ? data[name] : '';
        });
        Object.assign(opt, { srfactivedata: JSON.stringify(data) });
        this.post(opt).subscribe((success) => {
            if (success.ret === 0) {
                const index: number = this.$items.findIndex(item => Object.is(item.srfkey, data.srfkey));
                if (index !== -1) {
                    Object.assign(this.$items[index], success.data);
                }
            } else {
                this.$notification.error('错误', success.info);
            }
        }, (error) => {
            this.$notification.error('错误', error.info);
        });
    }

    /**
     * 新建编辑行
     *
     * @param {*} [param={}]
     * @memberof IBizGridService
     */
    public newRowAjax(param: any = {}): void {
        let opt: any = {};
        Object.assign(opt, param);
        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, opt);
        Object.assign(opt, { srfaction: 'loaddraft', srfctrlid: 'grid' });
        this.post(opt).subscribe((success: any) => {
            if (success.ret === 0) {
                // 设置数据主键或者自建临时主键
                const srfkey: string = (Object.is(success.data.srfkey, '')) ? IBizUtil.createUUID() : success.data.srfkey;
                success.data.srfkey = srfkey;
                this.setEditItemState(srfkey);
                this.setEditItemDisabled(srfkey, 1);
                this.$items.push(Object.assign(success.data, { openeditrow: false, rowdatamodal: 'gridloaddraft' }));
                let backdata: any = JSON.parse(JSON.stringify(success.data));
                this.$backupDatas.push(backdata);
            } else {
                this.$notification.error('错误', `获取默认数据失败, ${success.info}`);
            }
        }, (error: any) => {
            this.$notification.error('错误', `获取默认数据失败, ${error.info}`);
        });
    }
}

