import { IBizTabService } from '@ibizsys/widget/IBizTabService';


/**
 * 分页控件服务对象
 * 
 * @export
 * @class IBizExpTabService
 * @extends {IBizTabService}
 */
export class IBizExpTabService extends IBizTabService {

    /**
     * Creates an instance of IBizExpTabService.
     * 创建 IBizExpTabService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizExpTabService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}