import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';
import { IBizUICounterService } from '@ibizsys/util/IBizUICounterService';


/**
 * 工作流树导航部件
 * 
 * @export
 * @class IBizTreeExpBarService
 * @extends {IBizService}
 */
export class IBizWFExpBarService extends IBizService {

    /**
     * 导航树部件是否收缩，默认展开
     * 
     * @type {boolean}
     * @memberof IBizWFExpBarService
     */
    public $isCollapsed: boolean = true;

    /**
     * 导航菜单数据项
     * 
     * @type {Array<any>}
     * @memberof IBizWFExpBarService
     */
    public $items: Array<any> = [];

    /**
     * 选中菜单项
     * 
     * @type {*}
     * @memberof IBizWFExpBarService
     */
    public $selectItem: any = {};

    /**
     * 计数器
     *
     * @type {IBizUICounterService}
     * @memberof IBizWFExpBarService
     */
    public $UICounter: IBizUICounterService

    /**
     * Creates an instance of IBizWFExpBarService.
     * 创建 IBizWFExpBarService 示例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizWFExpBarService
     */
    constructor(opts: any = {}) {
        super(opts);

        if (this.getViewController()) {
            const viewController = this.getViewController();
            viewController.on(IBizEvent.IBizViewController_INITED, () => {
                this.$UICounter = viewController.getUICounter(this.getUICounterName());
                this.onCounterChanged();
                this.$UICounter.on(IBizEvent.COUNTERCHANGE, (data) => {
                    this.onCounterChanged();
                });
            });
        }
    }

    /**
     * 加载导航树数据
     * 
     * @param {*} _opt 
     * @memberof IBizWFExpBarService
     */
    public load(_opt: any): void {
        let opts: any = {};
        Object.assign(opts, _opt);
        Object.assign(opts, { srfaction: 'fetch', srfctrlid: this.getName() });

        this.post(opts, this.getBackendUrl()).subscribe(
            (result) => {
                if (result.ret === 0) {
                    this.$items = result.items;
                    this.onCounterChanged();
                    this.formarItems(this.$items);
                    this.fire(IBizEvent.IBizTreeExpBar_LOADED, this.$items[0]);
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    /**
     * 格式化数据项
     *
     * @private
     * @param {*} _items
     * @returns {*}
     * @memberof IBizWFExpBarService
     */
    private formarItems(_items: any): any {
        _items.forEach(item => {
            if (item.checked) {
                Object.assign(this.$selectItem, item);
            }
            item.bchecked = item.checked ? true : false;

            if (item.items) {
                const hasItemCheck = this.formarItems(item.items);
                if (hasItemCheck) {
                    item.expanded = true;
                }
            }
            item.hassubmenu = item.items ? true : false;
        });
    }

    /**
     * 菜单项选中处理
     * 
     * @param {*} item 
     * @returns {void} 
     * @memberof IBizTreeExpBarService
     */
    public selection(item: any = {}): void {
        if (item.items && item.items.length > 0) {
            return;
        }

        if (Object.is(item.id, this.$selectItem.id)) {
            return;
        }
        this.$selectItem = {};
        Object.assign(this.$selectItem, item);

        this.fire(IBizEvent.IBizTreeExpBar_SELECTIONCHANGE, this.$selectItem);
    }


    /**
     * 菜单节点选中处理
     * 
     * @param {*} [item={}] 
     * @memberof IBizWFExpBarService
     */
    public expandedAndSelectSubMenu(item: any = {}): void {
        if (Object.is(item.id, this.$selectItem.id)) {
            return;
        }
        this.$selectItem = {};
        Object.assign(this.$selectItem, item);

        this.fire(IBizEvent.IBizTreeExpBar_SELECTIONCHANGE, this.$selectItem);
    }

    /**
     * 获取计数器名称
     * 在发布器中重写
     * 
     * @returns {string} 
     * @memberof IBizWFExpBarService
     */
    public getUICounterName(): string {
        return undefined;
    }

    /**
     * 设置选中项
     *
     * @param {*} [item={}]
     * @memberof IBizWFExpBarService
     */
    public setSelectItem(item: any = {}): void {
        if (item && !Object.is(item.id, this.$selectItem.id)) {
            this.$selectItem = {};
            Object.assign(this.$selectItem, item);
        }
    }

    /**
     * 计数器值变化
     *
     * @private
     * @returns {void}
     * @memberof IBizWFExpBarService
     */
    private onCounterChanged(): void {
        if (!this.$UICounter) {
            return;
        }
        const data = this.$UICounter.getData();
        if (!data) {
            return;
        }
        let bNeedReSelect: boolean = this.itemSelect(this.$items, data);

        if (bNeedReSelect) {
            this.$selectItem = {};
            Object.assign(this.$selectItem, this.$items[0]);
            this.fire(IBizEvent.IBizTreeExpBar_SELECTIONCHANGE, this.$selectItem);
        }
    }

    /**
     * 选中项
     *
     * @private
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {boolean}
     * @memberof IBizWFExpBarService
     */
    private itemSelect(items: Array<any>, data: any = {}): boolean {
        let bNeedReSelect: boolean = false;
        items.forEach(item => {
            let counterid = item.counterid;
            let countermode = item.countermode;

            item.show = true;
            let count = data[counterid];
            if (!count) {
                count = 0;
            }
            if (count === 0 && countermode && countermode === 1) {
                item.show = false;
                // 判断是否选中列，如果是则重置选中
                if (this.$selectItem && Object.is(this.$selectItem.id, item.id)) {
                    bNeedReSelect = true;
                }
            }

            item.counterdata = count;
            if (item.items) {
                bNeedReSelect = this.itemSelect(item.items, data);
            }
        });
        return bNeedReSelect;
    }

    /**
     * 获取数据项
     *
     * @returns {Array<any>}
     * @memberof IBizWFExpBarService
     */
    public getItems(): Array<any> {
        return this.$items;
    }
}
