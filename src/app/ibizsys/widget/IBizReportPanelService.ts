import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';
import { IBizEnvironment } from '@env/IBizEnvironment';

/**
 * 报表框架服务对象
 * 
 * @export
 * @class IBizReportPanelService
 * @extends {IBizService}
 */
export class IBizReportPanelService extends IBizService {

    /**
     * 是否显示报表，本地开发不显示报表
     * 
     * @type {boolean}
     * @memberof IBizReportPanelService
     */
    public $showReport: boolean = false;

    /**
     * 报表视图参数
     * 
     * @type {string}
     * @memberof IBizReportPanelService
     */
    public $viewurl: string;

    /**
     * 报表ID
     * 
     * @type {string}
     * @memberof IBizReportPanelService
     */
    public $reportid: string = '';

    /**
     * Creates an instance of IBizReportPanelService.
     * 创建 IBizReportPanel 实例 
     * 
     * @param {*} [opts={}] 
     * @memberof IBizReportPanelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据加载
     * 
     * @memberof IBizReportPanelService
     */
    public load(): void {
        this.buildReport();
    }

    /**
     * 处理报表内容
     * 
     * @memberof IBizReportPanelService
     */
    private buildReport(): void {

        let params: any = {};
        this.fire(IBizEvent.IBizReportPanel_BEFORELOAD, params);
        if (params.srfaction) {
            delete params.srfaction;
        }

        if (Object.is(this.$reportid, '')) {
            return;
        }
        Object.assign(params, { srfreportid: this.$reportid });

        if (!IBizEnvironment.LocalDeve) {
            if (Object.keys(params).length !== 0) {
                this.$showReport = true;
                let _data: Array<any> = [];
                const _paramsArr: Array<any> = Object.keys(params);
                _paramsArr.forEach(key => {
                    _data.push(`${key}=${params[key]}`);
                });
                const urlData: string = _data.join('&');
                this.$viewurl = `..${IBizEnvironment.PDFReport}?${urlData}`;
            }
        }
    }
}