import { IBizMDService } from '@ibizsys/widget/IBizMDService';
import { IBizEvent } from '@ibizsys/IBizEvent';


/**
 * 数据视图部件控制器
 * 
 * @export
 * @class IBizDataViewService
 * @extends {IBizMDService}
 */
export class IBizDataViewService extends IBizMDService {

    /**
     * 双击选中项
     * 
     * @private
     * @type {Array<any>}
     * @memberof IBizDataViewService
     */
    private $dblselection: Array<any> = [];

    /**
     * 选中项
     * 
     * @type {*}
     * @memberof IBizDataViewService
     */
    public $selectItem: any = {};

    /**
     * Creates an instance of IBizDataViewService.
     * 创建 IBizDataViewService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizDataViewService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据加载
     * 
     * @param {*} [arg={}] 
     * @memberof IBizDataViewService
     */
    public load(arg: any = {}): void {
        let opt: any = {};
        Object.assign(opt, arg);
        if (this.$loading) {
            return;
        }
        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });

        // 设置为正在加载，使load方法在加载中时不可用。
        this.$loading = true;
        // 发送加载数据前事件
        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, opt);
        // this.mask();
        // this.fillPostParams(opt);
        this.$IBizHttp.post(this.getBackendUrl(), opt)
            .subscribe(
                response => {
                    // this.unmask();
                    if (!response.items || response.ret !== 0) {
                        if (response.errorMessage) {
                            this.showToast(this.$showErrorToast, '', response.errorMessage);
                        }
                        // this.ignoreUFI = false;
                        // this.ignoreformfieldchange = false;
                        this.$loading = false;
                        return;
                    }
                    //                if (Object.is(this.$start, 0)) {
                    this.$items = response.items;
                    //                } else {
                    //                    this.$items.push(...response.items);
                    //                }
                    this.fire(IBizEvent.IBizMDControl_LOADED, response.items);
                    this.$loading = false;
                },
                error => {
                    this.$loading = false;
                    // this.unmask();
                    // this.showToast(this.$showErrorToast, '', '数据加载失败,请检查网络连接或投诉');
                    console.log(error.info);
                }
            );
    }

    /**
     * 选择变化
     * 
     * @param {*} [item={}] 
     * @memberof IBizDataViewService
     */
    public selectChange(data: any = {}): void {

        const arr: Array<any> = this.$selection.filter((item) => Object.is(data.srfkey, item.srfkey));
        this.$selection = [];
        this.$selectItem = {};
        if (arr.length !== 1) {
            this.$selection.push(data);
            Object.assign(this.$selectItem, data);
        }

        this.fire(IBizEvent.IBizDataView_SELECTIONCHANGE, this.$selection);
    }


    /**
     * 双击选择变化
     * 
     * @param {*} [data={}] 
     * @memberof IBizDataViewService
     */
    public DBClickSelectChange(data: any = {}): void {

        const arr: Array<any> = this.$dblselection.filter((item) => Object.is(data.srfkey, item.srfkey));
        this.$dblselection = [];
        this.$selectItem = {};
        if (arr.length !== 1) {
            this.$dblselection.push(data);

            Object.assign(this.$selectItem, data);
        }

        this.fire(IBizEvent.IBizDataView_DATAACTIVATED, this.$dblselection);
    }
}

