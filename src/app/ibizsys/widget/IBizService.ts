import { IBizServiceBase } from '@ibizsys/widget/IBizServiceBase';
import { IBizHttp } from '@ibizsys/util/IBizHttp';
import { IBizNotification } from '@ibizsys/util/IBizNotification';
import { Observable } from 'rxjs';


/**
 * 部件服务对象
 * 
 * @export
 * @class IBizService
 * @extends {IBizServiceBase}
 */
export class IBizService extends IBizServiceBase {

    /**
     * IBiz http 对象
     * 
     * @type {HttpProvider}
     * @memberof IBizService
     */
    public $IBizHttp: IBizHttp;

    /**
     * 部件名称
     * 
     * @private
     * @type {string}
     * @memberof IBizService
     */
    private $name: string;

    /**
     * 后台交互URL
     * 
     * @private
     * @type {string}
     * @memberof IBizService
     */
    private $url: string;

    /**
     * 视图控制器对象
     * 
     * @private
     * @type {*}
     * @memberof IBizService
     */
    private $viewController: any;

    /**
     * IBiz 消息提示对象
     * 
     * @type {IBizNotification}
     * @memberof IBizService
     */
    public $notification: IBizNotification;

    /**
     * 不带图标的提示
     * 
     * @memberof IBizService
     */
    public $showBlankToast = 'BLANK';

    /**
     * 成功提示
     * 
     * @memberof IBizService
     */
    public $showSuccessToast = 'SUCCESS';

    /**
     * 失败提示
     * 
     * @memberof IBizService
     */
    public $showErrorToast = 'ERROR';

    /**
     * 警告提示
     * 
     * @memberof IBizService
     */
    public $showWarningToast = 'WANGING';

    /**
     * 信息提示
     * 
     * @memberof IBizService
     */
    public $showInfoToast = 'INFO';

    /**
     * 可使用html代码来渲染内容
     * 
     * @memberof IBizService
     */
    public $showHtmlToast = 'HTML';

    /**
     * Creates an instance of IBizService.
     * 创建 IBizService 实例。 
     * 
     * @param {*} [opts={}] 
     * @memberof IBizService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.$name = opts.name;
        this.$url = opts.url;
        this.$notification = opts.notification;
        this.$viewController = opts.viewController;
        if (this.$viewController) {
            this.$IBizHttp = this.$viewController.$IBizHttp;
        }
    }

    /**
     * 获取部件名称
     * 
     * @returns {String}
     * @memberof IBizService
     */
    public getName(): string {
        return this.$name;
    }

    /**
     * 获取后台路径
     * 
     * @returns {*} 
     * @memberof IBizService
     */
    public getBackendUrl(): string {
        let url: string = '';
        let dynamicParams: any = {};
        if (this.$url) {
            url = this.$url;
        }
        if (this.getViewController()) {
            if (!url) {
                url = this.getViewController().getBackendUrl();
            }

            // 动态视图参数
            dynamicParams = this.getViewController().getDynamicParams();
            if (dynamicParams && Object.keys(dynamicParams).length > 0) {
                url = this.addOptionsForUrl(url, dynamicParams);
            }
        }
        return url;
    }

    /**
     * 添加参数到指定的url中
     *
     * @param {string} url 路径
     * @param {*} [opt={}] 参数
     * @returns {string}
     * @memberof IBizDynamicService
     */
    public addOptionsForUrl(url: string, opt: any = {}): string {
        const keys: string[] = Object.keys(opt);
        const isOpt: number = url.indexOf('?');
        keys.forEach(
            (key, index) => {
                if (index === 0 && isOpt === -1) {
                    url += `?${key}=${opt[key]}`;
                } else {
                    url += `&${key}=${opt[key]}`;
                }
            }
        );
        return url;
    }

    /**
     * 获取视图控制器
     * 
     * @returns {*} 
     * @memberof IBizService
     */
    public getViewController(): any {
        if (this.$viewController) {
            return this.$viewController;
        }
        return undefined;
    }


    /**
     * 信息提示框
     * 
     * @param {string} type 提示类型
     * @param {string} title  提示标题
     * @param {string} content  提示内容
     * @memberof IBizService
     */
    public showToast(type: string, title: string, content: string): void {
        if (this.$notification) {
            if (Object.is(type, this.$showBlankToast)) {
                this.$notification.blank(title, content);
            } else if (Object.is(type, this.$showSuccessToast)) {
                this.$notification.success(title, content);
            } else if (Object.is(type, this.$showErrorToast)) {
                this.$notification.error(title, content);
            } else if (Object.is(type, this.$showWarningToast)) {
                this.$notification.warning(title, content);
            } else if (Object.is(type, this.$showInfoToast)) {
                this.$notification.info(title, content);
            } else if (Object.is(type, this.$showHtmlToast)) {
                this.$notification.html(content);
            }
        }

    }

    /**
     * 有loading动画的post请求
     * 
     * @param {*} param 请求携带的参数
     * @param {string} [url] 请求地址.(ps：不填写时采用视图默认url)
     * @returns {Observable<any>} 
     * @memberof IBizService
     */
    public post(param: any, url?: string): Observable<any> {
        let _url: string;
        if (url) {
            _url = url;
        } else {
            _url = this.getBackendUrl();
        }
        return this.$IBizHttp.post(_url, param);
    }

    /**
     * 无loading动画的post请求
     * 
     * @param {*} param 请求携带的参数
     * @param {string} [url] 请求地址.(ps：不填写时采用视图默认url)
     * @returns {Observable<any>} 
     * @memberof IBizService
     */
    public post2(param: any, url?: string): Observable<any> {
        let _url: string;
        if (url) {
            _url = url;
        } else {
            _url = this.getBackendUrl();
        }
        return this.$IBizHttp.post2(_url, param);
    }
}
