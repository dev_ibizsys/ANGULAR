import { IBizService } from '@ibizsys/widget/IBizService';

/**
 * 分页部件服务对象
 * 
 * @export
 * @class IBizTabService
 * @extends {IBizService}
 */
export class IBizTabService extends IBizService {

    /**
     * 激活分页部件分页数
     * 
     * @type {number}
     * @memberof IBizTabService
     */
    public $activeTabIndex: number = 0;

    /**
     * 分页部件对象
     * 
     * @type {*}
     * @memberof IBizTabService
     */
    public $tabs: any = {};

    /**
     * Creates an instance of IBizTabService.
     * 创建 IBizTabService 实例
     * @param {*} [opts={}] 
     * @memberof IBizTabService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.regTabs();
    }

    /**
     * 注册所有分页部件对象
     * 
     * @memberof IBizTabService
     */
    public regTabs(): void {

    }

    /**
     * 注册分页部件对象
     * 
     * @param {*} [tab={}] 
     * @memberof IBizTabService
     */
    public regTab(tab: any = {}): void {
        if (Object.keys(tab).length > 0 && tab.name) {
            this.$tabs[tab.name] = tab;
        }
    }

    /**
     * 获取分页部件对象
     * 
     * @param {string} name 
     * @returns {*} 
     * @memberof IBizTabService
     */
    public getTab(name: string): any {
        if (this.$tabs[name]) {
            return this.$tabs[name];
        }
        return undefined;
    }

    /**
     * 设置激活分页数
     * 
     * @param {number} index 
     * @memberof IBizTabService
     */
    public setActiveTab(index: number): void {
        setTimeout(() => {
            this.$activeTabIndex = index;
        });
    }
}

