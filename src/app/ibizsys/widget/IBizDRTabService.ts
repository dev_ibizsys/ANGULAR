import { IBizTabService } from '@ibizsys/widget/IBizTabService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 关系分页部件服务对象
 * 
 * @export
 * @class IBizDRTabService
 * @extends {IBizTabService}
 */
export class IBizDRTabService extends IBizTabService {

    /**
     * 父数据对象
     * 
     * @type {*}
     * @memberof IBizDRTabService
     */
    public $parentData: any = {};

    /**
     * Creates an instance of IBizDRTabService.
     * 创建 IBizDRTabService 实例
     * @param {*} [opts={}] 
     * @memberof IBizDRTabService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 设置父数据
     * 
     * @param {*} [data={}] 
     * @memberof IBizDRTabService
     */
    public setParentData(data: any = {}): void {
        Object.assign(this.$parentData, data);
    }

    /**
     * 分页部件选中变化
     * 
     * @param {*} [args={}] 
     * @returns {void} 
     * @memberof IBizDRTabService
     */
    public onTabSelectionChange(args: any = {}): void {
        let viewid: string = args.name;
        let controller = this.getViewController();

        let parentKey: string = '';
        if (this.$parentData.srfparentkey) {
            parentKey = this.$parentData.srfparentkey;
        }

        if (!parentKey || Object.is(parentKey, '')) {
            this.$notification.warning('警告', '请先建立主数据');
            this.setActiveTab(0);
            return;
        }

        if (Object.is(viewid, 'form')) {
            this.fire(IBizEvent.IBizDRTab_SELECTCHANGE, { parentMode: {}, parentData: {}, viewid: viewid });
            this.setActiveTab(0);
            return;
        }

        const dritem: any = { viewid: viewid.toLocaleUpperCase() };
        if (!dritem.viewid || Object.is(dritem.viewid, '')) {
            return;
        }

        const viewItem: any = controller.getDRItemView(dritem);
        if (viewItem == null || !viewItem.viewparam) {
            return;
        }

        const viewParam = viewItem.viewparam;
        let parentData: any = {};
        if (true) {
            Object.assign(parentData, viewParam);
            Object.assign(parentData, this.$parentData);

            if (viewParam.srfparentdeid) {
                Object.assign(parentData, { srfparentdeid: viewParam.srfparentdeid });
            }
        }

        this.setActiveTab(args.index);
        this.fire(IBizEvent.IBizDRTab_SELECTCHANGE, { parentMode: viewParam, parentData: parentData, viewid: viewid });
    }

    /**
     * 设置关系项禁用
     *
     * @param {boolean} state
     * @memberof IBizDRTabService
     */
    public setDisabled(state: boolean) {
        Object.keys(this.$tabs).forEach((name: string) => {
            const tab = this.$tabs[name];
            tab.disabled = Object.is(tab.name, 'form') ? false : state;
        });
    }

}

