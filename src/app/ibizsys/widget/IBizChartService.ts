import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 图表部件服务对象
 * 
 * @export
 * @class IBizChartService
 * @extends {IBizService}
 */
export class IBizChartService extends IBizService {

    /**
     * 图表数据
     * 
     * @type {Array<any>}
     * @memberof IBizChartService
     */
    public $data: Array<any> = [];

    /**
     * Creates an instance of IBizChartService.
     * 创建 IBizChartService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizChartService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 加载报表数据
     * 
     * @memberof IBizChartService
     */
    public load(): void {
        this.buildChart();
    }

    /**
     * 处理图表内容
     * 
     * @memberof IBizChartService
     */
    private buildChart(): void {
        let params: any = {};
        this.fire(IBizEvent.IBizChart_BEFORELOAD, params);
        Object.assign(params, { srfrender: 'ECHARTS3', srfaction: 'FETCH', srfctrlid: this.getName() });
        this.post(params, this.getBackendUrl()).subscribe((result) => {
            if (result.ret === 0) {
                this.$data = result.data;
                const data = this.getChartConfig();
                let target = {};
                Object.assign(target, data, this.$data);
                this.fire(IBizEvent.IBizChart_LOADED, target);
            } else {
                this.showToast(this.$showErrorToast, '系统异常', result.info);
            }
        }, (error) => {
            this.showToast(this.$showErrorToast, '系统异常', error.info);
        });
    }

    /**
     * 获取图表基础配置数据
     */
    public getChartConfig(): any {
        const opts: any = {};
        return opts;
    }
}

