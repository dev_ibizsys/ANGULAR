import { NzModalService, NzModalSubject } from 'ng-zorro-antd';
import { IBizObject } from '@ibizsys/IBizObject';

/**
 * 部件服务对象基类，处理ng-zorro-antd组件模态框对象
 * 
 * @export
 * @class IBizServiceBase
 * @extends {IBizObject}
 */
export class IBizServiceBase extends IBizObject {

    /**
     * 打开模态框服务对象
     * 
     * @type {NzModalService}
     * @memberof IBizServiceBase
     */
    public nzModalService: NzModalService;

    /**
     * 监控模态框服务对象，回调使用
     * 
     * @type {NzModalSubject}
     * @memberof IBizServiceBase
     */
    public nzModalSubject: NzModalSubject;

    /**
     * Creates an instance of IBizServiceBase.
     * 创建 IBizServiceBase 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
        this.nzModalService = opts.modal;
        this.nzModalSubject = opts.modalSubject;
    }
}

