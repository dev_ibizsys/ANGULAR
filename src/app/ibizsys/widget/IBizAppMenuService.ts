import { IBizUICounterService } from '@ibizsys/util/IBizUICounterService';
import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 应用菜单部件服务对象
 * 
 * @export
 * @class IBizAppMenuService
 * @extends {IBizService}
 */
export class IBizAppMenuService extends IBizService {

    /**
     * 菜单数据项
     * 
     * @type {any[]}
     * @memberof IBizAppMenuService
     */
    public $items: any[] = [];

    /**
     * 计数器数据
     *
     * @private
     * @type {*}
     * @memberof IBizAppMenuService
     */
    private uicounterdata: any = {};

    /**
     * Creates an instance of IBizAppMenuService.
     * 创建 IBizAppMenuService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizAppMenuService
     */
    constructor(opts: any = {}) {
        super(opts);

        const viewController = this.getViewController();
        if (viewController) {
            viewController.on(IBizEvent.IBizViewController_INITED, () => {
                const uicounter: IBizUICounterService = viewController.getUICounter(this.getRefUICounterName());
                if (uicounter) {
                    uicounter.on(IBizEvent.COUNTERCHANGE, (data: any) => {
                        this.uicounterdata = {};
                        Object.assign(this.uicounterdata, data);
                        if (this.$items) {
                            this.doMenus(this.$items);
                        }
                    });
                }
            });
        }
    }

    /**
     * 部件加载
     * 
     * @memberof IBizAppMenuService
     */
    public load(): void {
        const params: any = { srfctrlid: this.getName(), srfaction: 'FETCH' };
        this.post(params, this.getBackendUrl()).subscribe(success => {
            if (success.ret === 0) {
                this.$items = success.items;
                const data = this.doMenus(success.items);
                this.fire(IBizEvent.IBizAppMenu_LOADED, data);
            }
        }, error => {
            console.log(error);
        });
    }


    /**
     * 处理菜单数据
     * 
     * @private
     * @param {*} items 
     * @returns {*} 
     * @memberof StartupService
     */
    private doMenus(items: Array<any>): any[] {
        let datas: Array<any> = [];
        items.forEach(item => {
            const data = this.renderMenuItemRoute(item.appfuncid);
            if (data) {
                item.link = data.codename ? data.codename : data.funcid;
            }
            if (Object.is(item.iconcls, '')) {
                item.iconcls = 'fa fa-cogs';
            }
            if (Object.is(item.icon, '')) {
                item.icon = item.iconcls;
            }

            if (this.uicounterdata[item.counterid]) {
                item.badge = this.uicounterdata[item.counterid];
                this.fire(IBizEvent.IBizAppMenu_UPDATE, item);
            }

            item.children = [];
            if (item.items) {
                item.items.forEach(element => {
                    let data1 = this.renderMenuItemRoute(element.appfuncid);
                    if (data1) {
                        element.link = data1.codename ? data1.codename : data1.funcid;
                    }
                    if (Object.is(element.iconcls, '')) {
                        element.iconcls = 'fa fa-cogs';
                    }
                    if (Object.is(element.icon, '')) {
                        element.icon = element.iconcls;
                    }
                    item.children.push(element);

                    element.children = [];
                    if (element.items) {
                        element.items.forEach(eledata => {
                            let data2 = this.renderMenuItemRoute(eledata.appfuncid);
                            if (data2) {
                                eledata.link = data2.codename ? data2.codename : data2.funcid;
                            }
                            if (Object.is(eledata.iconcls, '')) {
                                eledata.iconcls = 'fa fa-cogs';
                            }
                            if (Object.is(eledata.icon, '')) {
                                eledata.icon = eledata.iconcls;
                            }
                            element.children.push(eledata);

                        });

                    }

                });
            }
            let children: Array<any> = [];
            children.push(item);
            datas.push({ children: children });
        });
        return datas;
    }

    /**
     * 处理表单项，在服务对象中重写
     * 
     * @param {string} appfuncid 
     * @returns {*} 
     * @memberof IBizAppMenuService
     */
    public renderMenuItemRoute(appfuncid: string): any {
        return undefined;
    }

    /**
     * 菜单选中
     * 
     * @param {*} select 
     * @returns {*} 
     * @memberof IBizAppMenuService
     */
    public onSelectChange(select: any): any {
        this.fire(IBizEvent.IBizAppMenu_MENUSELECTION, select);
    }

    /**
     * 获取菜单项数组
     *
     * @returns {any[]}
     * @memberof IBizAppMenuService
     */
    public getItems(): any[] {
        return this.$items;
    }


    /**
     * 获取菜单项
     *
     * @param {Array<any>} items
     * @param {string} [appfuncid]
     * @param {string} [link]
     * @param {string} [id]
     * @returns {*}
     * @memberof IBizAppMenuService
     */
    public getItem(items: Array<any>, appfuncid?: string, link?: string, id?: string): any {
        let item: any = {};
        items.some((_item: any) => {
            if (appfuncid && Object.is(appfuncid, _item.appfuncid)) {
                Object.assign(item, _item);
                return true;
            }
            if (link && Object.is(link, _item.link)) {
                Object.assign(item, _item);
                return true;
            }
            if (id && Object.is(id, _item.id)) {
                Object.assign(item, _item);
                return true;
            }

            if (_item.items && Array.isArray(_item.items) && _item.items.length > 0) {
                let subItem = this.getItem(_item.items, appfuncid, link, id);
                if (Object.keys(subItem).length > 0) {
                    Object.assign(item, subItem);
                    return true;
                }
            }

            return false;
        });
        return item;
    }

    /**
     * 获取第一项菜单项
     *
     * @param {any[]} items
     * @returns {*}
     * @memberof IBizAppMenuService
     */
    public getFirstItem(items: any[]): any {
        let item: any = {};
        items.some((_item: any) => {
            if (!Object.is(_item.link, '')) {
                Object.assign(item, _item);
                return true;
            }
            if (_item.items && Array.isArray(_item.items)) {
                let subItem = this.getFirstItem(_item.items);
                if (Object.keys(subItem).length > 0) {
                    Object.assign(item, subItem);
                    return true;
                }
            }
            return false;
        });
        return item;
    }

    /**
     * 获取引用计数器名称
     *
     * @returns {(string | undefined)}
     * @memberof IBizAppMenuService
     */
    public getRefUICounterName(): string | undefined {
        return undefined;
    }
}
