import { IBizFormService } from '@ibizsys/widget/IBizFormService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 搜索表单部件服务对象
 * 
 * @export
 * @class IBizSearchFormService
 * @extends {IBizFormService}
 */
export class IBizSearchFormService extends IBizFormService {

    /**
     * 是搜重置搜索
     * 
     * @type {boolean}
     * @memberof IBizSearchFormService
     */
    public $bResetting: boolean = false;

    /**
     * 是否有更多搜索
     * 
     * @memberof IBizSearchFormService
     */
    public $searchMore = false;

    /**
     * 搜索表单是否打开
     * 
     * @type {boolean}
     * @memberof IBizSearchFormService
     */
    public $opened: boolean = false;

    /**
     * Creates an instance of IBizSearchFormService.
     * 创建 IBizSearchFormService 实例
     * 
     * @param {*} [opt={}] 
     * @memberof IBizSearchFormService
     */
    constructor(opt: any = {}) {
        super(opt);
    }

    /**
     * 表单类型
     * 
     * @returns {string} 
     * @memberof IBizSearchFormService
     */
    public getFormType(): string {
        return 'SEARCHFORM';
    }

    /**
     * 更多搜索
     * 
     * @memberof IBizSearchFormService
     */
    public toggleSearchMore(): void {
        this.$searchMore = !this.$searchMore;
    }

    /**
     * 执行搜索功能
     * 
     * @memberof IBizSearchFormService
     */
    public onSearch(): void {
        this.fire(IBizEvent.IBizSearchForm_FORMSEARCHED, null);
    }

    /**
     * 重置表单
     * 
     * @memberof IBizSearchFormService
     */
    public onReset(): void {
        this.$bResetting = true;
        this.reset();
    }

    /**
     * 搜索表单草稿加载完成
     *
     * @memberof IBizSearchFormService
     */
    public onDraftLoaded(): void {
        super.onDraftLoaded();
        if (this.$bResetting) {
            this.$bResetting = false;
            this.fire(IBizEvent.IBizSearchForm_FORMRESETED, null);
        }
    }

    /**
     * 搜索表单加载完成
     *
     * @memberof IBizSearchFormService
     */
    public onLoaded(): void {
        super.onLoaded();
        if (this.$bResetting) {
            this.$bResetting = false;
            this.fire(IBizEvent.IBizSearchForm_FORMRESETED, null);
        }
    }


    /**
     * 搜索功能是否支持,全支持
     * 
     * @returns {boolean} 
     * @memberof IBizSearchFormService
     */
    public isOpen(): boolean {
        // return this.$opened;
        return true;
    }

    /**
     * 设置搜索表单是否展开
     * 
     * @param {boolean} open 
     * @memberof IBizSearchFormService
     */
    public setOpen(open: boolean): void {
        this.$opened = open;
    }

    /**
     * 关闭搜索功能
     * 
     * @memberof IBizSearchFormService
     */
    public close(): void {
        this.$opened = false;
    }
}

