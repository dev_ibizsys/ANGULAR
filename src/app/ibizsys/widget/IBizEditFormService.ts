import { IBizFormService } from '@ibizsys/widget/IBizFormService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 编辑表单控制器
 * 
 * @export
 * @class IBizEditFormService
 * @extends {IBizFormService}
 */
export class IBizEditFormService extends IBizFormService {

    /**
     * Creates an instance of IBizEditFormService.
     * 创建 IBizEditFormService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizEditFormService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据保存
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizEditFormService
     */
    public save2(opts: any = {}): void {

        if (!this.isAllowSave()) {
            this.showToast(this.$showErrorToast, '错误', '表单项输入错误');
            return;
        }

        let arg: any = {};
        this.fire(IBizEvent.IBizEditForm_FORMBEFORESAVE, arg);
        Object.assign(arg, opts);
        // if (IBizApp_Data)
        //     arg.srfappdata = IBizApp_Data;
        const data = this.getValues();

        // $.extend(arg, data);
        Object.assign(arg, data);

        if (Object.is(data.srfuf, '1')) {
            // $.extend(arg, { srfaction: 'update' });
            Object.assign(arg, { srfaction: 'update', srfctrlid: this.getName() });
        } else {
            // $.extend(arg, { srfaction: 'create' });
            Object.assign(arg, { srfaction: 'create', srfctrlid: this.getName() });
        }


        // 获取所有Disabled数据
        // var disablevalues = {};
        // $.each(this.fieldMap, function (name, item) {
        //     if (item.isDisabled()) {
        //         if (disablevalues[item.name] == undefined) {
        //             disablevalues[item.name] = item.getValue();
        //         }
        //     }
        // });
        // $.extend(arg, disablevalues);

        arg.srfcancel = false;
        // this.fireEvent(IBizEditForm.FORMBEFORESAVE, this, arg);
        if (arg.srfcancel) {
            return;
        }
        delete arg.srfcancel;

        this.$ignoreUFI = true;
        this.$ignoreformfieldchange = true;

        this.submit(arg).subscribe((action) => {
            this.resetFormError();
            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            this.setDataAccAction(action.dataaccaction);
            this.fillForm(action.data);
            this.$formDirty = false;
            // 判断是否有提示
            if (action.info && !Object.is(action.info, '')) {
                // IBiz.alert('', action.info, 1);
                this.showToast(this.$showInfoToast, '', action.info);
            }
            // this.fireEvent('formsaved', this, action);
            this.$ignoreUFI = false;
            this.$ignoreformfieldchange = false;
            this.fire(IBizEvent.IBizForm_FORMSAVED, this);
            // this.fireEvent('formfieldchanged', null);
            this.fire(IBizEvent.IBizForm_FORMFIELDCHANGED, null);
            this.onSaved();
        }, (action) => {
            if (action.error) {
                this.setFormError(action.error, 'BACKEND');
            }

            this.$ignoreUFI = false;
            this.$ignoreformfieldchange = false;

            // this.fireEvent(IBizEditForm.FORMSAVEERROR, this);
            this.fire(IBizEvent.IBizEditForm_FORMSAVEERROR, this);

            action.failureType = 'SERVER_INVALID';
            if (action.ret === 10) {
                this.$notification.confirm('保存错误信息', '保存数据发生错误, ' + this.getActionErrorInfo(action) + ', 是否要重新加载数据？').subscribe((result) => {
                    if (result && Object.is(result, 'OK')) {
                        this.reload();
                    }
                });
            } else {
                this.showToast(this.$showErrorToast, '保存错误信息', '保存数据发生错误,' + this.getActionErrorInfo(action));
            }
        });
    }

    /**
     * 
     * 
     * @memberof IBizEditFormService
     */
    public onSaved(): void {

    }

    /**
     * 表单数据刷新
     * 
     * @memberof IBizEditFormService
     */
    public reload(): void {

        let field = this.findField('srfkey');
        let loadarg: any = {};
        if (field) {
            loadarg.srfkey = field.getValue();
            if (loadarg.srfkey.indexOf('SRFTEMPKEY:') === 0) {
                field = this.findField('srforikey');
                if (field) {
                    loadarg.srfkey = field.getValue();
                }
            }
            const viewController = this.getViewController();
            if (viewController) {
                const viewParmams: any = viewController.getViewParam();
                if (!Object.is(loadarg.srfkey, viewParmams.srfkey)) {
                    loadarg.srfkey = viewParmams.srfkey;
                }
            }
        }
        this.autoLoad(loadarg);
    }

    /**
     * 
     * 
     * @param {any} arg 
     * @returns {void} 
     * @memberof IBizEditFormService
     */
    public remove(arg): void {

        if (!arg) {
            arg = {};
        }

        // if (IBizApp_Data)
        //     arg.srfappdata = IBizApp_Data;
        if (!arg.srfkey) {
            let field = this.findField('srfkey');
            if (field) {
                arg.srfkey = field.getValue();
            }
        }

        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert($IGM('IBIZEDITFORM.REMOVEFAILED.TITLE', '删除错误信息'), $IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.showToast(this.$showErrorToast, '删除错误信息', '当前表单未加载数据！');
            return;
        }
        // $.extend(arg, { srfaction: 'remove' });
        Object.assign(arg, { srfaction: 'remove', srfctrlid: this.getName() });
        this.$ignoreUFI = true;

        this.load(arg).subscribe((action) => {
            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            // this.fireEvent(IBizForm.FORMREMOVED);
            this.fire(IBizEvent.IBizForm_FORMREMOVED, this);
        }, (action) => {
            action.failureType = 'SERVER_INVALID';
            this.showToast(this.$showErrorToast, '删除错误信息', '删除数据发生错误, ' + this.getActionErrorInfo(action));
            this.$ignoreUFI = false;
        });
    }

    /**
     * 
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizEditFormService
     */
    public wfstart(arg: any = {}): void {

        if (!this.isAllowSave()) {
            this.showToast(this.$showErrorToast, '错误', '表单项输入错误');
            return;
        }

        if (!arg) {
            arg = {};
        }

        // if (IBizApp_Data)
        //     arg.srfappdata = IBizApp_Data;
        if (!arg.srfkey) {
            let field = this.findField('srfkey');
            if (field) {
                arg.srfkey = field.getValue();
            }
            field = this.findField('srforikey');
            if (field) {
                let v = field.getValue();
                if (v && !Object.is(v, '')) {
                    arg.srfkey = v;
                }
            }
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert($IGM('IBIZEDITFORM.WFSTARTFAILED.TITLE', '启动流程错误信息'), $IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.showToast(this.$showErrorToast, '启动流程错误信息', '当前表单未加载数据！');
            return;
        }

        // $.extend(arg, { srfaction: 'wfstart' });
        Object.assign(arg, { srfaction: 'wfstart', srfctrlid: this.getName() });
        this.$ignoreUFI = true;
        this.$ignoreformfieldchange = true;

        this.post(arg).subscribe((action) => {
            if (action.ret !== 0) {
                action.failureType = 'SERVER_INVALID';
                this.showToast(this.$showErrorToast, '启动流程错误信息', '启动流程发生错误,' + this.getActionErrorInfo(action));
                this.$ignoreUFI = false;
                this.$ignoreformfieldchange = false;
                return;
            }

            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            this.setDataAccAction(action.dataaccaction);
            this.fillForm(action.data);
            this.$formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED);
            // this.fireEvent(IBizForm.FORMWFSTARTED);
            this.$ignoreUFI = false;
            this.$ignoreformfieldchange = false;
            this.fire(IBizEvent.IBizForm_FORMWFSTARTED, this);
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            this.fire(IBizEvent.IBizForm_FORMFIELDCHANGED, null);
        }, (action) => {
            if (action.error) {
                this.setFormError(action.error, 'BACKEND');
            }
            action.failureType = 'SERVER_INVALID';
            this.showToast(this.$showErrorToast, '启动流程错误信息', '启动流程发生错误,' + this.getActionErrorInfo(action));
            this.$ignoreUFI = false;
            this.$ignoreformfieldchange = false;
        });
    }

    /**
     * 
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizEditFormService
     */
    public wfsubmit(arg: any = {}): void {

        if (!this.isAllowSave()) {
            this.showToast(this.$showErrorToast, '错误', '表单项输入错误');
            return;
        }

        if (!arg) {
            arg = {};
        }

        // if (IBizApp_Data)
        //     arg.srfappdata = IBizApp_Data;

        let data = this.getValues();
        // $.extend(arg, data);
        Object.assign(arg, data);
        // $.extend(arg, { srfaction: 'wfsubmit' });
        Object.assign(arg, { srfaction: 'wfsubmit', srfctrlid: this.getName() });

        //        if (!arg.srfkey) {
        //            var field = this.findField('srfkey');
        //            if (field) {
        //                arg.srfkey = field.getValue();
        //            }
        //        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert($IGM('IBIZEDITFORM.WFSUBMITFAILED.TITLE', '提交流程错误信息'), $IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.showToast(this.$showErrorToast, '提交流程错误信息', '当前表单未加载数据！');
            return;
        }

        this.$ignoreUFI = true;
        this.$ignoreformfieldchange = true;

        this.load(arg).subscribe((action) => {
            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            this.setDataAccAction(action.dataaccaction);
            this.fillForm(action.data);
            this.$formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED);
            // this.fireEvent(IBizForm.FORMWFSUBMITTED);
            this.$ignoreUFI = false;
            this.$ignoreformfieldchange = false;
            this.fire(IBizEvent.IBizForm_FORMWFSUBMITTED, this);
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            this.fire(IBizEvent.IBizForm_FORMFIELDCHANGED, null);
        }, (action) => {
            if (action.error) {
                this.setFormError(action.error, 'BACKEND');
            }
            action.failureType = 'SERVER_INVALID';
            this.showToast(this.$showErrorToast, '提交流程错误信息', '工作流提交发生错误,' + this.getActionErrorInfo(action));
            this.$ignoreUFI = false;
            this.$ignoreformfieldchange = false;
        });
    }

    /**
     * 界面行为
     * 
     * @param {*} [arg={}] 
     * @memberof IBizEditFormService
     */
    public doUIAction(arg: any = {}): void {
        let opt: any = {};
        if (arg) {
            Object.assign(opt, arg);
        }
        Object.assign(opt, { srfaction: 'uiaction', srfctrlid: this.getName() });

        this.post(opt).subscribe((data) => {
            if (data.ret === 0) {
                // IBiz.processResultBefore(data);
                this.fire(IBizEvent.IBizEditForm_UIACTIONFINISHED, data);
                if (data.reloadData) {
                    this.reload();
                }
                if (data.info && !Object.is(data.info, '')) {
                    this.showToast(this.$showInfoToast, '', data.info);
                }
                // IBiz.processResult(data);
            } else {
                this.showToast(this.$showErrorToast, '界面操作错误信息', '操作失败,' + data.errorMessage);
            }
        }, (error) => {
            this.showToast(this.$showErrorToast, '界面操作错误信息', '操作失败,' + error.info);
        });

    }

    /**
     * 表单类型
     *
     * @returns {string}
     * @memberof IBizEditFormService
     */
    public getFormType(): string {
        return 'EDITFORM';
    }

    /**
     * 是否允许保存
     *
     * @returns {boolean}
     * @memberof IBizEditFormService
     */
    public isAllowSave(): boolean {
        let allow = true;
        const fieldNames = Object.keys(this.$fields);
        fieldNames.some(name => {
            const field = this.findField(name);
            if (field && field.hasActiveError() && Object.is(field.getErrorType(), 'FRONTEND')) {
                allow = false;
                return true;
            }
        });
        return allow;
    }
}