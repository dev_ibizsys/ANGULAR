import { IBizEvent } from '@ibizsys/IBizEvent';
import { IBizViewPanelService } from '@ibizsys/widget/IBizViewPanelService';

/**
 * 拾取数据视图面板
 * 
 * @export
 * @class IBizPickupViewPanelService
 * @extends {IBizViewPanelService}
 */
export class IBizPickupViewPanelService extends IBizViewPanelService {

    /**
     * 父数据
     *
     * @type {*}
     * @memberof IBizPickupViewPanelService
     */
    public $parentData: any = {};

    /**
     * 选中数据
     *
     * @type {Array<any>}
     * @memberof IBizPickupViewPanelService
     */
    public $selections: Array<any> = [];

    /**
     * 所有数据
     *
     * @type {Array<any>}
     * @memberof IBizPickupViewPanelService
     */
    public $allData: Array<any> = [];

    /**
     * Creates an instance of IBizPickupViewPanelService.
     * 创建 IBizPickupViewPanelService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizPickupViewPanelService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 获取所有数据
     *
     * @returns {Array<any>}
     * @memberof IBizPickupViewPanelService
     */
    public getAllData(): Array<any> {
        return this.$allData;
    }

    /**
     * 获取所有选中数据
     *
     * @returns {Array<any>}
     * @memberof IBizPickupViewPanelService
     */
    public getSelections(): Array<any> {
        return this.$selections;
    }

    /**
     * 数据选中
     *
     * @param {Array<any>} $event
     * @memberof IBizPickupViewPanelService
     */
    public onSelectionChange($event: Array<any>): void {
        this.$selections = $event;
        this.fire(IBizEvent.IBizPickupViewPanel_SELECTIONCHANGE, this.$selections);
    }

    /**
     * 数据激活
     *
     * @param {Array<any>} $event
     * @memberof IBizPickupViewPanelService
     */
    public onDataActivated($event: Array<any>): void {
        this.$selections = $event;
        this.fire(IBizEvent.IBizPickupViewPanel_DATAACTIVATED, this.$selections);
    }

    /**
     * 全部数据
     *
     * @param {Array<any>} $event
     * @memberof IBizPickupViewPanelService
     */
    public onAllData($event: Array<any>): void {
        this.$allData = $event;
        this.fire(IBizEvent.IBizPickupViewPanel_ALLDATA, this.$allData);
    }

    /**
     * 设置父数据
     *
     * @param {*} [parentData={}]
     * @memberof IBizPickupViewPanelService
     */
    public setParentData(parentData: any = {}): void {
        this.$parentData = parentData;
    }

    /**
     * 刷新面板
     *
     * @memberof IBizPickupViewPanelService
     */
    public refreshViewPanel(): void {
    }
}