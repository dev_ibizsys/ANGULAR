import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 树部件服务对象
 * 
 * @export
 * @class IBizTreeService
 * @extends {IBizService}
 */
export class IBizTreeService extends IBizService {

    /**
     * 树部件是否收缩，默认展开
     * 
     * @type {boolean}
     * @memberof IBizTreeService
     */
    public $isCollapsed: boolean = true;

    /**
     * 数据项节点集合
     * 
     * @type {Array<any>}
     * @memberof IBizTreeService
     */
    public $items: Array<any> = [];

    /**
     * 默认节点
     * 
     * @private
     * @type {*}
     * @memberof IBizTreeService
     */
    private node: any = {};

    /**
     * 树节点操作行为处理
     * 
     * @memberof IBizTreeService
     */
    public options = {
        getChildren: (node: any) => {
            if (node && node.data) {
                this.node = node.data;
            }
            return new Promise((resolve, reject) => {
                this.loadChildren(resolve);
            });
        }
    };

    /**
     * Creates an instance of IBizTreeService.
     * 创建 IBizTreeService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizTreeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 加载节点数据
     * 
     * @param {*} [treeCfg={}] 
     * @memberof IBizTreeService
     */
    public load(treeCfg: any = {}): void {
        let param: any = {
            srfnodeid: this.node.id ? this.node.id : '#', srfaction: 'fetch', srfrender: 'JSTREE',
            srfviewparam: JSON.stringify(this.getViewController().getViewParam()),
            srfctrlid: this.getName()
        };

        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, param);

        this.post(param).subscribe((result) => {
            if (result.ret !== 0) {
                this.showToast(this.$showErrorToast, '错误', result.info);
                return;
            }
            this.$items = this.formatTreeData(result.items);
            this.fire(IBizEvent.IBizTree_CONTEXTMENU, this.$items);
        }, (error) => {
            this.showToast(this.$showErrorToast, '错误', error.info);
        });
    }

    /**
     * 获取选择节点数据
     * 
     * @param {any} bFull true：返回的数据包含节点全部数据，false：返回的数据仅包含节点ID
     * @returns {*} 
     * @memberof IBizTreeService
     */
    public getSelected(bFull: boolean): any {

    }

    /**
     * 清除选中节点
     *
     * @memberof IBizTreeService
     */
    public cleanSelectNode(): void {
        this.node = {};
    }

    /**
     * 获取所有节点数据
     *
     * @returns {Array<any>}
     * @memberof IBizTreeService
     */
    public getNodes(): Array<any> {
        return this.$items;
    }

    /**
     * 节点重新加载
     * 
     * @param {*} [node={}] 
     * @memberof IBizTreeService
     */
    public reload(data: any = {}): void {
        this.node = {};
        this.load(data);
    }

    /**
     * 删除节点
     * 
     * @param {any} node 
     * @memberof IBizTreeService
     */
    public remove(node: any): void {

    }

    /**
     * 实体界面行为
     * 
     * @param {any} params 
     * @memberof IBizTreeService
     */
    public doUIAction(params: any): void {

    }

    /**
     * 格式化树数据
     * 
     * @private
     * @param {Array<any>} items 
     * @returns {Array<any>} 
     * @memberof IBizTreeService
     */
    private formatTreeData(items: Array<any>): Array<any> {
        let data: Array<any> = [];
        items.forEach((item) => {
            let tempData: any = {};
            Object.assign(tempData, item);
            tempData.name = tempData.text;
            tempData.hasChildren = true;
            data.push(tempData);

        });
        return data;
    }

    /**
     * 树节点激活加载子数据
     *
     * @private
     * @param {*} resolve
     * @memberof IBizTreeService
     */
    private loadChildren(resolve: any): void {
        let param: any = {
            srfnodeid: this.node.id ? this.node.id : '#', srfaction: 'fetch', srfrender: 'JSTREE',
            srfviewparam: JSON.stringify(this.getViewController().getViewParam()),
            srfctrlid: this.getName()
        };

        this.fire(IBizEvent.IBizMDControl_BEFORELOAD, param);

        this.post2(param).subscribe((result) => {
            if (result.ret !== 0) {
                this.showToast(this.$showErrorToast, '错误', result.info);
                resolve([]);
                this.node.hasChildren = false;
                return;
            }
            const _items = [...this.formatTreeData(result.items)];
            if (_items.length === 0) {
                this.node.hasChildren = false;
            }
            resolve(_items);

        }, (error) => {
            this.showToast(this.$showErrorToast, '错误', error.info);
            this.node.hasChildren = false;
            resolve([]);
        });
    }

    /**
     * 树节点激活选中数据
     * 
     * @param {*} $event 
     * @memberof IBizTreeService
     */
    public onEvent($event: any): void {
        if ($event && Object.is($event.eventName, 'activate')) {
            this.fire(IBizEvent.IBizTree_SELECTIONCHANGE, [$event.node.data]);
        }
        if ($event && Object.is($event.eventName, 'deactivate')) {
            this.fire(IBizEvent.IBizTree_DATAACTIVATED, [$event.node.data]);
        }
    }
}