import { IBizService } from '@ibizsys/widget/IBizService';

/**
 * 门户部件服务对象
 * 
 * @export
 * @class IBizPortalService
 * @extends {IBizService}
 */
export class IBizPortalService extends IBizService {

    /**
     * Creates an instance of IBizPortalService.
     * 创建 IBizPortalService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizPortalService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

