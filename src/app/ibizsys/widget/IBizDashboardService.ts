import { IBizService } from '@ibizsys/widget/IBizService';

/**
 * 数据看板
 *
 * @export
 * @class IBizDashboardService
 * @extends {IBizService}
 */
export class IBizDashboardService extends IBizService {

    /**
     * Creates an instance of IBizDashboardService.
     * 创建 IBizDashboardService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizDashboardService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}