import { IBizService } from '@ibizsys/widget/IBizService';

/**
 * 首页视图部件服务对象
 * 
 * @export
 * @class IBizIndexViewService
 * @extends {IBizService}
 */
export class IBizIndexViewService extends IBizService {

    /**
     * Creates an instance of IBizIndexViewService.
     * 创建 IBizIndexViewService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizIndexViewService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

