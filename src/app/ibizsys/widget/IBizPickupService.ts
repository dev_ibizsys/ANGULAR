import { IBizListService } from '@ibizsys/widget/IBizListService';

/**
 * 选择部件服务对象
 * 
 * @export
 * @class IBizPickupService
 * @extends {IBizListService}
 */
export class IBizPickupService extends IBizListService {

    /**
     * Creates an instance of IBizPickupService.
     * 创建 IBizPickupService 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizPickupService
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
