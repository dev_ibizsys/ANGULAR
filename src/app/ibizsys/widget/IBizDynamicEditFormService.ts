import { IBizEditFormService } from '@ibizsys/widget/IBizEditFormService';

export class IBizDynamicEditFormService extends IBizEditFormService {

    constructor(opt: any = {}) {
        super(opt);
    }

}