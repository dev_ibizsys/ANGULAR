import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { NzNotificationService, NzModalService } from 'ng-zorro-antd';

/**
 * 消息提示工具类
 * 参考地址： https://ng-zorro.github.io/#/components/notification
 * 
 * @export
 * @class IBizNotification
 */
@Injectable()
export class IBizNotification {

    private $sureConfirm = 'OK';

    constructor(
        private nznotification: NzNotificationService,
        private Modal: NzModalService
    ) { }

    /**
     * 不带图标的提示
     * 
     * @param {string} title 标题
     * @param {string} content 内容
     * @param {Object} [options]  其他参数
     * @memberof IBizNotification
     */
    public blank(title: string, content: string, options?: Object) {
        this.nznotification.blank(title, content, options);
    }

    /**
     * 成功提示
     * 
     * @param {string} title 标题
     * @param {string} content 内容
     * @param {Object} [options]  其他参数
     * @memberof IBizNotification
     */
    public success(title: string, content: string, options?: Object) {
        this.nznotification.success(title, content, options);
    }

    /**
     * 失败提示
     * 
     * @param {string} title 标题
     * @param {string} content 内容
     * @param {Object} [options]  其他参数
     * @memberof IBizNotification
     */
    public error(title: string, content: string, options?: Object) {
        this.nznotification.error(title, content, options);
    }

    /**
     * 警告提示
     * 
     * @param {string} title 标题
     * @param {string} content 内容
     * @param {Object} [options]  其他参数
     * @memberof IBizNotification
     */
    public warning(title: string, content: string, options?: Object) {
        this.nznotification.warning(title, content, options);
    }

    /**
     * 信息提示
     * 
     * @param {string} title 标题
     * @param {string} content 内容
     * @param {Object} [options]  其他参数
     * @memberof IBizNotification
     */
    public info(title: string, content: string, options?: Object) {
        this.nznotification.info(title, content, options);
    }

    /**
     * 可使用html代码来渲染内容
     * 
     * @param {string} html html 内容
     * @param {Object} [options]  其他参数
     * @memberof IBizNotification
     */
    public html(html: string, options?: Object) {
        this.nznotification.html(html, options);
    }

    /**
     * 移除特定id的消息，当id为空时，移除所有消息
     * 
     * @param {string} [id] ID
     * @memberof IBizNotification
     */
    public remove(id?: string) {
        this.nznotification.remove(id);
    }

    /**
     * 
     * 
     * @param {string} title 
     * @param {string} content 
     * @returns 
     * @memberof IBizNotification
     */
    public confirm(title: string, content: string): Observable<any> {
        return this.Modal.confirm({
            title: title,
            content: content,
        }).map(result => {
            if (result && Object.is(result, 'onOk')) {
                return this.$sureConfirm;
            }
        });
    }
}
