
/**
 * 工具资源文件
 *
 * @export
 * @class IBizHandResponseData
 */
export class IBizHandResponseData {

    /**
     * 处理错误结痂
     *
     * @static
     * @param {*} response
     * @returns {*}
     * @memberof IBizHandResponseData
     */
    public static doErrorResponseData(response: any): any {
        let data: any = { failureType: 'SERVER_INVALID', info: '本地网络异常，请重试', errorMessage: '本地网络异常，请重试' };
        let errorInfo = '';
        switch (response.status) {
            case 200:
                break;
            case 400:
                errorInfo = '错误的请求(400)，请重试';
                break;
            case 401:
                errorInfo = '用户未认证(401)，请重试';
                break;
            case 403:
                errorInfo = '服务器拒绝请求(403)，请重试';
                break;
            case 404:
                errorInfo = '服务器拒绝请求(404)，请重试';
                break;
            case 405:
                errorInfo = '请求方法被禁用(405)，请重试';
                break;
            case 406:
                errorInfo = '请求的资源不满足请求头的条件(406)，请重试';
                break;
            case 407:
                errorInfo = '用户未代理认证(407)，请重试';
                break;
            case 408:
                errorInfo = '服务器等待请求超时(408)，请重试';
                break;
            case 409:
                errorInfo = '请求的资源存在冲突(409)，请重试';
                break;
            case 410:
                errorInfo = '请求的资源已过期(410)，请重试';
                break;
            case 411:
                errorInfo = '服务器拒绝在没有定义 Content-Length 头的情况下接受请求(411)，请重试';
                break;
            case 412:
                errorInfo = '客户端请求信息的先决条件错误(412)，请重试';
                break;
            case 413:
                errorInfo = '请求的实体过大，服务器无法处理(413)，请重试';
                break;
            case 414:
                errorInfo = '请求的URI过长(414)，请重试';
                break;
            case 415:
                errorInfo = '服务器无法处理请求附带的媒体格式(415)，请重试';
                break;
            case 416:
                errorInfo = '客户端请求的范围无效(416)，请重试';
                break;
            case 417:
                errorInfo = '服务器无法满足Expect的请求头信息(417)，请重试';
                break;
            case 421:
                errorInfo = '从当前客户端所在的IP地址到服务器的连接数超过了服务器许可的最大范围(421)，请重试';
                break;
            case 422:
                errorInfo = '请求有语义错误(422)，请重试';
                break;
            case 423:
                errorInfo = '当前资源被锁定(423)，请重试';
                break;
            case 424:
                errorInfo = '之前的某个请求发生错误(424)，请重试';
                break;
            case 426:
                errorInfo = '客户端应当切换到TLS/1.0(426)，请重试';
                break;
            case 500:
                errorInfo = '服务器内部错误(500)，请重试';
                break;
            case 501:
                errorInfo = '服务器不支持请求的功能(501)，请重试';
                break;
            case 502:
                errorInfo = '网关或代理服务器请求无响应(502)，请重试';
                break;
            case 503:
                errorInfo = '临时的服务器维护或者过载(503)，请重试';
                break;
            case 504:
                errorInfo = '网关或代理服务器请求超时(504)，请重试';
                break;
            case 505:
                errorInfo = '服务器不支持请求的HTTP协议的版本(505)，请重试';
                break;
            case 506:
                errorInfo = '服务器存在内部配置错误(506)，请重试';
                break;
            case 507:
                errorInfo = '服务器无法存储完成请求所必须的内容(507)，请重试';
                break;
            case 509:
                errorInfo = '服务器达到带宽限制(509)，请重试';
                break;
            case 510:
                errorInfo = '获取资源所需要的策略并没有没满足(510)，请重试';
                break;
        }
        if (!Object.is(errorInfo, '')) {
            Object.assign(data, { info: errorInfo, errorMessage: errorInfo });
        }
        return data;
    }
}