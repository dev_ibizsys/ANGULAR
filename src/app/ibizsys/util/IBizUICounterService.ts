import { IBizService } from '@ibizsys/widget/IBizService';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 计数器服务对象
 * 
 * @export
 * @class IBizUICounterService
 * @extends {IBizService}
 */
export class IBizUICounterService extends IBizService {

    /**
     * 定时器时间
     * 
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $timer: any;

    /**
     * 定时器
     * 
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $timerTag: any;

    /**
     * 计数器id
     * 
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $counterId: any;

    /**
     * 计数器参数
     * 
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $counterParam: any = {};

    /**
     * 最后加载数据
     * 
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $lastReloadArg: any = {};

    /**
     * 计数器结果
     * 
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $result: any;

    /**
     * 计数器交互数据
     *
     * @private
     * @type {*}
     * @memberof IBizUICounterService
     */
    private $data: any = {};

    /**
     * Creates an instance of IBizUICounterService.
     * 创建 IBizUICounterService 服务对象
     * 
     * @param {*} [config={}]
     * @memberof IBizUICounterService
     */
    constructor(config: any = {}) {
        super(config);

        this.$counterId = config.counterId;
        Object.assign(this.$counterParam, config.counterParam);
        this.$timer = config.timer;
        this.load();
    }

    /**
     * 加载定时器
     * 
     * @memberof IBizUICounter
     */
    private load(): void {
        if (this.$timer > 1000) {
            this.$timerTag = setInterval(() => {
                this.reload();
            }, this.$timer);
        }
        this.reload();
    }

    /**
     * 刷新计数器
     * 
     * @private
     * @param {*} [arg={}] 
     * @memberof IBizUICounterService
     */
    public reload(arg: any = {}): void {
        let params: any = {};
        Object.assign(this.$lastReloadArg, arg);
        Object.assign(params, this.$lastReloadArg);
        Object.assign(params, { srfcounterid: this.$counterId, srfaction: 'FETCH', srfcounterparam: JSON.stringify(this.$counterParam) });
        this.$IBizHttp.post2(this.getBackendUrl(), params).subscribe(
            res => {
                if (res.ret === 0) {
                    this.setData(res);
                }
            },
            error => {
                console.log('加载计数器异常');
            }
        );
    }

    /**
     * 处理数据
     * 
     * @private
     * @param {*} result 
     * @memberof IBizUICounterService
     */
    private setData(result: any): void {
        this.$result = result;
        this.$data = result.data;
        this.fire(IBizEvent.COUNTERCHANGE, this.$data);
    }

    /**
     * 获取结果
     * 
     * @returns {*} 
     * @memberof IBizUICounterService
     */
    public getResult(): any {
        return this.$result;
    }

    /**
     * 获取数据
     * 
     * @returns {*} 
     * @memberof IBizUICounterService
     */
    public getData(): any {
        return this.$data;
    }

    /**
     * 关闭计数器
     * 
     * @memberof IBizUICounter
     */
    public close(): void {
        if (this.$timerTag !== undefined) {
            clearInterval(this.$timerTag);
            delete this.$timer;
        }
    }
}

