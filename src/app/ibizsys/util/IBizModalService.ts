import { IBizService } from '../widget/IBizService';
import { NzModalService } from 'ng-zorro-antd';
import { Observable, Subject } from 'rxjs/Rx';


/**
 * 模态框服务对象
 * 
 * @export
 * @class IBizModalService
 * @extends {IBizService}
 */
export class IBizModalService extends IBizService {

    /**
     * 模态框服务对象
     * 
     * @private
     * @type {NzModalService}
     * @memberof IBizModalService
     */
    private $modal: NzModalService;

    /**
     * 模态框处理结果数据
     * 
     * @private
     * @type {*}
     * @memberof IBizModalService
     */
    private $result: any = {};

    /**
     * Creates an instance of IBizModalService.
     * 创建 IBizModalService 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizModalService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.$modal = opts.modalCtrl;
    }

    /**
     * 打开模态框, 只返回数据变化后的值
     * 
     * @param {*} [options={}] 
     * @returns {Observable<any>} 
     * @memberof IBizModalService
     */
    public openModal(options: any = {}): Observable<any> {
        if (!Object.is(this.viewOpenMode(), 'POPUPMODAL')) {
            this.showToast(this.$showWarningToast, '警告', '该视图的打开方式请选择模式弹出！');
            return new Subject();
        }
        let opt = this.getModalParam(options);
        return this.$modal.open(opt).map(result => {
            if (result.ret && Object.is(result.ret, 'OK')) {
                this.$result = {};
                Object.assign(this.$result, result);
            }
            if (result && Object.is(result, 'DATACHANGE')) {
                return this.$result;
            }
        });
    }

    /**
     * 获取模态框参数
     * 
     * @param {*} [opt={}] 
     * @param {*} [options] 
     * @returns {*} 
     * @memberof IBizModalService
     */
    public getModalParam(opt: any = {}, options?: any): any {
        opt.modalZIndex = opt.modalZIndex ? opt.modalZIndex : 300;
        let zIndex = opt.modalZIndex + 100;

        if (Object.is(options.width, 0)) {
            options.width = this.getModalWidth();
        }

        Object.assign(options, {
            componentParams: {
                modalViewParam: opt.viewParam ? opt.viewParam : {},
                modalZIndex: zIndex,
            },
            zIndex: zIndex
        });
        return options;
    }

    /**
     * 获取窗口对象宽度，默认600
     * 
     * @returns {number} 
     * @memberof IBizModalService
     */
    public getModalWidth(): number {
        if (window && window.innerWidth > 200) {
            if (window.innerWidth > 200) {
                return window.innerWidth - 200;
            } else {
                return window.innerWidth;
            }
        }
        return 600;
    }

    /**
     * 视图打开模式
     * 
     * @returns {string} 
     * @memberof IBizModalService
     */
    public viewOpenMode(): string {
        return undefined;
    }
}

