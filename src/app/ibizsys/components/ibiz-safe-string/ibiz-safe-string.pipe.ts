import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * 去掉xss保护，默认字符串拼接代码为安全的html
 * 
 * @export
 * @class IBizSafeHtmlPipe
 * @implements {PipeTransform}
 */
@Pipe({ name: 'IBizSafeString' })
export class IBizSafeHtmlPipe implements PipeTransform {

    constructor(private sanitizer: DomSanitizer) {
    }

    /**
     * 转换代码
     * 
     * @param {string} code 代码
     * @param {string} type 代码类型
     * @returns  
     * @memberof IBizSafeHtmlPipe
     */
    transform(code: string, type: string) {
        if (Object.is(type, 'html')) {
            return this.sanitizer.bypassSecurityTrustHtml(code);
        } else if (Object.is(type, 'style')) {
            return this.sanitizer.bypassSecurityTrustStyle(code);
        } else if (Object.is(type, 'script')) {
            return this.sanitizer.bypassSecurityTrustScript(code);
        } else if (Object.is(type, 'url')) {
            return this.sanitizer.bypassSecurityTrustUrl(code);
        } else if (Object.is(type, 'resourceurl')) {
            return this.sanitizer.bypassSecurityTrustResourceUrl(code);
        }
    }
}