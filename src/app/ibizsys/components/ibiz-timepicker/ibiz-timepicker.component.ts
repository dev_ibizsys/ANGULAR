import { Component, Input } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-ibiz-timepicker',
    templateUrl: './ibiz-timepicker.component.html',
    styleUrls: ['./ibiz-timepicker.component.less']
})
export class IBizTimepickerComponent {

    /**
     * 编辑器值
     *
     * @type {string}
     * @memberof IBizTimepickerComponent
     */
    public value: string;

    /**
     * 表单部件对象
     *
     * @type {*}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    form: any;

    /**
     * 表格部件对象，行编辑使用
     *
     * @type {*}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    grid: any;

    /**
     * 表格数据，行编辑使用
     *
     * @type {*}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    data: any;

    /**
     * 表单值绑定
     *
     * @memberof IBizTimepickerComponent
     */
    @Input()
    set itemvalue(val: string) {
        this.value = val;
    }

    /**
     * 编辑器名称
     *
     * @type {string}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    name: string;

    /**
     * 编辑器样式
     *
     * @type {*}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    styleCss: any;

    /**
     * 时间编辑器格式
     *
     * @type {string}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    datefmt: string;

    /**
     * 编辑器是否启用
     *
     * @type {string}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    disabled: string;

    /**
     * 编辑器提示信息
     *
     * @type {string}
     * @memberof IBizTimepickerComponent
     */
    @Input()
    placeholder: string;

    /**
     * Creates an instance of IBizTimepickerComponent.
     * 创建 IBizTimepickerComponent 实例
     * 
     * @param {DatePipe} datePipe
     * @memberof IBizTimepickerComponent
     */
    constructor(private datePipe: DatePipe) { }

    /**
     * 数据发生改变
     *
     * @param {Date} value
     * @memberof IBizTimepickerComponent
     */
    public valueChange(value: Date): void {
        let val = this.datePipe.transform(value, this.datefmt);
        val = val == null ? '' : val;
        if (this.form) {
            let itemField = this.form.findField(this.name);
            if (itemField) {
                itemField.setValue(val);
            }
        }
        if (this.grid) {
            this.grid.colValueChange(this.name, val, this.data);
        }
    }

}
