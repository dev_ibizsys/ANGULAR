import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-ibiz-textarea-10',
    templateUrl: './ibiz-textarea-10.component.html',
    styles: []
})
export class IBizTextarea10Component {

    /**
     * 编辑器名称
     *
     * @type {*}
     * @memberof IBizTextarea10Component
     */
    @Input()
    name: any;

    /**
     * 表单对象
     *
     * @type {*}
     * @memberof IBizTextarea10Component
     */
    @Input()
    form: any;

    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof IBizTextarea10Component
     */
    @Input()
    disabled: boolean;

    /**
     * 定时
     *
     * @type {*}
     * @memberof IBizTextareaComponent
     */
    public timeOut: any;

    /**
     * 值属性
     *
     * @type {*}
     * @memberof IBizTextarea10Component
     */
    public value: any;

    /**
     * 样式对象
     *
     * @type {*}
     * @memberof IBizTextarea10Component
     */
    @Input()
    styleCss: any;

    /**
     * ID
     *
     * @type {string}
     * @memberof IBizTextarea10Component
     */
    @Input()
    id: string;

    /**
     * 用户字典id
     *
     * @type {string}
     * @memberof IBizTextarea10Component
     */
    @Input()
    dictId: string;

    /**
     * 用户词条数组
     *
     * @type {*}
     * @memberof IBizTextarea10Component
     */
    @Input()
    dictitems: any;

    /**
     * 下拉选选中的值
     *
     * @type {string}
     * @memberof IBizTextarea10Component
     */
    public selectedValue: string = '';

    /**
     * 设置值
     *
     * @memberof IBizTextarea10Component
     */
    @Input()
    set itemvalue(val: any) {
        this.value = val ? val : '';
    }

    /**
     * 提示信息
     *
     * @type {string}
     * @memberof IBizTextarea10Component
     */
    @Input() placeholder: string;

    constructor() { }

    /**
     * 数据发生改变，触发表单项更新
     *
     * @param {string} newVal
     * @memberof IBizTextarea10Component
     */
    public valueChange(newVal: string): void {
        if (this.form) {
            if (this.timeOut) {
                clearTimeout(this.timeOut);
                this.timeOut = undefined;
            }
            this.timeOut = setTimeout(() => {
                if (this.name && !Object.is(this.name, '')) {
                    let itemField = this.form.findField(this.name);
                    if (itemField) {
                        itemField.setValue(newVal);
                    }
                }
            }, 300);
        }
    }

    /**
     * 下拉选择回调
     *
     * @param {*} value
     * @memberof IBizTextarea10Component
     */
    public setValue(value: any): void {
        if (this.dictitems) {
            this.dictitems.some((item) => {
                if (value && Object.is(item.text, value)) {
                    this.valueChange(item.value);
                    return true;
                }
            });
        }
        if (!value) {
            this.value = '';
        }
    }
}
