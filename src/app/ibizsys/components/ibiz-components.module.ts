import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { FileUploadModule } from 'ng2-file-upload';

import { IBizFormComponent } from '@ibizsys/components/ibiz-form/ibiz-form.component';
import { IBizFormItemComponent } from '@ibizsys/components/ibiz-form-item/ibiz-form-item.component';
import { IBizPickerComponent } from './ibiz-picker/ibiz-picker.component';
import { IBizMPickerComponent } from './ibiz-mpicker/ibiz-mpicker.component';
import { IBizDatepickerComponent } from './ibiz-datepicker/ibiz-datepicker.component';
import { IBizTimepickerComponent } from './ibiz-timepicker/ibiz-timepicker.component';
import { IBizCheckboxComponent } from './ibiz-checkbox/ibiz-checkbox.component';
import { IBizEcharts4Component } from './ibiz-echarts4/ibiz-echarts4.component';
import { IBizFileUploadComponent } from './ibiz-file-upload/ibiz-file-upload.component';
import { IBizFormGroupComponent } from './ibiz-form-group/ibiz-form-group.component';
import { IBizGroupMenuComponent } from './ibiz-group-menu/ibiz-group-menu.component';
import { IBizDRPanelComponent } from './ibiz-drpanel/ibiz-drpanel.component';
import { IBizPasswordComponent } from './ibiz-password/ibiz-password.component';
import { IBizSafeHtmlPipe } from './ibiz-safe-string/ibiz-safe-string.pipe';
import { IBizTextareaComponent } from './ibiz-textarea/ibiz-textarea.component';
import { IBizImportdataViewComponent } from './ibiz-importdata-view/ibiz-importdata-view.component';
import { IBizPictureComponent } from './ibiz-picture/ibiz-picture.component';
import { IBizSelectComponent } from './ibiz-select/ibiz-select.component';
import { IBizRichTextEditorComponent } from './ibiz-rich-text-editor/ibiz-rich-text-editor.component';
import { IBizAutocompleteComponent } from './ibiz-autocomplete/ibiz-autocomplete.component';
import { IBizTextarea10Component } from './ibiz-textarea-10/ibiz-textarea-10.component';
import { IBizIFrameComponent } from '@ibizsys/components/ibiz-iframe/ibiz-iframe.component';

const conponents = [
  IBizFormComponent,
  IBizFormItemComponent,
  IBizPickerComponent,
  IBizMPickerComponent,
  IBizDatepickerComponent,
  IBizTimepickerComponent,
  IBizCheckboxComponent,
  IBizEcharts4Component,
  IBizFileUploadComponent,
  IBizFormGroupComponent,
  IBizGroupMenuComponent,
  IBizDRPanelComponent,
  IBizPasswordComponent,
  IBizSafeHtmlPipe,
  IBizTextareaComponent,
  IBizImportdataViewComponent,
  IBizPictureComponent,
  IBizSelectComponent,
  IBizRichTextEditorComponent,
  IBizAutocompleteComponent,
  IBizTextarea10Component,
  IBizIFrameComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FileUploadModule,
  ],
  exports: [
    ...conponents
  ],
  declarations: [
    ...conponents
  ],
  entryComponents: [
    IBizImportdataViewComponent,
  ],
})
export class IBizComponentsModule { }
