import { Component, Input, OnInit } from '@angular/core';
import { IBizEvent } from '@ibizsys/IBizEvent';

@Component({
    selector: 'app-ibiz-iframe',
    template: `<ng-container *ngIf="isShowIFrame">
        <iframe width="100%" [height]="height" [src]=" iframeurl | IBizSafeString:'resourceurl'" frameborder="0"></iframe>
    </ng-container>`
})
export class IBizIFrameComponent implements OnInit {

    /**
     * 表单部件对象
     *
     * @type {*}
     * @memberof IBizIFrameComponent
     */
    @Input() form: any;

    /**
     * 表单成员高度
     *
     * @type {string}
     * @memberof IBizIFrameComponent
     */
    @Input() height: string;

    /**
     * iframe url
     *
     * @type {string}
     * @memberof IBizIFrameComponent
     */
    @Input() url: string;

    /**
     * 表单成员刷新项
     *
     * @type {string}
     * @memberof IBizIFrameComponent
     */
    @Input() refreshitems: string;

    /**
     * 参数名称
     *
     * @type {string}
     * @memberof IBizIFrameComponent
     */
    @Input() paramsname: string;

    /**
     * 是否显示 iframe 
     *
     * @type {boolean}
     * @memberof IBizIFrameComponent
     */
    public isShowIFrame: boolean = false;

    /**
     * 参数对象
     *
     * @private
     * @type {*}
     * @memberof IBizIFrameComponent
     */
    private hookItems: any = {};

    /**
     * ifrmae url
     *
     * @type {string}
     * @memberof IBizIFrameComponent
     */
    public iframeurl: string;

    ngOnInit() {
        this.initIFrame();
    }

    /**
     * 初始化关系部件
     * 
     * @private
     * @memberof IBizDRPanelComponent
     */
    private initIFrame(): void {

        if (this.form) {
            const form: any = this.form;
            form.on(IBizEvent.IBizForm_FORMLOADED, (data) => {
                this.refreshIFrame();
            });
            form.on(IBizEvent.IBizForm_FORMSAVED, (data) => {
                this.refreshIFrame();
            });

            const items: Array<any> = this.refreshitems.split(';');

            if (items.length > 0) {
                items.forEach(item => {
                    this.hookItems[item.toLowerCase()] = true;
                });

                form.on(IBizEvent.IBizForm_FORMFIELDCHANGED, (data) => {
                    if (form.$ignoreformfieldchange) {
                        return;
                    }

                    let fieldname = '';
                    if (data != null) {
                        fieldname = data.name;
                    } else {
                        return;
                    }
                    if (this.isRefreshItem(fieldname)) {
                        this.refreshIFrame();
                    }
                });
            }
        }
    }

    /**
     * 是否刷新数据项
     *
     * @private
     * @param {string} name
     * @returns {boolean}
     * @memberof IBizIFrameComponent
     */
    private isRefreshItem(name: string): boolean {
        if (!name || name === '') {
            return false;
        }

        if (this.hookItems[name.toLowerCase()]) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 刷新 iframe
     *
     * @private
     * @memberof IBizIFrameComponent
     */
    private refreshIFrame(): void {
        if (this.url && (!this.url.startsWith('https://') && !this.url.startsWith('http://'))) {
            return;
        }
        const data = this.form.getValues();
        const items: Array<any> = this.refreshitems.split(';');
        let params: Array<any> = [];
        items.forEach(name => {
            if (data && data.hasOwnProperty(name) && !Object.is(data[name], '')) {
                params.push(`${name}=${data[name]}`);
            }
        });

        this.iframeurl = this.url.indexOf('?') === -1 ? `${this.url}?${params.join('&')}` : `${this.url}&${params.join('&')}`;
        this.isShowIFrame = true;
    }

}
