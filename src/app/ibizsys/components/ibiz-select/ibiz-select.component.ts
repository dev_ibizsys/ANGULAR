import { IBizHttp } from '@ibizsys/util/IBizHttp';
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { NzSelectComponent } from 'ng-zorro-antd';

@Component({
    selector: 'app-ibiz-select',
    templateUrl: './ibiz-select.component.html',
    styles: []
})
export class IBizSelectComponent implements OnInit {

    /**
     * 选中值
     *
     * @type {*}
     * @memberof IBizSelectComponent
     */
    public value: any;

    /**
     * 是否启用
     * 
     * @type {boolean}
     * @memberof IBizPickerComponent
     */
    @Input() disabled: boolean;

    /**
     * 表单部件对象
     * 
     * @type {*}
     * @memberof IBizPickerComponent
     */
    @Input() form: any;

    /**
     * 表格部件对象，行编辑使用
     *
     * @type {*}
     * @memberof IBizSelectComponent
     */
    @Input() grid: any;

    /**
     * 表格数据，行编辑使用
     *
     * @type {*}
     * @memberof IBizSelectComponent
     */
    @Input() data: any;

    /**
     * 组件名称
     * 
     * @type {string}
     * @memberof IBizPickerComponent
     */
    @Input() name: string;

    /**
     * 引入样式
     *
     * @type {*}
     * @memberof IBizSelectComponent
     */
    @Input() styleCss: any;

    /**
     * 默认文字
     *
     * @type {string}
     * @memberof IBizSelectComponent
     */
    @Input() placeHolder: string;

    /**
     * 代码表
     *
     * @type {Array<any>}
     * @memberof IBizSelectComponent
     */
    @Input() codelist: Array<any>;

    /**
     * 代码表类型
     *
     * @type {string}
     * @memberof IBizSelectComponent
     */
    @Input() codelisttype: string;

    /**
     * 动态代码表数据请求url
     *
     * @type {string}
     * @memberof IBizSelectComponent
     */
    @Input() url: string;

    /**
     * 动态代码表文本值
     *
     * @type {string}
     * @memberof IBizSelectComponent
     */
    @Input() text: string;

    /**
     * nzselect组件对象
     *
     * @type {NzSelectComponent}
     * @memberof IBizSelectComponent
     */
    @ViewChild('selectObj') selectObj: NzSelectComponent;

    /**
     * 组件值
     * 
     * @memberof IBizPickerComponent
     */
    @Input()
    set itemvalue(val) {
        if (Object.is(val, '')) {
            this.selectObj.clearSelect();
        } else {
            this.value = val;
            this.selectObj.writeValue(val);
        }
    }

    /**
     * Creates an instance of IBizSelectComponent.
     * 创建 IBizSelectComponent 实例
     * 
     * @param {IBizHttp} iBizHttp
     * @memberof IBizSelectComponent
     */
    constructor(private iBizHttp: IBizHttp) { }

    /**
     * Angular生命周期，部件初始化
     *
     * @memberof IBizSelectComponent
     */
    ngOnInit(): void {
        if (this.grid && Object.is(this.codelisttype, 'DYNAMIC')) {
            // this.codelist = [{ realtext: this.data[this.text], text: this.data[this.text], value: this.data[this.name] }];
            this.loadDynamicCodeList();
        }
    }

    /**
     * 选中值发生改变
     *
     * @memberof IBizSelectComponent
     */
    public onValueChanged(): void {
        this.value = this.value ? this.value : '';
        if (this.form) {
            const itemField = this.form.findField(this.name);
            if (itemField) {
                itemField.setValue(this.value);
            }
        }
        if (this.grid) {
            this.grid.colValueChange(this.name, this.value, this.data);
            if (Object.is(this.codelisttype, 'DYNAMIC')) {
                const code: Array<any> = this.codelist.filter(item => Object.is(item.value, this.value));
                const text: string = (code && code.length === 1) ? code[0].text : '';
                this.grid.colValueChange(this.text, text, this.data);
            }
        }
    }

    /**
     * 加载动态代码表
     *
     * @private
     * @returns {void}
     * @memberof IBizSelectComponent
     */
    private loadDynamicCodeList(): void {
        if (!this.grid || !Object.is(this.codelisttype, 'DYNAMIC')) {
            return;
        }
        this.codelist = [];
        this.iBizHttp.post(this.url).subscribe((success) => {
            if (success.ret === 0) {
                [...this.codelist] = success.items;
            }
        }, (error) => {
            console.log(error);
        });
    }

    /**
     * 打开下拉列表
     *
     * @param {*} $event
     * @returns {void}
     * @memberof IBizSelectComponent
     */
    public openChange($event: any): void {
        if (!$event) {
            return;
        }
        this.loadDynamicCodeList();
    }
}
