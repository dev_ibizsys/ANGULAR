import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ibiz-password',
  templateUrl: './ibiz-password.component.html',
  styles: []
})
export class IBizPasswordComponent implements OnInit {

  public textType = false;
  public timeOut: any;
  public styleCss: any = {};

  @Input()
  form: any;
  @Input()
  name: string;
  @Input()
  itemvalue: string;
  @Input()
  disabled: boolean;
  @Input()
  set width(val: number) {
    if (val &&　val > 0) {
      this.styleCss.width = val + 'px';
    }
  }
  @Input()
  set height(val: number) {
    if (val &&　val > 0) {
      this.styleCss.geight = val + 'px';
    }
  }
  constructor() { }

  ngOnInit() {
  }

  /**
   * 切换图片
   * 
   * @memberof IBizPasswordComponent
   */
  changeType(): void {
    this.textType = !this.textType;
  }

  /**
   * 数据发生改变，触发表单项更新
   * 
   * @param {any} newVal 
   * @returns {void} 
   * @memberof IBizPasswordComponent
   */
  valueChange(newVal): void {
    if (this.form) {
      if (this.timeOut) {
        clearTimeout(this.timeOut);
        this.timeOut = undefined;
      }
      this.timeOut = setTimeout(() => {
          if (this.name && !Object.is(this.name, '')) {
            let itemField = this.form.findField(this.name);
            if (itemField) {
                itemField.setValue(newVal);
            }
          }
      }, 300);
    }
  }
}
