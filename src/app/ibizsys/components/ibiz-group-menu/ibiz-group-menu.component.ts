import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-ibiz-group-menu',
	templateUrl: './ibiz-group-menu.component.html',
	styleUrls: ['./ibiz-group-menu.component.less']
})
export class IBizGroupMenuComponent implements OnInit {

	/**
	 * 选中的菜单对象
	 * 
	 * @memberof IBizGroupMenuComponent
	 */
	public selectData = {};

	@Input()
	menus: any[];
	@Input()
	menuCtrl: any;


	constructor() { }

	ngOnInit() {

	}

	/**
	 * 选中菜单项
	 * 
	 * @param {*} select 
	 * @memberof IBizGroupMenuComponent
	 */
	public onSelect(select: any) {
		if (this.menuCtrl) {
			this.selectData = select;
			this.menuCtrl.onSelectChange(select);
		}
	}

}
