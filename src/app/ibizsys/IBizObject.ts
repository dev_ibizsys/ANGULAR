import { EventEmitter } from 'events';

/**
 * IbizSys控制器借口对象
 * 
 * @export
 * @abstract
 * @class IBizObject
 */
export abstract class IBizObject {

    /**
     * ECMAScrip 6 事件对象
     * 
     * @private
     * @type {EventEmitter}
     * @memberof IBizObject
     */
    private $emitter: EventEmitter;

    /**
     * Creates an instance of IBizObject.
     * 创建 IBizObject 实例 
     * 
     * @param {*} [opts={}]
     * @memberof IBizObject
     */
    constructor(opts: any = {}) {
        this.$emitter = new EventEmitter();
        this.$emitter.setMaxListeners(100);
    }

    /**
     * 控制器事件声明
     * 
     * @param {string} eventName 事件名称
     * @param {*} datas 
     * @memberof IBizObject
     */
    public fire(eventName: string, datas: any): void {
        this.$emitter.emit(eventName, datas);
    }

    /**
     * 控制器对象事件注册
     *
     * @param {string} eventName
     * @param {*} func
     * @memberof IBizObject
     */
    public on(eventName: string, func: any): void {
        this.$emitter.on(eventName, func);
    }
}

