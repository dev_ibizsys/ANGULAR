import { IBizEnvironment } from './../../../environments/IBizEnvironment';
import { IBizObject } from '@ibizsys/IBizObject';
import { IBizEvent } from '@ibizsys/IBizEvent';

/**
 * 表单属性对象<主要管理属性及其标签的值、可用、显示、必填等操作>
 * 
 * @export
 * @class IBizField
 * @extends {IBizService}
 */
export class IBizField extends IBizObject {

    /**
     *表单项类型
     * 
     * @private
     * @type {string}
     * @memberof IBizField
     */
    public fieldType: string;

    /**
     * 表单对象
     * 
     * @private
     * @type {*}
     * @memberof IBizField
     */
    public form: any;

    /**
     * 表单项的值
     * 
     * @private
     * @type {string}
     * @memberof IBizField
     */
    private $value: string;

    /**
     * 表单项名称
     * 
     * @type {string}
     * @memberof IBizField
     */
    public name: string;

    /**
     * 表单项是否禁用
     * 
     * @private
     * @type {boolean}
     * @memberof IBizField
     */
    public disabled: boolean;

    /**
     * 隐藏表单项
     * 
     * @type {boolean}
     * @memberof IBizField
     */
    public hidden: boolean;

    /**
     * 是否可见
     * 
     * @type {boolean}
     * @memberof IBizField
     */
    public visible: boolean;

    /**
     * 是否是必填
     * 
     * @type {boolean}
     * @memberof IBizField
     */
    public allowEmpty: boolean;

    /**
     * 是否有错误信息
     * 
     * @type {boolean}
     * @memberof IBizField
     */
    public hasError: boolean = false;

    /**
     * 表单项校验状态
     * 
     * @type {string}
     * @memberof IBizField
     */
    public validateStatus: string = 'success';

    /**
     * 表达校验错误信息
     * 
     * @type {string}
     * @memberof IBizField
     */
    public errorInfo: string = '';

    /**
     * 属性动态配置值<代码表>
     *
     * @type {Array<any>}
     * @memberof IBizField
     */
    public config: Array<any> = [];

    /**
     * 属性动态配置值<用户字典>
     *
     * @type {Array<any>}
     * @memberof IBizField
     */
    public dictitems: Array<any> = [];

    /**
     * 错误类型
     *
     * @private
     * @type {('FRONTEND' | 'BACKEND' | '')} 前端 | 后端 | 空
     * @memberof IBizField
     */
    private errorType: 'FRONTEND' | 'BACKEND' | '';

    /**
     * 表单项当前权限模式
     *
     * @private
     * @type {number} (表单项权限标记为false时: 0 = 无权限, 1 = 有权限;表单项权限标记为true时: 0 = 无权限, 1 = 只读, 2 = 读写)
     * @memberof IBizField
     */
    private jurisdiction: number = IBizEnvironment.formItemPrivTag ? 2 : 1;

    /**
     * 表单项无权限默认显示模式
     *
     * @private
     * @type {number}
     * @memberof IBizField
     */
    private noPrivDisplayMode: number = 1;

    /**
     * 旧值
     *
     * @private
     * @type {*}
     * @memberof IBizField
     */
    private oldValue: any;

    /**
     * Creates an instance of IBizField.
     * 创建 IBizField 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizField
     */
    constructor(opts: any = {}) {
        super(opts);
        this.fieldType = opts.fieldType;
        this.form = opts.form;
        this.name = opts.name;
        this.disabled = opts.disabled;
        this.hidden = opts.hidden;
        this.visible = opts.visible ? true : false;
        this.allowEmpty = opts.allowEmpty ? true : false;
        this.noPrivDisplayMode = opts.noPrivDisplayMode;
    }

    /**
     * 设置值
     * 
     * @memberof IBizField
     */
    set value(val) {
        const oldVal = this.$value;
        this.$value = val;
        if (oldVal !== this.$value) {
            this.onValueChanged(oldVal);
        }
    }

    /**
     * 获取值
     * 
     * @type {string}
     * @memberof IBizField
     */
    get value(): string {
        return this.$value ? this.$value : '';
    }

    /**
     * 获取表单项类型
     * 
     * @returns {string} 
     * @memberof IBizField
     */
    public getFieldType(): string {
        return this.fieldType;
    }

    /**
     * 设置表单对象
     * 
     * @param {*} form 
     * @memberof IBizField
     */
    public setForm(form: any): void {
        this.form = form;
    }

    /**
     * 获取表单对象
     * 
     * @returns {*} 
     * @memberof IBizField
     */
    public getForm(): any {
        return this.form;
    }

    /**
     * 获取值
     * 
     * @returns {*} 
     * @memberof IBizField
     */
    public getValue(): any {
        return this.value;
    }

    /**
     * 设置值
     * 
     * @param {string} value 
     * @memberof IBizField
     */
    public setValue(value: string): void {
        this.value = value;
    }

    /**
     * 获取属性名
     * 
     * @returns {string} 
     * @memberof IBizField
     */
    public getName(): string {
        return this.name;
    }

    /**
     * 是否启用
     * 
     * @returns {boolean} 
     * @memberof IBizField
     */
    public isDisabled(): boolean {
        return this.disabled;
    }

    /**
     * 设置是否启用
     * 
     * @param {boolean} disabled 
     * @memberof IBizField
     */
    public setDisabled(disabled: boolean): void {
        if (IBizEnvironment.formItemPrivTag) {
            this.disabled = (this.jurisdiction === 0 || this.jurisdiction === 1) ? true : disabled;
        } else {
            this.disabled = this.jurisdiction === 0 ? true : disabled;
        }
    }

    /**
     * 设置是否拥有权限
     *
     * @param {string} jurisdiction
     * @memberof IBizField
     */
    public setJurisdiction(jurisdiction: string): void {
        try {
            this.jurisdiction = parseInt(jurisdiction, 10);
        } catch (error) {
            console.warn(error);
        }
        this.visible = (this.jurisdiction !== 0 || this.noPrivDisplayMode === 1) ? true : false;
        if (IBizEnvironment.formItemPrivTag) {
            this.disabled = (this.jurisdiction === 0 || this.jurisdiction === 1) ? true : false;
        } else {
            this.disabled = this.jurisdiction === 0 ? true : false;
        }
    }

    /**
     * 隐藏控件
     * 
     * @param {boolean} hidden 
     * @memberof IBizField
     */
    public setHidden(hidden: boolean): void {
        this.hidden = hidden;

    }

    /**
     * 设置可见性
     * 
     * @param {boolean} visible 
     * @memberof IBizField
     */
    public setVisible(visible: boolean): void {
        this.visible = (this.jurisdiction !== 0 || this.noPrivDisplayMode === 1) ? visible : false;
    }

    /**
     * 设置属性动态配置
     *
     * @param {Array<any>} config 代码表
     * @memberof IBizField
     */
    public setAsyncConfig(config: Array<any>): void {
        if (Array.isArray(config)) {
            this.config = [...config];
        }
    }

    /**
     * 设置用户字典
     *
     * @param {Array<any>} item
     * @memberof IBizField
     */
    public setDictItems(item: Array<any>): void {
        if (Array.isArray(item)) {
            this.dictitems = [...item];
        }
    }

    /**
     * 设置只读<Ext版本方法禁止使用>
     * 
     * @param {boolean} readonly 
     * @memberof IBizField
     */
    public setReadOnly(readonly: boolean): void {
        this.setDisabled(readonly);
    }

    /**
     * 编辑是否必须输入
     * 
     * @param {boolean} allowblank 
     * @memberof IBizField
     */
    public setAllowBlank(allowblank: boolean): void {
        this.allowEmpty = allowblank;
    }

    /**
     * 标记表单项值无效提示
     * 
     * @param {*} info 
     * @memberof IBizField
     */
    public markInvalid(info: any): void {
    }

    /**
     * 设置表单项错误
     * 
     * @param {*} info 
     * @memberof IBizField
     */
    public setActiveError(info: any): void {
        this.markInvalid(info);
    }

    /**
     * 表单项是否有错误
     * 
     * @returns {boolean} 
     * @memberof IBizField
     */
    public hasActiveError(): boolean {
        return this.hasError;
    }

    /**
     * 重置表单项错误
     * 
     * @memberof IBizField
     */
    public unsetActiveError(): void {
        this.hasError = false;
    }

    /**
     * 值变化
     * 
     * @param {string} oldValue 
     * @param {string} newValue 
     * @memberof IBizField
     */
    public onValueChanged(oldValue: string): void {
        this.oldValue = oldValue;
        this.fire(IBizEvent.IBizField_VALUECHANGED, { name: this.getName(), value: oldValue, field: this });
    }

    /**
     * 输入框失去焦点触发
     * 
     * @param {*} $event 
     * @memberof IBizField
     */
    public onBlur($event: any): void {
        if (!$event) {
            return;
        }
        if (Object.is($event.target.value, this.value)) {
            return;
        }
        this.value = $event.target.value;
    }

    /**
     * 键盘事件
     *
     * @param {*} $event
     * @returns {void}
     * @memberof IBizField
     */
    public onKeydown($event: any): void {
        if (!$event) {
            return;
        }
        if ($event.keyCode !== 13) {
            return;
        }
        if (Object.is($event.target.value, this.value)) {
            return;
        }

        this.value = $event.target.value;
    }

    /**
     * 设置表单项错误信息
     * 
     * @param {*} [info={}] 
     * @memberof IBizField
     */
    public setErrorInfo(info: any = {}): void {
        this.validateStatus = info.validateStatus;
        this.hasError = info.hasError;
        this.errorInfo = info.errorInfo;
        this.errorType = info.errorType;
    }

    /**
     * 获取表单项错误类型
     *
     * @returns {string}
     * @memberof IBizField
     */
    public getErrorType(): string {
        return this.errorType;
    }

    /**
     * 获取表单项旧值
     *
     * @returns {*}
     * @memberof IBizField
     */
    public getOLdValue(): any {
        return this.oldValue;
    }
}