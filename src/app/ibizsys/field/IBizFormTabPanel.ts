import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 表单分页面板
 * 
 * @export
 * @class IBizFormTabPanel
 * @extends {IBizField}
 */
export class IBizFormTabPanel extends IBizField {

    /**
     * Creates an instance of IBizFormTabPanel.
     * 创建 IBizFormTabPanel 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormTabPanel
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}