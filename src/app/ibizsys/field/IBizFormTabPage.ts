import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 表单分页部件
 *
 * @export
 * @class IBizFormTabPage
 * @extends {IBizField}
 */
export class IBizFormTabPage extends IBizField {

    /**
     * Creates an instance of IBizFormTabPage.
     * 创建 IBizFormTabPage 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormTabPage
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}