import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 表单直接内容
 * 
 * @export
 * @class IBizFormRawItem
 * @extends {IBizField}
 */
export class IBizFormRawItem extends IBizField {

    /**
     * Creates an instance of IBizFormRawItem.
     * 创建 IBizFormRawItem 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormRawItem
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}