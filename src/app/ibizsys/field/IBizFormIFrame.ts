import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 表单IFrame部件
 * 
 * @export
 * @class IBizFormIFrame
 * @extends {IBizField}
 */
export class IBizFormIFrame extends IBizField {

	/**
	 * Creates an instance of IBizFormIFrame.
	 * 创建 IBizFormIFrame 实例
	 * 
	 * @param {*} [opts={}]
	 * @memberof IBizFormIFrame
	 */
	constructor(opts: any = {}) {
		super(opts);
	}
}