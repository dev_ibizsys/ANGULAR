import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 表单关系部件
 * 
 * @export
 * @class IBizFormDRPanel
 * @extends {IBizField}
 */
export class IBizFormDRPanel extends IBizField {

    /**
     * Creates an instance of IBizFormDRPanel.
     * 创建 IBizFormDRPanel 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormDRPanel
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}