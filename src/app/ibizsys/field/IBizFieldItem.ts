import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 表单项
 * 
 * @export
 * @class IBizFieldItem
 * @extends {IBizField}
 */
export class IBizFieldItem extends IBizField {

    /**
     * Creates an instance of IBizFieldItem.
     * 创建 IBizFieldItem 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFieldItem
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}