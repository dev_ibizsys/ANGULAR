import { IBizField } from '@ibizsys/field/IBizField';

/**
 * 隐藏表单项
 * 
 * @export
 * @class IBizHiddenField
 * @extends {IBizField}
 */
export class IBizHiddenField extends IBizField {

    /**
     * Creates an instance of IBizHiddenField.
     * 创建 IBizHiddenField 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizHiddenField
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}