import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { IBizComponentsModule } from '../components/ibiz-components.module';

import { IBizToolbarComponent } from './ibiz-dynamic-toolbar/ibiz-toolbar.component';
import { IBizButtonComponent } from './ibiz-dynamic-button/ibiz-button.component';
import { IBizDynamicFormComponent } from './ibiz-dynamic-form/ibiz-dynamic-form.component';
import { IBizButtonComponentService } from './ibiz-button-component.service';
import { IBizFieldComponentService } from './ibiz-field-component.service';
import { IBizDynamicFormPageComponent } from './ibiz-dynamic-form-page/ibiz-dynamic-form-page.component';
import { IBizDynamicInputComponent } from './ibiz-dynamic-input/ibiz-dynamic-input.component';
import { IBizDynamicFormGroupComponent } from './ibiz-dynamic-from-group/ibiz-dynamic-form-group.component';
import { IBizDynamicPickerComponent } from './ibiz-dynamic-picker/ibiz-dynamic-picker.component';
import { IBizDynamicSpanComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-span/ibiz-dynamic-span.component';
import { IBizDynamicRadioButtonListComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-radio-button-list/ibiz-dynamic-radio-button-list.component';
import { IBizDynamicAddressPickupComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-address-pickup/ibiz-dynamic-address-pickup.component';
import { IBizDynamicTextreaComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-textarea/ibiz-dynamic-textarea.component';
import { IBizDynamicTextrea10Component } from '@ibizsys/dynamic-components/ibiz-dynamic-textarea10/ibiz-dynamic-textarea10.component';
import { IBizDynamicListBoxPickupComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-list-box-pickup/ibiz-dynamic-list-box-pickup.component';
import { IBizDynamicPasswordComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-password/ibiz-dynamic-password.component';
import { IBizDynamicDataPickerexComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-date-pickerex/ibiz-dynamic-data-pickerex.component';
import { IBizDynamicPictureComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-picture/ibiz-dynamic-picture.component';
import { IBizDynamicFileuploaderComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-fileuploader/ibiz-dynamic-fileuploader.component';
import { IBizDynamicDropdownlistComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-dropdownlist/ibiz-dynamic-dropdownlist.component';
import { IBizDynamicCheckboxComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-checkbox/ibiz-dynamic-checkbox.component';
import { IBizDynamicCheckboxListComponent } from '@ibizsys/dynamic-components/ibiz-dynamic-checkbox-list/ibiz-dynamic-checkbox-list.component';
import { IBizDynamicViewModalService } from '@ibizsys/dynamic-components/ibiz-dynamic-view-modal.service';

const dynamicComponents = [
    IBizToolbarComponent,
    IBizButtonComponent,
    IBizDynamicFormComponent,
    IBizDynamicFormPageComponent,
    IBizDynamicFormGroupComponent,
    IBizDynamicInputComponent,
    IBizDynamicPickerComponent,
    IBizDynamicSpanComponent,
    IBizDynamicRadioButtonListComponent,
    IBizDynamicAddressPickupComponent,
    IBizDynamicTextreaComponent,
    IBizDynamicTextrea10Component,
    IBizDynamicListBoxPickupComponent,
    IBizDynamicPasswordComponent,
    IBizDynamicDataPickerexComponent,
    IBizDynamicPictureComponent,
    IBizDynamicFileuploaderComponent,
    IBizDynamicDropdownlistComponent,
    IBizDynamicCheckboxComponent,
    IBizDynamicCheckboxListComponent,
];

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        IBizComponentsModule,
    ],
    exports: [
        ...dynamicComponents
    ],
    declarations: [
        ...dynamicComponents
    ],
    entryComponents: [
        ...dynamicComponents
    ],
    providers: [
        IBizDynamicViewModalService,
        IBizButtonComponentService,
        IBizFieldComponentService,
    ],
})
export class DynamicComponentsModule {}