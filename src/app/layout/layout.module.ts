import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { LayoutDefaultComponent } from './default/default.component';
import { LayoutFullScreenComponent } from './fullscreen/fullscreen.component';
import { HeaderComponent } from './default/header/header.component';
// import { SidebarComponent } from './default/sidebar/sidebar.component';
// import { HeaderSearchComponent } from './default/header/components/search.component';
// import { HeaderThemeComponent } from './default/header/components/theme.component';
import { HeaderNotifyComponent } from './default/header/components/notify/notify.component';
// import { HeaderTaskComponent } from './default/header/components/task.component';
// import { HeaderIconComponent } from './default/header/components/icon.component';
// import { HeaderFullScreenComponent } from './default/header/components/fullscreen.component';
// import { HeaderLangsComponent } from './default/header/components/langs.component';
// import { HeaderStorageComponent } from './default/header/components/storage.component';
import { HeaderUserComponent } from './default/header/components/user.component';
import { HeaderButtonComponent } from './default/header/components/header-button/header-button.component';

import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';

const COMPONENTS = [
    LayoutDefaultComponent,
    LayoutFullScreenComponent,
    HeaderComponent,
    // SidebarComponent
];

const HEADERCOMPONENTS = [
    // HeaderSearchComponent,
    HeaderNotifyComponent,
    // HeaderTaskComponent,
    // HeaderIconComponent,
    // HeaderFullScreenComponent,
    // HeaderThemeComponent,
    // HeaderLangsComponent,
    // HeaderStorageComponent,
    HeaderUserComponent,
    HeaderButtonComponent,
];

// passport
import { LayoutPassportComponent } from './passport/passport.component';
const PASSPORT = [
    LayoutPassportComponent
];

@NgModule({
    imports: [SharedModule, LoadingModule.forRoot({
        animationType: ANIMATION_TYPES.threeBounce,
        backdropBackgroundColour: 'rgba(0,0,0,0.1)',
        backdropBorderRadius: '10px',
        primaryColour: 'rgba(16,142,233,0.7)',
        secondaryColour: 'rgba(16,142,233,1.0)',
        tertiaryColour: 'rgba(16,142,233,0.7)'
    })],
    providers: [],
    declarations: [
        ...COMPONENTS,
        ...HEADERCOMPONENTS,
        ...PASSPORT
    ],
    exports: [
        ...COMPONENTS,
        ...PASSPORT
    ]
})
export class LayoutModule { }
