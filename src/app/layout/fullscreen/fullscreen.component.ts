import { IBizAppService } from '@ibizsys/IBizAppService';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'layout-fullscreen',
    templateUrl: './fullscreen.component.html',
    styleUrls: ['./fullscreen.component.less']
})
export class LayoutFullScreenComponent {
    constructor(public $ibizAppService: IBizAppService, private $route: Router) { }

    /**
     * 导航数据跳转处理
     *
     * @param {*} [data={}]
     * @memberof LayoutFullScreenComponent
     */
    public navigationLink(data: any = {}) {
        Object.assign(data, { breadcrumbs: true });
        this.$ibizAppService.updateActivatedRouteDatas(data);
        if (data.routerurl) {
            this.$route.navigateByUrl(data.routerurl);
        }
    }
}
