import { IBizAppService } from 'app/ibizsys/IBizAppService';
import { Component } from '@angular/core';

/**
 * 我的待办
 *
 * @export
 * @class HeaderNotifyComponent
 */
@Component({
    selector: 'header-notify',
    templateUrl: './notify.component.html',
    styleUrls: ['./notify.component.less']
})
export class HeaderNotifyComponent {

    /**
     * 我的待办状态
     *
     * @type {boolean}
     * @memberof HeaderNotifyComponent
     */
    public visible: boolean = false;

    /**
     * 待办工作流
     *
     * @type {Array<any>}
     * @memberof HeaderNotifyComponent
     */
    public items: Array<any> = [];

    /**
     * Creates an instance of HeaderNotifyComponent.
     * 创建 HeaderNotifyComponent 实例
     * 
     * @param {IBizAppService} ibizapp
     * @memberof HeaderNotifyComponent
     */
    constructor(private ibizapp: IBizAppService) {
        this.ibizapp.getAppUICounter().subscribe((data: any = {}) => {
            if (data.WFTASK && data.WFTASK.items) {
                this.items = [...data.WFTASK.items];
            }
        });
    }

    /**
     * 关闭待办
     *
     * @memberof HeaderNotifyComponent
     */
    public closeNotify(): void {
        this.visible = false;
    }

    /**
     * 查看待办详情
     *
     * @param {*} [item={}]
     * @memberof HeaderNotifyComponent
     */
    public wfTaskItemDetail(item: any = {}): void {
        console.log(`查看待办${item.wfworkflowname}详情`);
        this.visible = false;
    }

    /**
     * 查看全部待办
     *
     * @memberof HeaderNotifyComponent
     */
    public wfTaskItemAll(): void {
        console.log('查看全部待办');
        this.visible = false;
    }
}
