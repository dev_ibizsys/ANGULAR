import { IBizNotification } from './../../../../ibizsys/util/IBizNotification';
import { IBizHttp } from './../../../../ibizsys/util/IBizHttp';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '@delon/theme';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { IBizEnvironment } from '@env/IBizEnvironment';

@Component({
    selector: 'header-user',
    template: `
    <nz-dropdown nzPlacement="bottomRight">
        <div class="item d-flex align-items-center px-sm" nz-dropdown>
            <nz-avatar [nzSrc]="'./assets/img/avatar.png'" nzSize="small" class="mr-sm"></nz-avatar>
            {{settings.user.name}}
        </div>
        <div nz-menu class="width-sm">
        <!-- <div nz-menu-item [nzDisable]="true"><i class="anticon anticon-user mr-sm"></i>个人中心</div> -->
        <!-- <div nz-menu-item [nzDisable]="true"><i class="anticon anticon-setting mr-sm"></i>设置</div> -->
        <!-- <li nz-menu-divider></li> -->
        <div nz-menu-item (click)="installRTData()"><i class="anticon anticon-setting mr-sm"></i>安装运行数据</div>
            <div nz-menu-item (click)="logout()"><i class="anticon anticon-setting mr-sm"></i>退出登录</div>
        </div>
    </nz-dropdown>
    `
})
export class HeaderUserComponent implements OnInit {
    constructor(
        public settings: SettingsService,
        private router: Router,
        private http: IBizHttp,
        private iBizNotification: IBizNotification,
        @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService) { }

    ngOnInit(): void {
        this.tokenService.change().subscribe((res: any) => {
            this.settings.setUser(res);
        });
        // mock
        const token = this.tokenService.get() || {
            token: 'nothing',
            name: '匿名访问',
            avatar: './assets/img/avatar.png',
            email: ''
        };
        this.tokenService.set(token);

        // 非本地开发获取用户信息
        if (!IBizEnvironment.LocalDeve) {
            this.http.post(IBizEnvironment.AppLogin, { srfaction: 'getcuruserinfo' }).subscribe((result) => {
                if (result.ret === 0) {
                    if (Object.keys(result.data).length !== 0) {
                        let _data: any = {};
                        Object.assign(_data, result.data);
                        this.tokenService.set({
                            token: _data.loginkey,
                            name: _data.username,
                            email: _data.loginname,
                            id: _data.userid,
                            time: +new Date
                        });
                    }
                } else {
                    this.tokenService.set(token);
                }
            }, (error) => {
                console.log(error)
            });
        }
    }

    public logout() {

        const curUrl: string = decodeURIComponent(window.location.href);
        if (IBizEnvironment.UacAuth) {
            window.location.href = `../api/uacloginout.do?RU=${curUrl}`;
        } else if (IBizEnvironment.LocalDeve) {
            console.log(this.router);
            // return ;
            this.tokenService.clear();
            this.router.navigate(['/passport/login', { callback: this.router.url }]);
        } else {
            window.location.href = `..${IBizEnvironment.Logout}?RU=${curUrl}`;
        }

    }

    /**
     * 安装运行数据
     * 
     * @memberof HeaderUserComponent
     */
    public installRTData(): void {
        this.http.post(IBizEnvironment.InstallRTData).subscribe((result) => {
            if (result.ret === 0) {
                this.iBizNotification.html(result.info);
            } else {
                this.iBizNotification.error('错误', result.info);
            }
        }, (error) => {
            this.iBizNotification.error('错误', error.info);
        });
    }
}
