import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IBizHttp } from '@ibizsys/util/IBizHttp';
import { IBizEnvironment } from '@env/IBizEnvironment';

@Component({
  selector: 'app-header-button',
  templateUrl: './header-button.component.html',
  styleUrls: ['./header-button.component.scss']
})
export class HeaderButtonComponent implements OnInit {
  @Input()
  url: string;
  @Output()
  edit: EventEmitter<any> = new EventEmitter();

  public items: any[] = [];

  public isShowEditMenu: boolean = IBizEnvironment.isEditableHeaderMenu;

  constructor(private http: IBizHttp) { }

  ngOnInit() {
    let postUrl: string;
    if (this.url) {
      postUrl = this.url;
    } else {
      postUrl = IBizEnvironment.HeaderMenuDataUrl;
    }
    if (postUrl && !Object.is(postUrl, '')) {
      this.http.post2(postUrl).subscribe((res) => {
        if (res && res.ret !== 0 || !res.items) { return; }
        this.items = res.items;
      });
    } else {
      this.isShowEditMenu = false;
    }
  }

  public openNewWindow(item): void {
    window.open(item.funcurl, (item.isblank === 1) ? '_blank' : '_self');
  }

  public openEdit(): void {
    this.edit.emit();
  }

}
