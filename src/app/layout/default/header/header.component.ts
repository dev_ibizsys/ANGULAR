import { Component } from '@angular/core';
import { SettingsService } from '@delon/theme';
import { IBizAppService } from '@ibizsys/IBizAppService';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {

    constructor(public settings: SettingsService, public $ibizapp: IBizAppService) { }

    toggleCollapsedSideabar() {
        this.settings.setLayout('collapsed', !this.settings.layout.collapsed);
    }
}
